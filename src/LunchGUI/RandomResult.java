package LunchGUI;

import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;

import LunchGUI.HomePage.SearchListener;
import lunch.DB;
import lunch.Lunch;

public class RandomResult {
	
	RandomResult(){
		this.setPage41();
	}
	
	static JPanel panel41 = new JPanel();
	static JButton back41 = new JButton("< 回上頁");
	
	static JLabel  status41 = new JLabel("搜尋結果",SwingConstants.CENTER);
	private JLabel  name41 = new JLabel("店名",SwingConstants.CENTER);
	private JLabel  region41 = new JLabel("區域",SwingConstants.CENTER);
	private JLabel  type41 = new JLabel("分類",SwingConstants.CENTER);
	private JLabel  price41 =new JLabel("最低價格",SwingConstants.CENTER);
	private JLabel  rate41 =new JLabel("評價",SwingConstants.CENTER);
	static JLabel  open41 =new JLabel("休息日",SwingConstants.CENTER);
	private JLabel  address41 =new JLabel("地址",SwingConstants.CENTER);
	
	static JLabel  nameR = new JLabel("校內",SwingConstants.CENTER);
	static JLabel  regionR = new JLabel("校內",SwingConstants.CENTER);
	static JLabel  typeR = new JLabel("小吃",SwingConstants.CENTER);
	static JLabel  priceR =new JLabel("60",SwingConstants.CENTER);
	static JLabel  rateR =new JLabel("4",SwingConstants.CENTER);
	static JLabel  openR =new JLabel(" ",SwingConstants.CENTER);
	static JLabel  addressR =new JLabel("台北市大安區長興街50號 台大男一舍（我家廚房）",SwingConstants.CENTER);
	
	static JButton again = new JButton("換一個");
	
	private JComponent[] jc41 = {back41,status41,name41,region41,type41,price41,rate41,open41,address41,nameR,regionR,typeR,priceR,rateR,openR,addressR,again};
	
	private int layout41 [][] ={{ 0, 0, 3, 1, 0, 0, Format.fill[2], Format.anchor[5],1,1 ,0,0,5,1}, 
			{ 3, 0, 18, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,5,1}, 
			{ 0, 1, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],100,1 ,0,0,0,1}, 
			{ 3, 1, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],50,1 ,0,0,0,1}, 
			{ 6, 1, 3, 1, 0, 0, Format.fill[0],Format. anchor[5],50,1 ,0,0,0,1}, 
			{ 9, 1, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],20,1 ,0,0,0,1}, 
			{ 12, 1, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],30,1 ,0,0,0,1}, 
			{ 15, 1, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],20,1 ,0,0,0,1}, 
			{ 18, 1, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],350,1 ,0,0,0,1}, 
			
			{ 0, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,0,1}, 
			{ 3, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,0,1}, 
			{ 6, 2, 3, 1, 0, 0, Format.fill[0],Format. anchor[5],1,1 ,0,0,0,1}, 
			{ 9, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,0,1}, 
			{ 12, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,0,1}, 
			{ 15, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,0,1},
			{ 18, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,0,1},
			
			{ 0, 3, 21, 1, 0, 0, Format.fill[3], Format.anchor[0],20,5 ,5,0,0,1}, };
	//初始化頁面
	public void setPage41() {
		panel41.setLayout(new GridBagLayout());
		Format.setFormat(jc41);
		for (int i = 0; i < jc41.length; i++) {
			Format.add(panel41,jc41,layout41,i);
		}
		for (int i = 9; i<jc41.length;i++) {
			Format.setWhiteBlack(jc41[i]);
		}
		Format.setGrayWhite(again);
		addListener();
	}
	//加入所有監聽器
	public void addListener() {
		back41.addMouseListener(new Back41Listener());
		again.addMouseListener(new AgainListener());
	
	}
	//回上頁
	class Back41Listener extends BackListener{
		@Override
		public void mouseClicked(MouseEvent e) {
			panel41.setVisible(false);
			HomePage.panel1.setVisible(true);
		}
	}
	//再隨機推薦一次
	class AgainListener extends AllListener {
		
		@Override
		public void mouseClicked(MouseEvent e) {
			Lunch ran = DB.randompick();
			String name = ran.name;
			String address = ran.address;
			String region = ran.place;
			String rate = Integer.toString(ran.rate);
			String price = Integer.toString(ran.cost) ;
			String type = ran.typ;
			String open = Format.openToString(ran.open);
			
			RandomResult.nameR.setText(name);
			RandomResult.addressR.setText(address);
			RandomResult.regionR.setText(region);
			RandomResult.rateR.setText(rate);
			RandomResult.priceR.setText(price);
			RandomResult.typeR.setText(type);
			RandomResult.openR.setText(open);
			

		}
		
		public void mouseEntered(MouseEvent e) {
			again.setForeground(Color.GRAY);
			again.setBackground(Color.WHITE);

		}

		@Override
		public void mouseExited(MouseEvent e) {
			again.setForeground(Color.WHITE);
			again.setBackground(Color.GRAY);
		}

	
	}
}
