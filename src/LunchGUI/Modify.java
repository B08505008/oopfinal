package LunchGUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.MouseEvent;

import javax.swing.*;

import lunch.DB;
import lunch.Lunch;
import lunch.replace;

public class Modify {
	//初始化頁面
	Modify(){
		setPage24();
		setPage34();
		
		panelsus.setLayout(new GridBagLayout());
		Format.setFormat(jcsus);
		Format.setBlackTrans(topic);
		topic.setFont(new Font("微軟正黑體", Font.BOLD, 30));
		for (int i = 0; i < jcsus.length; i++) {
			Format.add(panelsus,jcsus,layoutsus,i);
		}
		
		addListener();
	}
	static replace r;
	
	static JPanel panel24 = new JPanel();
	static JPanel panel34 = new JPanel();
	
	static JButton back24 = new JButton("< 回上頁");
	private JLabel  status24 = new JLabel("更改資料",SwingConstants.CENTER);
	private JLabel  inputlabel24= new JLabel("店     名 :   ",SwingConstants.RIGHT);
	private JTextField input24 = new JTextField();
	private JButton search24 = new JButton("搜尋");
	private JLabel  hint24= new JLabel(" ",SwingConstants.CENTER);
	
	static JButton back34 =  new JButton("< 回上頁");
	private JLabel  status34 =    new JLabel("更改資料",SwingConstants.CENTER);
	private JLabel  name34=       new JLabel("店       名",SwingConstants.CENTER);
	private JLabel  nameErr34=    new JLabel(" ",SwingConstants.CENTER);
	private JLabel  address34=    new JLabel("地       址",SwingConstants.CENTER);
	private JLabel  region34=     new JLabel("區       域",SwingConstants.CENTER);
	private JLabel  type34=       new JLabel("分       類",SwingConstants.CENTER);
	private JLabel  price34=      new JLabel("最低價格",SwingConstants.CENTER);
	private JLabel  dollar34=      new JLabel("元",SwingConstants.CENTER);
	private JLabel  rate34=       new JLabel("評       價",SwingConstants.CENTER);
	private JLabel  date34=       new JLabel("營業日期",SwingConstants.CENTER);
	private JButton  modify34=       new JButton("更改店家資訊");
	private JLabel  hint34=       new JLabel(" ",SwingConstants.CENTER);
	
	private JTextField nameInput34 = new JTextField();
	private JTextField addressInput34 = new JTextField();
	private JComboBox  regionInput34 = new JComboBox();
	private JTextField priceInput34 = new JTextField();
	private JComboBox  rateInput34 = new JComboBox();
	private JComboBox  typeInput341 = new JComboBox();
	private JComboBox  typeInput342 = new JComboBox();
	private JComboBox  typeInput343 = new JComboBox();
	private JCheckBox  dateInput341 = new JCheckBox("一");
	private JCheckBox  dateInput342 = new JCheckBox("二");
	private JCheckBox  dateInput343 = new JCheckBox("三");
	private JCheckBox  dateInput344 = new JCheckBox("四");
	private JCheckBox  dateInput345 = new JCheckBox("五");
	private JCheckBox  dateInput346 = new JCheckBox("六");
	private JCheckBox  dateInput347 = new JCheckBox("日");
	private JCheckBox[] dateInput = {dateInput341,dateInput342,dateInput343,dateInput344,
							dateInput345,dateInput346,dateInput347};
	
	static JPanel panelsus = new JPanel();
	static JLabel topic = new JLabel("更改成功！", SwingConstants.CENTER);
	static JButton addsus = new JButton("繼續更改");
	static JButton backsus = new JButton("返回首頁");
	
	private JComponent[] jcsus = { topic,addsus,backsus	};
	
	private int layoutsus[][] = {{ 0, 1, 6, 1, 0, 0, Format.fill[3], Format.anchor[0],1,1,0,0,10,0},
			
			   { 0, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],40,1 ,0,0,0,0}, 
			   { 3, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],40,1 ,0,0,0,0}};
	
	private JComponent[] jc24 = {back24,status24 ,inputlabel24,input24,search24,hint24};
	
	private JComponent[] jc34 = {back34,status34 ,
								name34,nameInput34,
								address34,addressInput34,
								region34, regionInput34,
								type34,typeInput341,
								rate34,rateInput34,
								price34,priceInput34,dollar34,
								date34,dateInput341,dateInput342,dateInput343,dateInput344,dateInput345,dateInput346,dateInput347
								,hint34,modify34 };
	private int layout24[][] = {
			{ 0, 0, 3, 1, 0, 0, Format.fill[3], Format.anchor[5] ,1,1,0,0,10,2}, { 3, 0, 40, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,10,0},
			{ 0, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,8,0},{ 3, 1, 30, 1, 0, 0, Format.fill[0], Format.anchor[5] ,200,1,0,0,0,0},{ 33, 1, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1,0,0,0,0 },
			{ 0, 2, 43, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,0,2}};
 

	private int layout34[][] = {{ 0, 0, 3, 1, 0, 0, Format.fill[3], Format.anchor[5] ,1,1,0,0,10,2}, { 3, 0, 9, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,10,0},
			{ 0, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 1, 9, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,8,0,0,5,0},
			{ 0, 2, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 2, 9, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,8,0,0,5,0},
			{ 0, 3, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,3,2}, { 3, 3, 9, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 0, 4, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 4, 9, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},//{ 6, 4, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 9, 4, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 0, 5, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 5, 9, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},
			{ 0, 6, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 6, 3, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,8,0,0,5,0},{ 6, 6, 1, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},
			{ 0, 7, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 7, 1, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},{ 4, 7, 1, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},{ 5, 7, 1, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0}
			,{ 6, 7, 1, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},{ 7, 7, 1, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},{ 8, 7, 1, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},{ 9, 7, 1, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0}
			,{ 0, 8, 7, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},{ 7, 8, 3, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0}};

	//初始化第一頁
	public void setPage24() {
		panel24.setLayout(new GridBagLayout());
		Format.setFormat(jc24);
		inputlabel24.setForeground(Color.BLACK);
		inputlabel24.setBackground(null);
		
		for (int i = 0; i < jc24.length; i++) {
			Format.add(panel24,jc24,layout24,i);
		}
	}
	//初始化第二頁		
	public void setPage34() {
		panel34.setLayout(new GridBagLayout());
		Format.setFormat(jc34);
		Format.setBlackTrans(name34);
		Format.setBlackTrans(region34);
		Format.setBlackTrans(address34);
		Format.setBlackTrans(type34);
		Format.setBlackTrans(date34);
		Format.setBlackTrans(rate34);
		Format.setBlackTrans(price34);
		Format.setBlackTrans(dollar34);
		Format.setBlackTrans(hint34);
		Format.setRedTrans(hint24);
		hint34.setForeground(Color.RED);
		setRegionInput34();
		setRateInput34();
		setTypeInput34();
		for (int i = 0; i < jc34.length; i++) {
			Format.add(panel34,jc34,layout34,i);
		}
	}
	//初始化地區選單
	public void setRegionInput34() {
		for(int i = 0; i< LunchGUI2.regionOption.length;i++) {
			regionInput34.addItem(LunchGUI2.regionOption[i]);
		}
	}
	//初始化評價選單
	public void setRateInput34() {
		for(int i = 0; i< LunchGUI2.rateOption.length;i++) {
			rateInput34.addItem(LunchGUI2.rateOption[i]);
		}
	}
	//初始化種類選單
	public void setTypeInput34() {
		typeInput341.addItem("分類一");
		for(int i = 0; i< LunchGUI2.typeOption.length;i++) {
			typeInput341.addItem(LunchGUI2.typeOption[i]);
		}
	}
	//加入所有監聽器
	public void addListener() {
		search24.addMouseListener(new Search24Listener());
		back24.addMouseListener(new Back24Listener());
		back34.addMouseListener(new Back34Listener());
		modify34.addMouseListener(new Modify34Listener());	
		backsus.addMouseListener(new BacksusListener());
		addsus.addMouseListener(new AddsusListener());
	}
	//整頁重整
	public void allReset() {
		nameInput34.setText("");
		addressInput34.setText("");
		regionInput34.setSelectedIndex(0);
		typeInput341.setSelectedIndex(0);
		rateInput34.setSelectedIndex(0);
		priceInput34.setText("");
		dateInput341.setSelected(false);
		dateInput342.setSelected(false);
		dateInput343.setSelected(false);
		dateInput344.setSelected(false);
		dateInput345.setSelected(false);
		dateInput346.setSelected(false);
		dateInput347.setSelected(false);
		hint34.setText(" ");
		input24.setText("");
		hint24.setText(" ");
	}
	
	//按了回上頁、重整頁面
	class Back24Listener extends BackListener{
		
		@Override
		public void mouseClicked(MouseEvent e) {
			allReset();
			panel24.setVisible(false);
			HomePage.panel1.setVisible(true);
		}
		
		
	}
	//根據搜尋結果決定是否跳至下一頁	
	class Search24Listener extends AllListener{
		
		@Override
		public void mouseClicked(MouseEvent e) {
			r = new replace();
			String name = input24.getText();
			boolean exist = DB.exist(name);
			if(exist) {
				r.check(name);
				panel24.setVisible(false);
				panel34.setVisible(true);
				System.out.println(r.saveID);
				nameInput34.setText(name);
				
				Lunch origin = DB.search(r.saveID)[0];
				nameInput34.setText(origin.name);
				addressInput34.setText(origin.address);
				regionInput34.setSelectedItem(origin.place);
				rateInput34.setSelectedItem(origin.rate);
				priceInput34.setText(Integer.toString(origin.cost));
				typeInput341.setSelectedItem(origin.typ);
				
				for(int i = 0; i<dateInput.length;i++) {
					if(origin.open[i]=='1') {
						dateInput[i].setSelected(true);
					}
				}
				
				
			}else {
				r = null;
				hint24.setText(name.equals("")?"請輸入店名":"店名不存在");
			}
			
		}
		
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setWhiteGray(search24);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setGrayWhite(search24);
		}
	}
	//按了回上頁、重整頁面
	class Back34Listener extends BackListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			panel34.setVisible(false);
			allReset();
			panel24.setVisible(true);
		}
	}
	//如未完整填寫，輸出提示；完整填寫則修改資料
	class Modify34Listener extends AllListener {
		@Override
		public void mouseClicked(MouseEvent e) {
			if(nameInput34.getText().equals("")) {
				hint34.setText("請輸入店名");
			}
			else if(addressInput34.getText().equals("")) {
				hint34.setText("請輸入地址");
			}
			else if(priceInput34.getText().equals("")) {
				hint34.setText("請輸入平均價格");
			}
			else if(dateInput341.isSelected()==false&&dateInput342.isSelected()==false&&dateInput343.isSelected()==false&&dateInput344.isSelected()==false&&
					dateInput345.isSelected()==false&&dateInput346.isSelected()==false&&dateInput347.isSelected()==false) {
				hint34.setText("請至少選擇一個營業日");
			}
			else {
				
				
				String name = nameInput34.getText();
				String address = addressInput34.getText();
				String region = (String) regionInput34.getSelectedItem();
				int rate = (int)rateInput34.getSelectedItem();
				int price = Integer.parseInt(priceInput34.getText());
				String type = (String)typeInput341.getSelectedItem();
				Boolean mon = dateInput341.isSelected();
				Boolean tue = dateInput342.isSelected();
				Boolean wed = dateInput343.isSelected();
				Boolean thu = dateInput344.isSelected();
				Boolean fri = dateInput345.isSelected();
				Boolean sat = dateInput346.isSelected();
				Boolean sun = dateInput347.isSelected();
				Boolean[] date = {mon,tue,wed,thu,fri,sat,sun};
				char [] open = {'1','1','1','1','1','1','1'};
				System.out.println(name+" "+address+" "+region+" "+rate+ " "+price+" "+type);
				
				
				for(int i = 0; i < date.length ; i++) {
					if(!date[i]) {
						open[i]='0';
						
					}
					System.out.println(open[i]);
				}
		
				Lunch update = new Lunch(name,address,region,rate,price,type,open);
				
				r.idUpdate(update);
				
				r=null;
				
				panel34.setVisible(false);
				panelsus.setVisible(true);
				
			}
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setWhiteGray(modify34);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setGrayWhite(modify34);
		}
	}
	//繼續修改其他資料
	class AddsusListener extends AllListener{
		@Override
		public void mouseClicked(MouseEvent e) {
			allReset();
			panelsus.setVisible(false);
			panel24.setVisible(true);
		}
		
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setWhiteGray(addsus);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setGrayWhite(addsus);
		}
	}
	//按了回上頁、重整頁面
	class BacksusListener extends BackListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			allReset();
			panelsus.setVisible(false);
			HomePage.panel1.setVisible(true);
		}
	}
	
}
