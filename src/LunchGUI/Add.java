package LunchGUI;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;

import HSR.DB;
import lunch.Lunch;


public class Add {
	
	//將所有物件設定好並加入頁面
	Add(){
		panel33.setLayout(new GridBagLayout());
		Format.setFormat(jc33);
		panelsus.setLayout(new GridBagLayout());
		Format.setFormat(jcsus);
		
		Format.setBlackTrans(name33);
		Format.setBlackTrans(region33);
		Format.setBlackTrans(address33);
		Format.setBlackTrans(type33);
		Format.setBlackTrans(date33);
		Format.setBlackTrans(rate33);
		Format.setBlackTrans(price33);
		Format.setBlackTrans(dollar33);
		Format.setBlackTrans(hint33);
		hint33.setForeground(Color.RED);
		Format.setBlackTrans(topic);
		topic.setFont(new Font("微軟正黑體", Font.BOLD, 30));
		setRegionInput33();
		setRateInput33();
		setTypeInput33();
		addListener();
		for (int i = 0; i < jc33.length; i++) {
			Format.add(panel33,jc33,layout33,i);
		}
		for (int i = 0; i < jcsus.length; i++) {
			Format.add(panelsus,jcsus,layoutsus,i);
		}
	}
	static JPanel panel33 = new JPanel();
	static JButton back33 =      new JButton("< 回上頁");
	private JLabel  status33 =    new JLabel("新增店家",SwingConstants.CENTER);
	private JLabel  name33=       new JLabel("店       名",SwingConstants.CENTER);
	private JLabel  nameErr33=    new JLabel(" ",SwingConstants.CENTER);
	private JLabel  address33=    new JLabel("地       址",SwingConstants.CENTER);
	private JLabel  region33=     new JLabel("區       域",SwingConstants.CENTER);
	private JLabel  type33=       new JLabel("分       類",SwingConstants.CENTER);
	private JLabel  price33=      new JLabel("最低價格",SwingConstants.CENTER);
	private JLabel  dollar33=      new JLabel("元",SwingConstants.CENTER);
	private JLabel  rate33=       new JLabel("評       價",SwingConstants.CENTER);
	private JLabel  date33=       new JLabel("營業日期",SwingConstants.CENTER);
	private JButton  add33=       new JButton("新增至資料庫");
	private JLabel  hint33=       new JLabel(" ",SwingConstants.CENTER);
	
	private JTextField nameInput33 = new JTextField();
	private JTextField addressInput33 = new JTextField();
	private JComboBox  regionInput33 = new JComboBox();
	private JTextField priceInput33 = new JTextField();
	private JComboBox  rateInput33 = new JComboBox();
	private JComboBox  typeInput331 = new JComboBox();
	private JCheckBox  dateInput331 = new JCheckBox("一");
	private JCheckBox  dateInput332 = new JCheckBox("二");
	private JCheckBox  dateInput333 = new JCheckBox("三");
	private JCheckBox  dateInput334 = new JCheckBox("四");
	private JCheckBox  dateInput335 = new JCheckBox("五");
	private JCheckBox  dateInput336 = new JCheckBox("六");
	private JCheckBox  dateInput337 = new JCheckBox("日");
	
	static JPanel panelsus = new JPanel();
	static JLabel topic = new JLabel("新增成功！", SwingConstants.CENTER);
	static JButton addsus = new JButton("繼續新增");
	static JButton backsus = new JButton("返回首頁");
	
	private JComponent[] jc33 = {back33,status33 ,
			 name33,nameInput33,
			 address33,addressInput33,
			 region33, regionInput33,
			 type33,typeInput331,
			 rate33,rateInput33,
			 price33,priceInput33,dollar33,
			 date33,dateInput331,dateInput332,dateInput333,dateInput334,dateInput335,dateInput336,dateInput337
			 ,hint33,add33 };
	
	private JComponent[] jcsus = { topic,addsus,backsus	};
	
	private int layoutsus[][] = {{ 0, 1, 6, 1, 0, 0, Format.fill[3], Format.anchor[0],1,1,0,0,10,0},
			
			   { 0, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],40,1 ,0,0,0,0}, 
			   { 3, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],40,1 ,0,0,0,0}};
	
	private int layout33[][] = {{ 0, 0, 3, 1, 0, 0, Format.fill[3], Format.anchor[5] ,1,1,0,0,10,2}, { 3, 0, 9, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,10,0},
			{ 0, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 1, 9, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,8,0,0,5,0},
			{ 0, 2, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 2, 9, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,8,0,0,5,0},
			{ 0, 3, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,3,2}, { 3, 3, 9, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 0, 4, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 4, 9, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},//{ 6, 4, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 9, 4, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 0, 5, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 5, 9, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},
			{ 0, 6, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 6, 3, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,8,0,0,5,0},{ 6, 6, 1, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},
			{ 0, 7, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 7, 1, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},{ 4, 7, 1, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},{ 5, 7, 1, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0}
			,{ 6, 7, 1, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},{ 7, 7, 1, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},{ 8, 7, 1, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},{ 9, 7, 1, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0}
			,{ 0, 8, 7, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},{ 7, 8, 3, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0}};
	
	
	//將所有區域加入選項
	public void setRegionInput33() {
		for(int i = 0; i< LunchGUI2.regionOption.length;i++) {
			regionInput33.addItem(LunchGUI2.regionOption[i]);
		}
	}
	//將所有評價加入選項
	public void setRateInput33() {
		for(int i = 0; i< LunchGUI2.rateOption.length;i++) {
			rateInput33.addItem(LunchGUI2.rateOption[i]);
		}
	}
	//將所有種類加入選項
	public void setTypeInput33() {
		for(int i = 0; i< LunchGUI2.typeOption.length;i++) {
			typeInput331.addItem(LunchGUI2.typeOption[i]);
		}
	}
	//整頁重整
	public void allReset() {
		nameInput33.setText("");
		addressInput33.setText("");
		regionInput33.setSelectedIndex(0);
		typeInput331.setSelectedIndex(0);
		rateInput33.setSelectedIndex(0);
		priceInput33.setText("");
		dateInput331.setSelected(false);
		dateInput332.setSelected(false);
		dateInput333.setSelected(false);
		dateInput334.setSelected(false);
		dateInput335.setSelected(false);
		dateInput336.setSelected(false);
		dateInput337.setSelected(false);
		hint33.setText(" ");
	}
	//將監聽器加入按鈕
	public void addListener() {
		back33.addMouseListener(new Back33Listener());
		add33.addMouseListener(new Add33Listener());	
		backsus.addMouseListener(new BacksusListener());
		addsus.addMouseListener(new AddsusListener());
	}
	//回上頁並重整頁面
	class Back33Listener extends BackListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			allReset();
			panel33.setVisible(false);
			HomePage.panel1.setVisible(true);
		}
	}
	//回上頁並重整頁面
	class BacksusListener extends BackListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			allReset();
			panelsus.setVisible(false);
			HomePage.panel1.setVisible(true);
		}
	}
	//如未完整填寫則輸出提示；完整填寫則加入店家
	class Add33Listener extends AllListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			if(nameInput33.getText().equals("")) {
				hint33.setText("請輸入店名");
			}
			else if(addressInput33.getText().equals("")) {
				hint33.setText("請輸入地址");
			}
			else if(priceInput33.getText().equals("")) {
				hint33.setText("請輸入平均價格");
			}
			else if(dateInput331.isSelected()==false&&dateInput332.isSelected()==false&&dateInput333.isSelected()==false&&dateInput334.isSelected()==false&&
					dateInput335.isSelected()==false&&dateInput336.isSelected()==false&&dateInput337.isSelected()==false) {
				hint33.setText("請至少選擇一個營業日");
			}
			else {
				hint33.setText(" ");
				
				String name = nameInput33.getText();
				String address = addressInput33.getText();
				String region = (String) regionInput33.getSelectedItem();
				int rate = (int)rateInput33.getSelectedItem();
				int price = Integer.parseInt(priceInput33.getText());
				String type = (String)typeInput331.getSelectedItem();
				Boolean mon = dateInput331.isSelected();
				Boolean tue = dateInput332.isSelected();
				Boolean wed = dateInput333.isSelected();
				Boolean thu = dateInput334.isSelected();
				Boolean fri = dateInput335.isSelected();
				Boolean sat = dateInput336.isSelected();
				Boolean sun = dateInput337.isSelected();
				Boolean[] date = {mon,tue,wed,thu,fri,sat,sun};
				char [] open = {'1','1','1','1','1','1','1'};
				System.out.println(name+" "+address+" "+region+" "+rate+ " "+price+" "+type);
				
				
				for(int i = 0; i < date.length ; i++) {
					if(!date[i]) {
						open[i]='0';
						
					}
					System.out.println(open[i]);
				}
		
				Lunch insert = new Lunch(name,address,region,rate,price,type,open);
				//lunch.DB.init();
				Boolean sus = lunch.DB.insert(insert);
				System.out.println(sus);
				Lunch[] rs=lunch.DB.search();//印出所有資料
				for(Lunch c:rs) {
					System.out.println(c.output());
				}
				
				if(sus==true) {
					panel33.setVisible(false);
					panelsus.setVisible(true);
					
				}
			}
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setWhiteGray(add33);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setGrayWhite(add33);
		}
	}
	//繼續加入
	class AddsusListener extends AllListener{
		@Override
		public void mouseClicked(MouseEvent e) {
			allReset();
			panelsus.setVisible(false);
			panel33.setVisible(true);
		}
		
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setWhiteGray(addsus);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setGrayWhite(addsus);
		}
	}
}

