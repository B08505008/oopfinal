package LunchGUI;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class BackListener extends AllListener {

	//滑鼠進入反白
	@Override
	public void mouseEntered(MouseEvent e) {
		Format.setWhiteGray(NameSearch.back31);
		Format.setWhiteGray(ConditionSearch.back32);
		Format.setWhiteGray(Add.back33);
		Format.setWhiteGray(Add.backsus);
		Format.setWhiteGray(Modify.back24);
		Format.setWhiteGray(Modify.back34);
		Format.setWhiteGray(Modify.backsus);
		Format.setWhiteGray(Delete.back31);
		Format.setWhiteGray(Delete.backsus);
		Format.setWhiteGray(RandomResult.back41);
		Format.setWhiteGray(ConditionResult.back41);
		Format.setWhiteGray(NameSearchResult.back41);
	}

	//滑鼠退出恢復
	@Override
	public void mouseExited(MouseEvent e) {
		Format.setGrayWhite(NameSearch.back31);
		Format.setGrayWhite(ConditionSearch.back32);
		Format.setGrayWhite(Add.back33);
		Format.setGrayWhite(Add.backsus);
		Format.setGrayWhite(Modify.back24);
		Format.setGrayWhite(Modify.back34);
		Format.setGrayWhite(Modify.backsus);
		Format.setGrayWhite(Delete.back31);
		Format.setGrayWhite(Delete.backsus);
		Format.setGrayWhite(RandomResult.back41);
		Format.setGrayWhite(ConditionResult.back41);
		Format.setGrayWhite(NameSearchResult.back41);
	}

}
