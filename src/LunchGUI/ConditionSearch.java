package LunchGUI;

import lunch.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class ConditionSearch {
	static JPanel panel32 = new JPanel();
	static JButton back32 = new JButton("< 回上頁");
	static JLabel  status32 = new JLabel("條件搜尋",SwingConstants.CENTER);
	static JLabel region32 =  new JLabel("區域",SwingConstants.CENTER);
	
	
	static  JCheckBox backDoor32 = new JCheckBox("118巷");
	static  JCheckBox frontDoor32 = new JCheckBox("公館商圈");
	static  JCheckBox lane32 = new JCheckBox("新南/溫州街");
	static  JCheckBox inside32 = new JCheckBox("校內餐廳");
	static  JCheckBox otherRegion32 = new JCheckBox("其他");
	static JCheckBox[] regionArr = {inside32,frontDoor32,lane32,backDoor32,otherRegion32};
	
	
	static  JLabel  type32= new JLabel("分類",SwingConstants.CENTER);
	static  JCheckBox[] typeArr = new JCheckBox[LunchGUI2.typeOption.length];
	
	static  JLabel  price32= new JLabel("最高預算",SwingConstants.CENTER);
	static  JTextField  priceInput32= new JTextField();
	static  JLabel dollar32 =  new JLabel("元");
	static  JCheckBox day32 = new JCheckBox("限今日有營業");
	static  JLabel 	rate32= new JLabel("最低評價",SwingConstants.CENTER);
	static 	JComboBox rateInput32 = new JComboBox();
	static  int[] rateOption32 = {5,4,3,2,1};
	static  JCheckBox five32 = new JCheckBox("5");
	static  JCheckBox four32 = new JCheckBox("4");
	static  JCheckBox three32 = new JCheckBox("3");
	static JCheckBox two32 = new JCheckBox("2");
	static  JCheckBox one32 = new JCheckBox("1");
	static  JTextField input32 = new JTextField();
	static  JLabel hint32 = new JLabel(" ");
	static  JLabel hint321 = new JLabel("註：單項未選擇視同全選",SwingConstants.LEFT);
	static  JButton search32 = new JButton("送出");
	
	static JComponent[] jc32 = {back32,status32,
			 region32,   type32,           price32,   
			 inside32,          priceInput32, dollar32,
			 frontDoor32,       rate32,
			 								
			 lane32,                       rateInput32, 
			 backDoor32,                   day32,
			 otherRegion32,              
			 hint321                               ,search32,
			 hint32};
	static int layout32[][] = {
			{ 0, 0, 1, 1, 0, 0, Format.fill[3], Format.anchor[5] ,1,1,0,0,5,1}, { 1, 0, 40, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 0, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],60,1,0,0,0,1 },{ 3, 1, 6, 1, 0, 0, Format.fill[0],Format.anchor[0] ,60,10,0,0,0,1},																	{ 9, 1, 3, 1, 0, 0, Format.fill[0], Format.anchor[0],60,1,0,0,0,1},
			{ 0, 2, 3, 1,0 , 0, Format.fill[3], Format.anchor[5],1,1 ,0,0,0,0}, 																	{ 9, 2, 2, 1, 0, 0, Format.fill[0], Format.anchor[0],90,1,5,5,0,0},{ 11, 2, 1, 1, 0, 0, Format.fill[3], Format.anchor[0],1,1,0,0,0,0},
			{ 0, 3, 3, 1,0 , 0, Format.fill[3], Format.anchor[5],1,1 ,0,0,0,0}, 																	    { 9, 3, 3, 1, 0, 0, Format.fill[0], Format.anchor[0],1,10,5,0,0,0},
			{ 0, 4, 3, 1,0 , 0, Format.fill[3], Format.anchor[5],1,1 ,0,0,0,0}, 																															            { 9, 4, 3, 1, 0, 0, Format.fill[0], Format.anchor[0],1,1,3,0,0,0},
			{ 0, 5, 3, 1,0 , 0, Format.fill[3], Format.anchor[5],1,1 ,0,0,0,0}, 													 																		            { 9, 5, 3, 1, 0, 0, Format.fill[0], Format.anchor[0],1,1,0,0,0,0},
			{ 0, 6, 3, 1,0 , 0, Format.fill[3], Format.anchor[5],1,1 ,0,0,0,0}, 														 																	
			{ 0, 7, 9, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1,10,0,0,0 },{ 9, 7, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1,0,0,0,0 }
			,{ 0, 8, 12, 1, 0, 0, Format.fill[0], Format.anchor[0],1,1,0,0,0,0 }};
	
	//將所有物件設定好並加入頁面
	ConditionSearch(){
		panel32.setLayout(new GridBagLayout());
		Format.setFormat(jc32);
		Format.setBlackTrans(dollar32);
		Format.setBlackTrans(hint321);
		hint32.setForeground(Color.RED);
		hint32.setOpaque(false);
		setRateInput32();
		setTypeInput32();
		for (int i = 0; i < jc32.length; i++) {
			Format.add(panel32,jc32,layout32,i);
		}
		allReset();
		addListener();
	}
	//將所有評價加入選項
	void setRateInput32() {
		for(int i = 0; i<rateOption32.length;i++) {
			rateInput32.addItem(rateOption32[i]);
		}
	}
	//加入單一勾選格至頁面，幫助setTypeInput32()
	void setTypeLayout(String s,int i ) {
		JCheckBox jcb = new JCheckBox(s);
		jcb.setFont(Format.f);
		GridBagConstraints c1 = new GridBagConstraints();
		c1.gridx = (i<5)?3:6;
		c1.gridy = (i<5)?(2+i):(i-3);
		c1.gridwidth = 3;
		c1.gridheight = 1;
		c1.weightx = 0;
		c1.weighty = 0;
		c1.fill = Format.fill[0];
		c1.anchor =(i<5)? Format.anchor[0]: Format.anchor[1];
		c1.ipadx = 1;
		c1.ipady = 1;
		c1.insets = (i<5)?new Insets(0,0,0,1):new Insets(0,15,0,20);
		panel32.add(jcb, c1);
		typeArr[i] = jcb;
	}
	//加入所有勾選格至頁面
	void setTypeInput32() {
		for(int i = 0; i<LunchGUI2.typeOption.length;i++) {
			setTypeLayout(LunchGUI2.typeOption[i],i);
		}
	}
	//重整頁面
	static void allReset() {
		priceInput32.setText("");
		day32.setSelected(false);
		rateInput32.setSelectedIndex(4);
		for(int i = 0;i<typeArr.length;i++) {
			typeArr[i].setSelected(false);
		}
		for(int i = 0;i<regionArr.length;i++) {
			regionArr[i].setSelected(false);
		}
		hint32.setText(" ");
	}
	//回上頁並重整頁面
	class Back32Listener extends BackListener{
		
		@Override
		public void mouseClicked(MouseEvent e) {
			panel32.setVisible(false);
			HomePage.panel1.setVisible(true);
			allReset();
			ConditionResult.reset();
		}
		
	}
	//依所選條件搜尋
	class Search32Listener extends AllListener{
		

		@Override
		public void mouseClicked(MouseEvent e) {
			String[] region = {"校內餐廳","公館商圈","新南/溫州街","118巷","其他"};
			String[] type = {"小吃","飯類","日式","韓式","火鍋","速食","素食","綜合","其他"};
			int regionNbr = region.length;
			int typeNbr = type.length; 
			
			for(int i = 0; i<regionArr.length;i++) {
				if(!regionArr[i].isSelected()) {
					region[i] = null;
					regionNbr--;
				}
			}		
			
			if(regionNbr == 0) {
				region = LunchGUI2.regionOption;
			}
			
			for(int i = 0; i<typeArr.length;i++) {
				if(!typeArr[i].isSelected()) {
					type[i] = null;
					typeNbr--;
				}
			}
			
			if(typeNbr == 0) {
				type = LunchGUI2.typeOption;
			}
			
			
			int rate = (int) rateInput32.getSelectedItem();
			int price;
			if(priceInput32.getText().equals("")) {
				price = 10000;
			}else {
				price = Integer.parseInt(priceInput32.getText());
			}
			boolean open = day32.isSelected();
			Lunch[] con = DB.filter(region, type, rate, price, open);
			System.out.println(con.length);
			if(con.length>0) {
				ConditionResult.lunch = con;
				panel32.setVisible(false);
				ConditionResult.result(con);
				ConditionResult.panel41.setVisible(true);
				
				String text = ""+con.length ;
				for(int i = 0;i<con.length;i++) {
					text+=" ";
					text+=con[i].name;
				}
				//hint32.setText(text);
			}else {
				hint32.setText("找不到符合條件的店家");
			}
			
			
			
		}
		
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setWhiteGray(search32);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setGrayWhite(search32);
		}
	}
	//加入所有監聽器至按鈕
	public void addListener() {
		back32.addMouseListener(new Back32Listener());
		search32.addMouseListener(new Search32Listener());
	}
}
