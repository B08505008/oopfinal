package LunchGUI;

import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;

import lunch.DB;
import lunch.Lunch;


public class NameSearch {
	static JPanel panel31 = new JPanel();
	static  JButton back31 = new JButton("< 回上頁");
	static JLabel  status31 = new JLabel("店名搜尋",SwingConstants.CENTER);
	static  JLabel  inputlabel31= new JLabel("店     名 :   ",SwingConstants.RIGHT);
	static  JTextField input31 = new JTextField("");
	static  JButton search31 = new JButton("搜尋");
	static JLabel ps31 = new JLabel("註：未輸入將回傳全部店名",SwingConstants.CENTER);
	static JLabel hint31 = new JLabel("                     ",SwingConstants.CENTER);
	static  JComponent[] jc31 = {back31,status31 ,inputlabel31,input31,search31,ps31,hint31};
	static  int layout31[][] = {{ 0, 0, 3, 1, 0, 0, Format.fill[3], Format.anchor[5] ,1,1,0,0,10,2}, { 3, 0, 40, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,10,0},
			{ 0, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,8,0},{ 3, 1, 30, 1, 0, 0, Format.fill[0], Format.anchor[5] ,200,1,0,0,0,0},{ 33, 1, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1,0,0,0,0 }
			,{ 3, 2, 30, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,0,0},{ 3, 3, 30, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,0,0}};
	//初始化頁面
	NameSearch(){
		 panel31.setLayout(new GridBagLayout());
			Format.setFormat(jc31);
			inputlabel31.setForeground(Color.BLACK);
			inputlabel31.setBackground(null);
			Format.setRedTrans(hint31);
			Format.setBlackTrans(ps31);

			for (int i = 0; i < jc31.length; i++) {
				Format.add(panel31,jc31,layout31,i);
			}
			
			addListener();
	 }
	 
	//加入所有監聽器
	public void addListener() {
			
			back31.addMouseListener(new Back31Listener());
			search31.addMouseListener(new Search31Listener ());
		}
	//按了回上頁、重整頁面	
	class Back31Listener extends BackListener{
			
			@Override
			public void mouseClicked(MouseEvent e) {
				panel31.setVisible(false);
				HomePage.panel1.setVisible(true);
				NameSearch.input31.setText("");
				hint31.setText(" ");
			}

		}
	//依輸入進行關鍵字搜尋，搜尋成功則跳至NameSearchResult
	class Search31Listener extends AllListener {

			@Override
			public void mouseClicked(MouseEvent e) {
				String keyword = input31.getText();
				Lunch[] result = DB.KeywordSearch(keyword);
				if (result.length == 0) {
					hint31.setText("找不到相關店名");
				} else {
					hint31.setText(" ");
					NameSearchResult.result(result);
					NameSearch.panel31.setVisible(false);
					NameSearchResult.panel41.setVisible(true);
				}
			  
				
			}

			
			@Override
			public void mouseEntered(MouseEvent e) {
				search31.setForeground(Color.GRAY);
				search31.setBackground(Color.WHITE);

			}

			@Override
			public void mouseExited(MouseEvent e) {
				search31.setForeground(Color.WHITE);
				search31.setBackground(Color.GRAY);
			}

			
		}
}
