package LunchGUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import lunch.Lunch;

public class ConditionResult {
	
	
	ConditionResult(){
		this.setPage41();
		addListener();
	}
	
	static JPanel panel41 = new JPanel();
	static JButton back41 = new JButton("< 回上頁");
	static Lunch[] lunch;
	
	static JLabel[] arr;
	
	static JLabel  status41 = new JLabel("搜尋結果",SwingConstants.CENTER);
	static JLabel  name41 = new JLabel("店名",SwingConstants.CENTER);
	static JLabel  region41 = new JLabel("區域",SwingConstants.CENTER);
	static JLabel  type41 = new JLabel("分類",SwingConstants.CENTER);
	static JLabel  price41 =new JLabel("最低價格",SwingConstants.CENTER);
	static JLabel  rate41 =new JLabel("評價",SwingConstants.CENTER);
	static JLabel  open41 =new JLabel("休息日",SwingConstants.CENTER);
	static JLabel  address41 =new JLabel("地址",SwingConstants.CENTER);
	
	static JComponent[] jc41 = {back41,status41,name41,region41,type41,price41,rate41,open41,address41};
	
	static int layout41 [][] ={{ 0, 0, 3, 1, 0, 0, Format.fill[2], Format.anchor[5],1,1 ,0,0,5,1}, 
			{ 3, 0, 18, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,5,1}, 
			{ 0, 1, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],100,1 ,0,0,0,1}, 
			{ 3, 1, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],50,1 ,0,0,0,1}, 
			{ 6, 1, 3, 1, 0, 0, Format.fill[0],Format. anchor[5],50,1 ,0,0,0,1}, 
			{ 9, 1, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],20,1 ,0,0,0,1}, 
			{ 12, 1, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],30,1 ,0,0,0,1}, 
			{ 15, 1, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],30,1 ,0,0,0,1}, 
			{ 18, 1, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],350,1 ,0,0,0,1}, 
			};
	
	//將所有物件設定好並加入頁面
	static void setPage41() {
		panel41.setLayout(new GridBagLayout());
		Format.setFormat(jc41);
		for (int i = 0; i < jc41.length; i++) {
			Format.add(panel41,jc41,layout41,i);
		}
		
	}
	//幫助result()用，將單一標籤設定資訊，加入頁面
	static void setResultLayout(String s,int j,int i ) {
		JLabel jl = new JLabel(s,SwingConstants.CENTER);
		Format.setWhiteBlack(jl);
		GridBagConstraints c1 = new GridBagConstraints();
		c1.gridx = 3*i;
		c1.gridy = j+2;
		c1.gridwidth = 3;
		c1.gridheight = 1;
		c1.weightx = 0;
		c1.weighty = 0;
		c1.fill = Format.fill[0];
		c1.anchor =Format.anchor[5];
		c1.ipadx = 1;
		c1.ipady = 1;
		c1.insets = new Insets(0,0,2,1);
		panel41.add(jl, c1);
	}
	//輸入Lunch陣列，將所有資訊加入頁面
	static void result(Lunch[] l) {
		for(int j = 0 ; j < l.length ; j++) {
			String[] s = {l[j].name,l[j].place,l[j].typ,Integer.toString(l[j].cost),Integer.toString(l[j].rate),Format.openToString(l[j].open),l[j].address};
			for(int i = 0 ; i <s.length  ; i++) {
				setResultLayout(s[i],j,i );
			}
		}
	}
	//重整頁面
	static void reset() {
		int size = panel41.getComponentCount();
		for(int i = size-1; i>8;i-- ) {
			panel41.remove(i);
		}
	}
	//加入監聽器至按鈕
	public void addListener() {
		back41.addMouseListener(new Back41Listener());
	
	}
	//回上頁並重整頁面
	class Back41Listener extends BackListener{
		@Override
		public void mouseClicked(MouseEvent e) {
			panel41.setVisible(false);
			reset();
			ConditionSearch.panel32.setVisible(true);
		}
	}
}
