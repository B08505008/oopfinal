package LunchGUI;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

public class LunchGUI2 {
	
	//初始化所有頁面
	public LunchGUI2() {
		frame.setSize(1000, 450);
		frame.setLayout(new GridBagLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
				
		frame.add(p1.panel1);
		frame.add(p31.panel31);
		frame.add(p32.panel32);
		frame.add(p33.panel33);
		frame.add(p33.panelsus);
		frame.add(p2434.panel24);
		frame.add(p2434.panel34);
		frame.add(p2434.panelsus);
		frame.add(p43.panel31);
		frame.add(p43.panelsus);
		frame.add(p44.panel41);
		frame.add(p45.panel41);
		frame.add(p46.panel41);
		p31.panel31.setVisible(false);
		p32.panel32.setVisible(false);
		p33.panel33.setVisible(false);
		p33.panelsus.setVisible(false);
		p2434.panel24.setVisible(false);
		p2434.panel34.setVisible(false);
		p2434.panelsus.setVisible(false);
		p43.panel31.setVisible(false);
		p43.panelsus.setVisible(false);
		p44.panel41.setVisible(false);
		p45.panel41.setVisible(false);
		p46.panel41.setVisible(false);
		
		p1.panel1.setVisible(true);
		frame.setVisible(true);
	}
	
	private JFrame frame = new JFrame("今天午餐要吃什麼好呢");
	
	private HomePage p1 = new HomePage();
	private NameSearch p31 = new NameSearch();
	private ConditionSearch p32 = new ConditionSearch();
	private Add p33 = new Add();
	private Modify p2434 = new Modify();
	private Delete p43 = new Delete();
	private RandomResult p44 = new RandomResult();
	private ConditionResult p45 = new ConditionResult();
	private NameSearchResult p46 = new NameSearchResult();
	
	static final String[] regionOption = {"校內餐廳","公館商圈","新南/溫州街","118巷","其他"};
	static final  int[] rateOption= {5,4,3,2,1};
	static final String[] typeOption = {"小吃","飯類","日式","韓式","火鍋",
			                      "速食","素食","綜合","其他"};
	
	
}
