package LunchGUI;

import lunch.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;

import lunch.Lunch;

public class HomePage {
	static JPanel panel1 = new JPanel();
	static JLabel topic = new JLabel("人生好難，想午餐更難", SwingConstants.CENTER);
	static JButton search = new JButton("我要找午餐");
	static JButton data = new JButton("我要改資料");
	static JButton shopName = new JButton("依店名搜尋");
	static JButton condition = new JButton("依條件搜尋");
	static JButton random = new JButton("幫我想一個");
	static JButton add = new JButton("新增店家");
	static JButton modify = new JButton("更改資料");
	static JButton delete = new JButton("刪除店家");
	static JLabel empty = new JLabel(" ", SwingConstants.CENTER);
	static JLabel eempty = new JLabel(" ", SwingConstants.CENTER);

	static JComponent[] jc = {topic,search,data,shopName,condition,random,add,modify,delete,empty };
	int layout[][] = {{ 0, 1, 6, 1, 0, 0, Format.fill[3], Format.anchor[0],1,1,0,0,10,0},
			
			   { 0, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],40,1 ,0,0,0,0}, 
			   { 3, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],40,1 ,0,0,0,0},
			   
			   { 0, 3, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,0,0},
			   { 0, 4, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,0,0},
			   { 0, 5, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,0,0},
			   
			   { 3, 3, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,0,0}, 
			   { 3, 4, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,0,0},
			   { 3, 5, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,0,0},
			   
			   
			   { 0, 5, 1, 1, 0, 0, Format.fill[2], Format.anchor[5],1,1 ,77,0,0,0}};
	//初始化頁面
	HomePage() {
		panel1.setLayout(new GridBagLayout());
		topic.setFont(new Font("微軟正黑體", Font.BOLD, 30));
		Format.setButton(jc);

		for (int i = 0; i < jc.length; i++) {
			Format.add(panel1, jc, layout, i);
		}

		Format.setWhiteGray(shopName);
		Format.setWhiteGray(condition);
		Format.setWhiteGray(random);
		Format.setWhiteGray(add);
		Format.setWhiteGray(modify);
		Format.setWhiteGray(delete);

		shopName.setVisible(false);
		condition.setVisible(false);
		random.setVisible(false);
		eempty.setVisible(false);
		add.setVisible(false);
		modify.setVisible(false);
		delete.setVisible(false);
		
		addListener();
	}
	//加入所有監聽器
	public void addListener() {
		search.addMouseListener(new SearchListener());
		shopName.addMouseListener(new ShopNameListener());
		condition.addMouseListener(new ConditionListener());
		random.addMouseListener(new RandomListener());
		data.addMouseListener(new DataListener());
		add.addMouseListener(new AddListener());
		modify.addMouseListener(new ModifyListener());
		delete.addMouseListener(new DeleteListener());
	
	}
	//滑鼠進入則跳出搜尋選單
	class SearchListener extends AllListener {


		@Override
		public void mouseEntered(MouseEvent e) {
			search.setForeground(Color.GRAY);
			search.setBackground(Color.WHITE);
			empty.setVisible(false);
			shopName.setVisible(true);
			condition.setVisible(true);
			random.setVisible(true);

		}

		@Override
		public void mouseExited(MouseEvent e) {
			search.setForeground(Color.WHITE);
			search.setBackground(Color.GRAY);
			empty.setVisible(true);
			shopName.setVisible(false);
			condition.setVisible(false);
			random.setVisible(false);
		}

		
	}
	//進入店名搜尋
	class ShopNameListener extends SearchListener{
		
		@Override
		public void mouseClicked(MouseEvent e) {
			panel1.setVisible(false);
			NameSearch.panel31.setVisible(true);
		}
		
	}
	//進入條件搜尋
	class ConditionListener extends SearchListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			panel1.setVisible(false);
			ConditionSearch.panel32.setVisible(true);
		}
	}
	//進入隨機搜尋並隨機輸出一個店家
	class RandomListener extends SearchListener {
		@Override
		public void mouseClicked(MouseEvent e) {
			Lunch ran = DB.randompick();
			String name = ran.name;
			String address = ran.address;
			String region = ran.place;
			String rate = Integer.toString(ran.rate);
			String price = Integer.toString(ran.cost) ;
			String type = ran.typ;
			String open = Format.openToString(ran.open);
			
			RandomResult.nameR.setText(name);
			RandomResult.addressR.setText(address);
			RandomResult.regionR.setText(region);
			RandomResult.rateR.setText(rate);
			RandomResult.priceR.setText(price);
			RandomResult.typeR.setText(type);
			RandomResult.openR.setText(open);
			
			panel1.setVisible(false);
			RandomResult.panel41.setVisible(true);
		}
	
	}
	
	//滑出進入則跳出資料選單
	class DataListener extends AllListener {

		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setWhiteGray(data);
			empty.setVisible(false);
			add.setVisible(true);
			modify.setVisible(true);
			delete.setVisible(true);

		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setGrayWhite(data);
			empty.setVisible(true);
			add.setVisible(false);
			modify.setVisible(false);
			delete.setVisible(false);

		}


	}
	//進入新增店家
	class AddListener extends DataListener{
		@Override
		public void mouseClicked(MouseEvent e) {
			Lunch[] rs=lunch.DB.search();//印出所有資料
			System.out.println("現有店家");
			for(Lunch c:rs) {
				System.out.println(c.output());
			}
			panel1.setVisible(false);
			Add.panel33.setVisible(true);
		}
	}
	//進入修改店家
	class ModifyListener extends DataListener{
		@Override
		public void mouseClicked(MouseEvent e) {
			Lunch[] rs=lunch.DB.search();//印出所有資料
			System.out.println("現有店家");
			for(Lunch c:rs) {
				System.out.println(c.output());
			}
			panel1.setVisible(false);
			Modify.panel24.setVisible(true);
		}
	}
	//進入刪除店家
	class DeleteListener extends DataListener{
		@Override
		public void mouseClicked(MouseEvent e) {
			Lunch[] rs=lunch.DB.search();//印出所有資料
			System.out.println("現有店家");
			for(Lunch c:rs) {
				System.out.println(c.output());
			}
			panel1.setVisible(false);
			Delete.panel31.setVisible(true);
		}
	}

}
