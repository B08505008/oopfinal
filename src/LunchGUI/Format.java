package LunchGUI;
import java.awt.*;
import javax.swing.*;

public class Format {
		 static JButton jb = new JButton();
		 static JTextField jtf = new JTextField();
		 static JLabel jl = new JLabel();
		 static JCheckBox jckb = new JCheckBox();
		 static JComboBox jcbb = new JComboBox();
	
		static final int fill[] = { GridBagConstraints.BOTH, GridBagConstraints.VERTICAL, GridBagConstraints.HORIZONTAL,
			GridBagConstraints.NONE };
		static final int anchor[] = { GridBagConstraints.CENTER, GridBagConstraints.EAST, GridBagConstraints.SOUTHEAST,
			GridBagConstraints.SOUTH, GridBagConstraints.SOUTHWEST, GridBagConstraints.WEST,
			GridBagConstraints.NORTHWEST, GridBagConstraints.NORTH, GridBagConstraints.NORTHEAST };
	
		static Font f= new Font("微軟正黑體",Font.BOLD,15);
	
		static void add(JPanel panel,JComponent[] jc , int[][] layout ,int i) {
			GridBagConstraints c1 = new GridBagConstraints();
			c1.gridx = layout[i][0];
			c1.gridy = layout[i][1];
			c1.gridwidth = layout[i][2];
			c1.gridheight = layout[i][3];
			c1.weightx = layout[i][4];
			c1.weighty = layout[i][5];
			c1.fill = layout[i][6];
			c1.anchor = layout[i][7];
			c1.ipadx = layout[i][8];
			c1.ipady = layout[i][9];
			c1.insets = new Insets(layout[i][10],layout[i][11],layout[i][12],layout[i][13]);
			panel.add(jc[i], c1);
		}
		
		static void add(JScrollPane jsp,JComponent[] jc , int[][] layout ,int i) {
			GridBagConstraints c1 = new GridBagConstraints();
			c1.gridx = layout[i][0];
			c1.gridy = layout[i][1];
			c1.gridwidth = layout[i][2];
			c1.gridheight = layout[i][3];
			c1.weightx = layout[i][4];
			c1.weighty = layout[i][5];
			c1.fill = layout[i][6];
			c1.anchor = layout[i][7];
			c1.ipadx = layout[i][8];
			c1.ipady = layout[i][9];
			c1.insets = new Insets(layout[i][10],layout[i][11],layout[i][12],layout[i][13]);
			jsp.add(jc[i], c1);
		}
		
		
		//根據物件類型設定其該有格式
		static void setFormat(JComponent[] jc) {
			setButton(jc);
			setLabel(jc);
			setCheckBox(jc);
			setComboBox(jc);
			setTextField(jc);
		}
		//將字格設為該有格式
		static void setTextField(JComponent[] jc) {
			for (int i = 0; i < jc.length; i++) {
				if(jc[i].getClass()==jtf.getClass()) {
					jc[i].setFont(f);
					
				}
			}
			
		}
		//將按鈕設為該有格式
		static void setButton(JComponent[] jc) {
			for (int i = 0; i < jc.length; i++) {
				if(jc[i].getClass()==jb.getClass()) {
					jc[i].setFont(f);
					jc[i].setForeground(Color.WHITE);
					jc[i].setBackground(Color.GRAY);
					
				}
			}
		}
		//將標籤設為該有格式
		static void setLabel(JComponent[] jc) {
			for (int i = 0; i < jc.length; i++) {
				if(jc[i].getClass()==jl.getClass()) {
					jc[i].setFont(f);
					jc[i].setForeground(Color.WHITE);
					jc[i].setOpaque(true);
					jc[i].setBackground(Color.GRAY);
				}
			}
		}
		//將勾選格設為該有格式
		static void setCheckBox(JComponent[] jc) {
			for (int i = 0; i < jc.length; i++) {
				if(jc[i].getClass()==jckb.getClass()) {
					jc[i].setFont(f);
				}
			}
		}
		//將選單設為該有格式
		static void setComboBox(JComponent[] jc) {
			for (int i = 0; i < jc.length; i++) {
				if(jc[i].getClass()==jcbb.getClass()) {
					jc[i].setFont(f);
					jc[i].setForeground(Color.BLACK);
					jc[i].setOpaque(true);
					jc[i].setBackground(Color.WHITE);
				}
			}
		}
		//將物件設為白底灰字
		static void setWhiteGray(JComponent jc) {
			jc.setForeground(Color.GRAY);
			jc.setBackground(Color.WHITE);
		}
		//將物件設為白底黑字
		static void setWhiteBlack(JComponent jc) {
			jc.setFont(f);
			jc.setOpaque(true);
			jc.setForeground(Color.BLACK);
			jc.setBackground(Color.WHITE);
		}
		//將物件設為灰底白字
		static void setGrayWhite(JComponent jc) {
			jc.setForeground(Color.WHITE);
			jc.setBackground(Color.GRAY);
		}
		//將物件設為透明底黑字
		static void setBlackTrans(JComponent jc) {
			jc.setForeground(Color.BLACK);
			jc.setOpaque(false);
		}
		//將物件設為透明底紅字
		static void setRedTrans(JComponent jc) {
			jc.setForeground(Color.RED);
			jc.setOpaque(false);
		}
		
		static String[] openDay = {"一","二","三","四","五","六","日",};
		static String openToString(char[] open) {
			String close = "";
			for(int i = 0; i<open.length;i++) {
				if(open[i]=='0') {
					close+=openDay[i];
				}
			}
			if(close.equals("")) {
				close = "無";
			}
			return close;
		}
		
		
}
