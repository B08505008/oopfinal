package LunchGUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;

import LunchGUI.Add.AddsusListener;
import LunchGUI.Add.BacksusListener;
import lunch.DB;
import lunch.Lunch;


public class Delete {
	static JPanel panel31 = new JPanel();
	static  JButton back31 = new JButton("< 回上頁");
	static JLabel  status31 = new JLabel("刪除店家",SwingConstants.CENTER);
	static  JLabel  inputlabel31= new JLabel("店     名 :   ",SwingConstants.RIGHT);
	static  JTextField input31 = new JTextField();
	static  JButton search31 = new JButton("刪除");
	static JLabel hint31 = new JLabel(" ",SwingConstants.CENTER);
	static  JComponent[] jc31 = {back31,status31 ,inputlabel31,input31,search31,hint31};
	static  int layout31[][] = {{ 0, 0, 3, 1, 0, 0, Format.fill[3], Format.anchor[5] ,1,1,0,0,10,2}, { 3, 0, 40, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,10,0},
				{ 0, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,8,0},{ 3, 1, 30, 1, 0, 0, Format.fill[0], Format.anchor[5] ,200,1,0,0,0,0},{ 33, 1, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1,0,0,0,0 }
				,{ 0, 2, 43, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,0,0}};
	
	static JPanel panelsus = new JPanel();
	static JLabel topic = new JLabel("刪除成功！", SwingConstants.CENTER);
	static JButton deletesus = new JButton("繼續刪除");
	static JButton backsus = new JButton("返回首頁");
	 
	private JComponent[] jcsus = { topic,deletesus,backsus	};
	
	private int layoutsus[][] = {{ 0, 1, 6, 1, 0, 0, Format.fill[3], Format.anchor[0],1,1,0,0,10,0},
			
			   { 0, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],40,1 ,0,0,0,0}, 
			   { 3, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],40,1 ,0,0,0,0}};
		//將所有物件設定好加入頁面
	 	Delete(){
			panel31.setLayout(new GridBagLayout());
			Format.setFormat(jc31);
			panelsus.setLayout(new GridBagLayout());
			Format.setFormat(jcsus);
			
			inputlabel31.setForeground(Color.BLACK);
			inputlabel31.setBackground(null);
			hint31.setForeground(Color.RED);
			hint31.setOpaque(false);
			
			Format.setBlackTrans(topic);
			topic.setFont(new Font("微軟正黑體", Font.BOLD, 30));

			for (int i = 0; i < jc31.length; i++) {
				Format.add(panel31, jc31, layout31, i);
			}
			
			for (int i = 0; i < jcsus.length; i++) {
				Format.add(panelsus, jcsus, layoutsus, i);
			}

			addListener();
	 }
	 
	 	//加入所有監聽器
		public void addListener() {
			
			back31.addMouseListener(new Back31Listener());
			search31.addMouseListener(new Search31Listener ());
			backsus.addMouseListener(new BacksusListener());
			deletesus.addMouseListener(new AddsusListener());
		}
		
		class Back31Listener extends BackListener{
			
			@Override
		public void mouseClicked(MouseEvent e) {
				allReset();
				panel31.setVisible(false);
				HomePage.panel1.setVisible(true);
			}

		}
		//頁面重整
		public void allReset() {
			input31.setText("");
			hint31.setText(" ");
		}
		//如店名不存在或未輸入則提示；店名存在則刪除店家
		class Search31Listener extends AllListener {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				
				
				if(input31.getText().equals("")) {
					hint31.setText("請輸入店名");
				}
				else if(!DB.exist(input31.getText())) {
					hint31.setText("店名不存在");
				}
				else {
					boolean sus = lunch.DB.remove(input31.getText());
					
					System.out.println("刪除前");
					Lunch[] rs=lunch.DB.search();//印出所有資料
					for(Lunch c:rs) {
						System.out.println(c.output());
					}
					
					if(sus==true) {
						System.out.println("刪除成功");
						panel31.setVisible(false);
						panelsus.setVisible(true);
					}else {
						System.out.println("刪除失敗");
						hint31.setText("刪除失敗");
					}
					
					System.out.println("刪除後");
					Lunch[] rs2=lunch.DB.search();//印出所有資料
					for(Lunch c:rs2) {
						System.out.println(c.output());
					}
				}
			}

			
			@Override
			public void mouseEntered(MouseEvent e) {
				search31.setForeground(Color.GRAY);
				search31.setBackground(Color.WHITE);

			}

			@Override
			public void mouseExited(MouseEvent e) {
				search31.setForeground(Color.WHITE);
				search31.setBackground(Color.GRAY);
			}

			
		}
		//繼續刪除
		class AddsusListener extends AllListener{
			@Override
			public void mouseClicked(MouseEvent e) {
				allReset();
				panelsus.setVisible(false);
				panel31.setVisible(true);
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				Format.setWhiteGray(deletesus);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				Format.setGrayWhite(deletesus);
			}
		}
		//回上頁並重整頁面
		class BacksusListener extends BackListener{

			@Override
			public void mouseClicked(MouseEvent e) {
				allReset();
				panelsus.setVisible(false);
				HomePage.panel1.setVisible(true);
			}
		}
}
