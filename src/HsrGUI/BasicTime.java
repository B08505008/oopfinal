package HsrGUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import HSR.BookException;
import HSR.DB;
import HSR.Order;
import HSR.Ticket;
import HSR.Timing;
import HSR.TrainInfo;
import HsrGUI.AllTrainN.frontL;
public class BasicTime {
	
	BasicTime(){
		panel.setLayout(new GridBagLayout());
		Format.setFormat(jc);
		for (int i = 0; i < jc.length; i++) {
			Format.add(panel,jc,layout,i);
		}
		Format.setBlackTrans(front);
		Format.setBlackTrans(later);
		set();
		addListener();
	}
	
	static GUITrainInfo[] table;
	static int infoNo = 0;
	static int firstIndex = -1;
	static String startStation ;
	static String arrivalStation ;
	static JLabel [][] arr = new JLabel[5][6];
	
	static JPanel panel = new JPanel();
	static JButton back = new JButton("< 回上頁");
	
	static JLabel  status = new JLabel("一般訂票",SwingConstants.CENTER);
	
	static JButton front = new JButton("<<較早車次");
	static JButton later = new JButton("較晚車次>>");
	
	static JLabel infoI = new JLabel(" ", SwingConstants.CENTER);

	static JLabel  trainNbr = new JLabel("車次",SwingConstants.CENTER);
	static JLabel  depart = new JLabel("出發時間",SwingConstants.CENTER);
	static JLabel arrival = new JLabel("抵達時間",SwingConstants.CENTER);
	static JLabel  travel = new JLabel("乘車時間",SwingConstants.CENTER);
	static JLabel discount =new JLabel("全票優惠",SwingConstants.CENTER);
	static JLabel discountC =new JLabel("大學生優惠",SwingConstants.CENTER);
	static JLabel  choose =new JLabel("選擇",SwingConstants.CENTER);
	static JLabel  address =new JLabel("地址",SwingConstants.CENTER);
	static JButton book = new JButton("預定所選車票");
	
	static JCheckBox c1 = new JCheckBox();
	static JCheckBox c2 = new JCheckBox();
	static JCheckBox c3 = new JCheckBox();
	static JCheckBox c4 = new JCheckBox();
	static JCheckBox c5 = new JCheckBox();
	
	static JCheckBox[] c = {c1,c2,c3,c4,c5};
	static JLabel[] no = new JLabel[5];

	
	static JComponent[] jc = {back,status,
							front,
							infoI,
							later,
							trainNbr,depart,arrival,travel,discount,discountC,choose,
							c1,c2,c3,c4,c5,book};
	
	static int layout [][] ={
			{ 0, 0, 3, 1, 0, 0, Format.fill[2], Format.anchor[5],1,1 ,0,0,5,1}, { 3, 0, 18, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,5,1},
			
			{ 0, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			{ 3, 1, 15, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			{ 18, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			{ 0, 7, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,5,1}, 
			{ 3, 7, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],60,1 ,0,0,5,1}, 
			{ 6, 7, 3, 1, 0, 0, Format.fill[0],Format. anchor[5],60,1 ,0,0,5,1}, 
			{ 9, 7, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],60,1 ,0,0,5,1}, 
			{ 12, 7, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],60,1 ,0,0,5,1}, 
			{ 15, 7, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],60,1 ,0,0,5,1}, 
			{ 18, 7, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,5,1}, 
			
			{ 18, 8, 3, 1, 0, 0, Format.fill[3], Format.anchor[0],1,1 ,0,0,5,1}, 
			{ 18, 9, 3, 1, 0, 0, Format.fill[3], Format.anchor[0],1,1 ,0,0,5,1}, 
			{ 18, 10, 3, 1, 0, 0, Format.fill[3], Format.anchor[0],1,1 ,0,0,5,1}, 
			{ 18, 11, 3, 1, 0, 0, Format.fill[3], Format.anchor[0],1,1 ,0,0,5,1}, 
			{ 18, 12, 3, 1, 0, 0, Format.fill[3], Format.anchor[0],1,1 ,0,0,5,1}, 
			{ 18, 13, 3, 1, 0, 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,1}, 
			
			};
	
	
	static void setTable(TrainInfo[] train) { //設置時刻表
		GUITrainInfo[] temp =Format.toGUITrainInfoArray(train);
		for(GUITrainInfo g : temp) {
			System.out.println(g);
		}		
		table = Format.sortByStart(temp);
		for(GUITrainInfo g : table) {
			System.out.println(g);
		}		
	}
	
	static boolean setTrain(Timing t) { //設定各車次資訊
		int nbr = 0;
		GUITrainInfo[] temp;
		startStation = table[0].startingStation;
		arrivalStation = table[0].endingStation;
		for(int i = 0 ; i < table.length ; i++) {
			if(table[i].depatureTimes.get(0).larger(t)) {
				firstIndex = i;
				break;
			}
		}
		System.out.println("table長度 = "+table.length);
		System.out.println("firstIndex = "+firstIndex);
		if(firstIndex == -1 ) {
			return false;
		}else {
			nbr = table.length - firstIndex ;
			infoNo = nbr;
			System.out.println("nbr = "+nbr);
			temp = new GUITrainInfo[nbr];
			int index = 0;
			for (int i = 0; i < table.length; i++) {
				if (table[i].depatureTimes.get(0).larger(t)) {
					temp[index] = table[i];
					index++;
				}
			}
			
			for (int j = 0; j < ((nbr > 5) ? 5 : nbr); j++) {
				String no = temp[j].trainNo;
				Timing startTime = temp[j].staToTime.get(startStation);
				String start = startTime.toString();
				Timing arrivalTime = temp[j].staToTime.get(arrivalStation);
				String arrival = arrivalTime.toString();
				Timing travelTime = arrivalTime.minus(startTime);
				String travel = travelTime.toString();
				String discount = Format.discountToString(temp[j].earlyDiscount, true);
				String discountC = Format.discountToString(temp[j].universityDiscount, false);
				String[] s = { no, start, arrival, travel, discount, discountC };
				for (int k = 0; k < s.length; k++) {
					arr[j][k].setText(s[k]);
				}
			}
			if(table.length<5){
				front.setVisible(false);
				later.setVisible(false);
			}
			return true;
		}
	}
	
	static void earlier() { //將資訊往前五班次
		firstIndex = firstIndex-5;
		if(infoNo<5) {
			firstIndex = table.length -5;
			infoNo = 5;
		}
		
		if(firstIndex < 0) {
			firstIndex = 0;
		}
		
		for (int j = firstIndex; j < firstIndex+5; j++) {
			String no = table[j].trainNo;
			Timing startTime = table[j].staToTime.get(startStation);
			String start = startTime.toString();
			Timing arrivalTime = table[j].staToTime.get(arrivalStation);
			String arrival = arrivalTime.toString();
			Timing travelTime = arrivalTime.minus(startTime);
			String travel = travelTime.toString();
			String discount = Format.discountToString(table[j].earlyDiscount, true);
			String discountC = Format.discountToString(table[j].universityDiscount, false);
			
			String[] s = { no, start, arrival, travel, discount, discountC };
			
			for (int k = 0; k < s.length; k++) {
				arr[j-firstIndex][k].setText(s[k]);
			}
		}
	}
	
	static void later() { //將資訊往後五班次
		firstIndex = firstIndex+5;
		if (infoNo > 4) {

			if (firstIndex > table.length - 5) {
				firstIndex = table.length - 5;
			}

			for (int j = firstIndex; j < firstIndex + 5; j++) {
				String no = table[j].trainNo;
				Timing startTime = table[j].staToTime.get(startStation);
				String start = startTime.toString();
				Timing arrivalTime = table[j].staToTime.get(arrivalStation);
				String arrival = arrivalTime.toString();
				Timing travelTime = arrivalTime.minus(startTime);
				String travel = travelTime.toString();
				String discount = Format.discountToString(table[j].earlyDiscount, true);
				String discountC = Format.discountToString(table[j].universityDiscount, false);

				String[] s = { no, start, arrival, travel, discount, discountC };

				for (int k = 0; k < s.length; k++) {
					arr[j - firstIndex][k].setText(s[k]);
				}
			}
		}
	}
	
	
	static void set() { //加上車次資訊方塊
		for(int j = 0 ; j < 5 ; j++) {
			String[] s = {" "," "," "," "," "," "};
			for(int i = 0 ; i <s.length  ; i++) {
				setLayout(s[i],j,i );
			}
		}
	}
	
	static void setLayout(String s,int j,int i ) { //將單一標籤設定資訊，加入頁面
		JLabel jl = new JLabel(s,SwingConstants.CENTER);
		jl.setFont(Format.f);
		Format.setBlackTrans(jl);
		GridBagConstraints c1 = new GridBagConstraints();
		c1.gridx = 3*i;
		c1.gridy = j+8;
		c1.gridwidth = 3;
		c1.gridheight = 1;
		c1.weightx = 0;
		c1.weighty = 0;
		c1.fill = Format.fill[0];
		c1.anchor =Format.anchor[5];
		c1.ipadx = 1;
		c1.ipady = 1;
		c1.insets = new Insets(0,0,2,1);
		panel.add(jl, c1);
		arr[j][i] = jl;
	}
	

	static void setInfo() { //設定最上方資訊欄內容
		Font f = new Font("微軟正黑體", Font.BOLD, 17);
		infoI.setFont(f);
		Format.setBlackTrans(infoI);
		String info;
		String date = BasicBook.yearI.getSelectedItem()+"/"+BasicBook.monthI.getSelectedItem()+"/"+BasicBook.dayI.getSelectedItem()+" | ";
		String station = (String)BasicBook.startI.getSelectedItem()+" 往 "+(String)BasicBook.stopI.getSelectedItem()+" | ";
		String type = (String)BasicBook.typeI.getSelectedItem()+" | ";
		int normal = BasicBook.normalI.getSelectedIndex();
		int child = BasicBook.childI.getSelectedIndex();
		int old = BasicBook.oldI.getSelectedIndex();
		int love = BasicBook.loveI.getSelectedIndex();
		int college = BasicBook.collegeI.getSelectedIndex();
		int[] tt = {normal,child,old,love,college};
		
		String ticket="";
		
		for(int i = 0;i<HSR_GUI.ticketType.length;i++) {
			if(tt[i]!=0) {
				if(!ticket.equals("")) {
				ticket+="/";
				}
				ticket+= HSR_GUI.ticketType[i]+" "+tt[i];
			}			
		}
		info =  date + station + type + ticket;
		infoI.setText(info);
		
	}
	
	static void reset() { //重整頁面
		for(JLabel[] j : arr) {
			for(JLabel a : j) {
				a.setText(" ");
			}
		}
		firstIndex = -1;
		for(JCheckBox jb : c) {
			jb.setSelected(false);
		}
		front.setVisible(true);
		later.setVisible(true);
	}

	public void addListener() {	//加入所有監聽器
		back.addMouseListener(new BackListener());
		for(int i = 0;i<c.length;i++) {
			c[i].addActionListener(new CL(i));
		}
		front.addMouseListener(new FrontL());
		later.addMouseListener(new LaterL());
		book.addMouseListener(new BookL());
	
	}

	class CL extends AllListener{ //回上頁
		int index;
		CL(int i ){
			index = i;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			for(int i = 0; i < c.length ; i++) {
				if(i==index) {
					continue;
				}else {
					c[i].setSelected(false);
				}
			}
		}
	}
	
	class BackListener extends HsrGUI.BackListener{ //回上頁
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			BasicBook.panel.setVisible(true);
			reset();
		}
	}
	
	class CancelL extends AllListener{ //跳至訂票結果頁面
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setBlackOrange(book);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setOrangeBlack(book);
		}
		@Override
		public void mouseClicked(MouseEvent e) {
			for(int i = 0; i<c.length;i++) {
				if(c[i].isSelected()) {
					
				}
			}
			panel.setVisible(false);
			CancelResult.panel.setVisible(true);
			reset();
		}
	}
	
	class FrontL extends AllListener{ //往前五個車次，若到頂則不動作
		@Override
		public void mouseClicked(MouseEvent e) {
			earlier();
		}
	}
	
	class LaterL extends AllListener{//往後五個車次，若到底則不動作
		@Override
		public void mouseClicked(MouseEvent e) {
			later();
		}
	}
	
	class BookL extends AllListener {
		@Override
		public void mouseClicked(MouseEvent e) {
			String uid = BasicBook.uidI.getText();
			String trainNbr="";
			for(int k = 0; k< c.length ; k++) {
				if(c[k].isSelected()) {
					trainNbr = arr[k][0].getText();
				}
			}
			String start = (String) BasicBook.startI.getSelectedItem();
			String stop = (String) BasicBook.stopI.getSelectedItem();
			String startID = Format.toStationID(start);
			String stopID = Format.toStationID(stop);
			int year = (int) BasicBook.yearI.getSelectedItem();
			int month = (int) BasicBook.monthI.getSelectedItem();
			int day = (int) BasicBook.dayI.getSelectedItem();
			String dateStd = Format.toStdDate(year, month, day);
			boolean std = (BasicBook.typeI.getSelectedIndex() == 0) ? true : false;
			int per = Format.toSeatPre(BasicBook.seatI);
			int[] amounts = Format.toAmounts(BasicBook.ticketI);
			Order o = new Order(uid, trainNbr, startID, stopID, dateStd, std, per, amounts);

			try {
				Ticket[] t = DB.buyticket(o);
				for (Ticket b : t) {
					System.out.println(b.toString());
				}
				String date = Format.dateToString(BasicBook.yearI, BasicBook.monthI, BasicBook.dayI);
				String travel = Format.travelToString(t[0].startTime, start, t[0].endTime, stop);
				String type = (String) BasicBook.typeI.getSelectedItem();
				String seat = Format.seatInfo(t);
				String price = Format.priceInfo(t);
				BasicResult.result(o.code, uid, trainNbr, date, travel, type, seat, price);
				panel.setVisible(false);
				BasicResult.panel.setVisible(true);
			} catch (BookException be) {
				BasicFail.hint.setText(be.getErrMsg());
				panel.setVisible(false);
				BasicFail.panel.setVisible(true);
			}
		}
	}
}
