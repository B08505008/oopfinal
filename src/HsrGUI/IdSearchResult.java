package HsrGUI;

import java.awt.*;
import java.awt.event.*;
import java.util.Date;
import javax.swing.*;

public class IdSearchResult {
	IdSearchResult(){
		panel.setLayout(new GridBagLayout());
		Format.setFormat(jc);
		Format.setBlackTrans(bidI);
		Format.setWhiteBlack(info);
		Format.setWhiteBlack(modify);
		
		
		addListener();

		for (int i = 0; i < jc.length; i++) {
			Format.add(panel,jc,layout,i);
		}
	}

	static JPanel panel = new JPanel();
	static JButton back = new JButton("< 回上頁");
	static JLabel status = new JLabel("訂位代碼查詢", SwingConstants.CENTER);
	static JLabel bid = new JLabel("您的訂位代碼", SwingConstants.CENTER);
	static JLabel bidI = new JLabel("202106211118", SwingConstants.CENTER);
	
	static JButton info = new JButton("訂位資訊查詢");
	static JButton modify = new JButton("更改/取消訂位");
	
	static JComponent[] jc ={back,status,
							 bid, bidI,
							 info,modify};
	
	static int layout[][] = {
			{ 0, 0, 3, 1, 0, 0, Format.fill[3], Format.anchor[5] ,1,1,0,0,5,2}, { 3, 0, 12, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			
			{ 0, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},{ 3, 1, 12, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,5,0,3,5,0},
			
			{ 3, 2, 6, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 9, 2, 6, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,5,0,0,5,0},
			};
	
	 void addListener() {
		back.addMouseListener(new BackListener());
		info.addMouseListener(new InfoListener());
		modify.addMouseListener(new ModifyListener());
	}
	
	class BackListener extends HsrGUI.BackListener{ //回上頁
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			IdSearch.panel.setVisible(true);
		}
	}
	
	class InfoListener extends AllListener{ //到訂票資訊搜尋頁面
		@Override
		public void mouseClicked(MouseEvent e) {
			InfoSearch.uidI.setText(IdSearch.uidI.getText());
			InfoSearch.bidI.setText(bidI.getText());
			InfoSearch.status.setText("訂位資訊查詢");
			panel.setVisible(false);
			InfoSearch.panel.setVisible(true);
		}
	}
	
	class ModifyListener extends AllListener{ //到更改/取消訂位頁面
		@Override
		public void mouseClicked(MouseEvent e) {
			InfoSearch.uidI.setText(IdSearch.uidI.getText());
			InfoSearch.bidI.setText(bidI.getText());
			InfoSearch.status.setText("更改/取消訂位");
			panel.setVisible(false);
			InfoSearch.panel.setVisible(true);
		}
	}
	

	

}
