package HsrGUI;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import HSR.*;

public class AllTrainS {
		
		AllTrainS(){
			panel.setLayout(new GridBagLayout());
			Format.setFormat(jc);
			for (int i = 0; i < jc.length; i++) {
				Format.add(panel,jc,layout,i);
			}
			Format.setBlackTrans(front);
			Format.setBlackTrans(later);
			setDi();
			setTable();
			addListener();
			set();
			//later();
		}
		
		static GUITrainInfo[] table;
		static int lastIndex = 19; 
		
		static JPanel panel = new JPanel();
		static JButton back = new JButton("< 回上頁");
		static JButton front = new JButton("<<往前五班");
		static JButton later = new JButton("往後五班>>");
		
		static JLabel[][] arr = new JLabel[20][14];
		
		static JLabel  status = new JLabel("全部時刻",SwingConstants.CENTER);
		static JComboBox diI = new JComboBox();	
		
		static String[] diOption = {"                北上","                南下"};
		
		static JLabel  no = new JLabel("車次",SwingConstants.CENTER);
		static JLabel  day = new JLabel("行駛日",SwingConstants.CENTER);
		static JLabel  ks = new JLabel("左營",SwingConstants.CENTER);
		static JLabel  tn =new JLabel("台南",SwingConstants.CENTER);
		static JLabel  cy =new JLabel("嘉義",SwingConstants.CENTER);
		static JLabel  yl =new JLabel("雲林",SwingConstants.CENTER);
		static JLabel  ch = new JLabel("彰化",SwingConstants.CENTER);
		static JLabel  tc =new JLabel("台中",SwingConstants.CENTER);
		static JLabel  ml =new JLabel("苗栗",SwingConstants.CENTER);
		static JLabel  hc =new JLabel("新竹",SwingConstants.CENTER);
		static JLabel  ty = new JLabel("桃園",SwingConstants.CENTER);
		static JLabel  bq =new JLabel("板橋",SwingConstants.CENTER);
		static JLabel  tp =new JLabel("台北",SwingConstants.CENTER);
		static JLabel  ng =new JLabel("南港",SwingConstants.CENTER);
		
		
		static JComponent[] jc = {back,status,
								  front,diI,later,
								  no,day, ng,tp,bq,ty,hc,ml,tc,ch,yl,cy,tn,ks};
		
		static int layout [][] ={
				{ 0, 0, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,5,1}, 
				{ 3, 0, 39, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,5,1}, 
				
				{ 12, 1, 6, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,5,1}, 
				{ 18, 1, 9, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,5,1},
				{ 27, 1, 6, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,5,1}, 
				
				{ 0, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,0,1}, 
				{ 3, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],25,8 ,0,0,0,1}, 
				{ 6, 2, 3, 1, 0, 0, Format.fill[0],Format. anchor[5],25,1 ,0,0,0,1}, 
				{ 9, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],25,1 ,0,0,0,1}, 
				{ 12, 2, 3, 1, 0, 0, Format.fill[1], Format.anchor[5],25,1 ,0,0,0,1}, 
				{ 15, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],25,1 ,0,0,0,1} ,
				{ 18, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],25,1 ,0,0,0,1}, 
				{ 21, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],25,1 ,0,0,0,1}, 
				{ 24, 2, 3, 1, 0, 0, Format.fill[0],Format. anchor[5],25,1 ,0,0,0,1}, 
				{ 27, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],25,1 ,0,0,0,1}, 
				{ 30, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],25,1 ,0,0,0,1}, 
				{ 33, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],25,1 ,0,0,0,1} ,
				{ 36, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],25,1 ,0,0,0,1}, 
				{ 39, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],25,1 ,0,0,0,1} ,
				
				};
		
		static void setDi() {//加入方向選項
			for(String s : diOption) {
				diI.addItem(s);
			}
			
		}
		
		
		static void setLayout(String s,int j,int i ) {//將單一標籤設定資訊，加入頁面
			JLabel jl = new JLabel(s,SwingConstants.CENTER);
			jl.setFont(Format.f);
			Format.setWhiteBlack(jl);
			GridBagConstraints c1 = new GridBagConstraints();
			c1.gridx = 3*i;
			c1.gridy = j+3;
			c1.gridwidth = 3;
			c1.gridheight = 1;
			c1.weightx = 0;
			c1.weighty = 0;
			c1.fill = Format.fill[0];
			c1.anchor =Format.anchor[5];
			c1.ipadx = 1;
			c1.ipady = 5;
			c1.insets = new Insets(0,0,2,1);
			panel.add(jl, c1);
			arr[j][i] = jl;
		}
	
		static void set() {//初始化頁面
			for(int j = 0 ; j < 20 ; j++) {
				String[] s = new String[14];
				for(int k = 0;k<HSR_GUI.station.length;k++) {
					if(table[j].staToTime.get(HSR_GUI.station[k])!=null) {
						s[k+2] = table[j].staToTime.get(HSR_GUI.station[k]).toString();
					}else {
						s[k+2] = " ";
					}
					
				}
				s[0] = table[j].trainNo;
				s[1] = Format.serviceToString(table[j].service);;
				for(int i = 0 ; i < 14  ; i++) {
					setLayout(s[i],j,i );
				}
			}
		}
		
		static void later() {//將資訊往後五班次
			if((lastIndex+5)<table.length) {
				lastIndex = lastIndex+5;
			}else if(lastIndex!=table.length-1) {
				lastIndex =table.length-1;
			}

			for (int j = lastIndex; j > lastIndex - 20; j--) {
				String[] s = new String[14];
				for (int k = 0; k < HSR_GUI.station.length; k++) {
					if (table[j].staToTime.get(HSR_GUI.station[k]) != null) {
						s[k + 2] = table[j].staToTime.get(HSR_GUI.station[k]).toString();
					} else {
						s[k + 2] = " ";
					}
				}
				s[0] = table[j].trainNo;
				s[1] = Format.serviceToString(table[j].service);

				for (int i = 0; i < 14; i++) {
					arr[19 - (lastIndex - j)][i].setText(s[i]);
				}
			}
		}
		
		static void earlier() {//將資訊往前五班次
			if(lastIndex>23) {
				lastIndex = lastIndex-5;
			}else if(lastIndex!=19) {
				lastIndex =19;
			}

			for (int j = lastIndex; j > lastIndex - 20; j--) {
				String[] s = new String[14];
				for (int k = 0; k < HSR_GUI.station.length; k++) {
					if (table[j].staToTime.get(HSR_GUI.station[k]) != null) {
						s[k + 2] = table[j].staToTime.get(HSR_GUI.station[k]).toString();
					} else {
						s[k + 2] = " ";
					}

				}
				s[0] = table[j].trainNo;
				s[1] = Format.serviceToString(table[j].service);

				for (int i = 0; i < 14; i++) {
					arr[19 - (lastIndex - j)][i].setText(s[i]);
				}
			}
		}
		
		static void setTable() {//設置北上時刻表
			TrainInfo[] all = GetTimeTableFromJson.getTimeTable();
			GUITrainInfo[] temp =Format.toGUITrainInfoArray(all, 1);
			table = Format.sortByTc(temp);
	
		}
		
		public void addListener() {//加入監聽器
			back.addMouseListener(new BackListener());
			later.addMouseListener(new laterL());
			front.addMouseListener(new frontL());
			diI.addActionListener(new diL());
		}
		
		class BackListener extends HsrGUI.BackListener{//回上頁
			@Override
			public void mouseClicked(MouseEvent e) {
				panel.setVisible(false);
				HomePage.panel.setVisible(true);
			}
		}
		
		class laterL extends AllListener{//往後五班次，若到底則不動作
			@Override
			public void mouseClicked(MouseEvent e) {
				if(lastIndex != table.length-1) {
					later();
				}
			}
		}
		
		class frontL extends AllListener{//往前五班次，若到頂則不動作
			@Override
			public void mouseClicked(MouseEvent e) {
				if(lastIndex != 19) {
					earlier();
				}
			}
		}
		
		class diL extends AllListener{  //若選擇北上則跳至北上頁面
			@Override
			public void actionPerformed(ActionEvent e) {
				if(diI.getSelectedIndex()==0) {
					AllTrainN.diI.setSelectedIndex(0);
					panel.setVisible(false);
					AllTrainN.panel.setVisible(true);
				}
			}
		}
	

}
