package HsrGUI;

import java.awt.*;
import HSR.*;
import java.awt.event.*;
import javax.swing.*;

public class InfoSearchResult {
	
	InfoSearchResult(){
		panel.setLayout(new GridBagLayout());
		Format.setFormat(jc);
		for(JComponent j : jcbt) {
			Format.setBlackTrans(j);
		}
		for (int i = 0; i < jc.length; i++) {
			Format.add(panel,jc,layout,i);
		}
		
		Format.setWhiteBlack(part);
		Format.setWhiteBlack(cancel);
		Format.setWhiteBlack(modify);
		Format.setRedTrans(hint);
		hint.setFont(Format.f16);
		hint.setVisible(false);
		addListener();
	}
	
	static Ticket[] t;
	static String sta2sta;
	
	static JPanel panel = new JPanel();
	static JLabel topic = new JLabel("訂位資訊", SwingConstants.CENTER);
	
	static JButton back = new JButton("< 回上頁");
	static JButton modify = new JButton("更改行程");
	static JButton part = new JButton("部分退票");
	static JButton cancel  = new JButton("取消訂位");
	
	static JLabel bid = new JLabel("訂位代碼", SwingConstants.CENTER);
	static JLabel uid = new JLabel("訂位人代碼", SwingConstants.CENTER);
	static JLabel date = new JLabel("乘車日期", SwingConstants.CENTER);
	static JLabel trainNbr = new JLabel("搭乘車次", SwingConstants.CENTER);
	static JLabel station = new JLabel("起訖時間", SwingConstants.CENTER);
	static JLabel type = new JLabel("車廂種類", SwingConstants.CENTER);
	static JLabel seat= new JLabel("座位資訊", SwingConstants.CENTER);
	
	static JLabel bidI = new JLabel("202106192055", SwingConstants.CENTER);
	static JLabel uidI = new JLabel("A234567890", SwingConstants.CENTER);
	static JLabel dateI = new JLabel("2021/6/24", SwingConstants.CENTER);
	static JLabel stationI = new JLabel("18:00 台北 - 19:00 台中", SwingConstants.CENTER);
	static JLabel trainNbrI = new JLabel("888", SwingConstants.CENTER);
	static JLabel typeI = new JLabel("標準車廂", SwingConstants.CENTER);
	static JLabel seatI = new JLabel("  3車20號、3車21號、3車22號、3車22號", SwingConstants.CENTER);
	static JLabel priceI = new JLabel(" ", SwingConstants.CENTER);
	static JLabel price = new JLabel("票價", SwingConstants.CENTER);
	static JButton[] mo = {modify,part,cancel};
	static JLabel hint = new JLabel("發車前30分鐘內不可改退票", SwingConstants.CENTER);
	static JComponent[] jc = {back,topic,
							bid,bidI,uid,uidI,
							date,dateI,trainNbr,trainNbrI,
							station,stationI,type,typeI,
							seat,seatI,
							price,priceI,
							modify,part,cancel,hint};
	
	static JComponent[] jcbt = {//topic,
			bidI,
			uidI,
			priceI,
			trainNbrI,dateI,
			stationI,
			seatI,
			typeI
			};
	
	static int layout [][] ={
			{ 0, 0, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,5,1},{ 3, 0, 9, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,5,1},
			
			{ 0, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],30,10 ,0,0,5,0},{ 3, 1, 4, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			{ 7, 1, 2, 1,0 , 0, Format.fill[1], Format.anchor[0],15,1 ,0,0,5,0},{ 9, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			
			
			{ 0, 3, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},{ 3, 3, 4, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			{ 7, 3, 2, 1,0 , 0, Format.fill[1], Format.anchor[0],30,10 ,0,0,5,0},{ 9, 3, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			{ 0, 4, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},{ 3, 4, 4, 1,0 , 0, Format.fill[0], Format.anchor[0],50,1 ,0,0,5,0},
			{ 7, 4, 2, 1,0 , 0, Format.fill[1], Format.anchor[0],30,10 ,0,0,5,0},{ 9, 4, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			{ 0, 5, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},{ 3, 5, 9, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			{ 0, 6, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},{ 3, 6, 9, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			{ 4, 8, 3, 1,0 , 0, Format.fill[3], Format.anchor[1],20,1 ,0,0,5,0},{ 7, 8, 2, 1,0 , 0, Format.fill[3], Format.anchor[1],20,1 ,0,0,5,0},
			{ 9, 8, 3, 1,0 , 0, Format.fill[3], Format.anchor[1],20,1 ,0,0,5,0},
			{ 4, 8, 8, 1,0 , 0, Format.fill[3], Format.anchor[0],20,1 ,0,0,5,0},
			};

	static void result(int bid,String uid,String no,String date,String travel,String type, String seat, String price, Ticket[] ticket,String sta) { //設定顯示資訊
		bidI.setText(Integer.toString(bid));
		uidI.setText(uid);
		trainNbrI.setText(no);
		dateI.setText(date);
		stationI.setText(travel);
		typeI.setText(type);
		seatI.setText(seat);
		priceI.setText(price);
		t = ticket;
		sta2sta = sta;
	}
	
	static void reset() {
		for(JButton j :InfoSearchResult.mo)	{
			j.setVisible(true);
		}
		InfoSearchResult.hint.setVisible(false);
	}
	
	public void addListener() {//加入所有監聽器
		back.addMouseListener(new BackListener());
		modify.addMouseListener(new ModifyL());
		part.addMouseListener(new PartL());
		cancel.addMouseListener(new CancelL());
	}
	
	class BackListener extends HsrGUI.BackListener{//返回首頁
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			reset();
			InfoSearch.reset();
			InfoSearch.panel.setVisible(true);
		}
	}
	
	class ModifyL extends AllListener{ //跳至更改行程頁面
		@Override
		public void mouseClicked(MouseEvent e) {
			String start = Format.transStationID(t[0].startStation);
			String stop = Format.transStationID(t[0].endStation);
			String station = Format.stationToString(start,stop);
			int[] amounts = {0,0,0,0,0,0};
			for(int i = 0; i< InfoSearchResult.t.length;i++) {
				if(InfoSearchResult.t[i].type==4) {
					amounts[0]++;
				}else {
					amounts[InfoSearchResult.t[i].type]++;
				}
			}
			String a = Format.toAmountInfo(amounts);
			Modify.result(station, a);
			panel.setVisible(false);
			Modify.panel.setVisible(true);
		}
	}
	
	class PartL extends AllListener{ //跳至部分退票頁面
		@Override
		public void mouseClicked(MouseEvent e) {
			PartCancel.result(sta2sta, t);
			panel.setVisible(false);
			PartCancel.panel.setVisible(true);
		}
	}
	
	class CancelL extends AllListener{//跳至退票確認頁面
		@Override
		public void mouseClicked(MouseEvent e) {
			for(Ticket tic : t) {
				DB.cancel(tic);
			}
			panel.setVisible(false);
			CancelResult.panel.setVisible(true);
		}
	}
}
