package HsrGUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import HSR.GetTimeTableFromJson;
import HSR.Timing;
import HSR.TrainInfo;
public class ConditionResult {
	
	ConditionResult(){
		panel.setLayout(new GridBagLayout());
		Format.setFormat(jc);
		for (int i = 0; i < jc.length; i++) {
			Format.add(panel,jc,layout,i);
		}
		set();
		Format.setBlackTrans(infoI);
		Format.setBlackTrans(front);
		Format.setBlackTrans(later);
		addListener();
	}
	
	static GUITrainInfo[] table;
	static int no = 0;
	static int firstIndex = -1;
	static String startStation ;
	static String arrivalStation ;
	
	static JPanel panel = new JPanel();
	static JButton back = new JButton("< 回上頁");
	static JLabel  status = new JLabel("時刻查詢",SwingConstants.CENTER);
	static JButton front = new JButton("<<較早車次");
	static JButton later = new JButton("較晚車次>>");
	static JLabel infoI = new JLabel(" ", SwingConstants.CENTER);
	static JLabel  trainNbr = new JLabel("車次",SwingConstants.CENTER);
	static JLabel  depart = new JLabel("出發時間",SwingConstants.CENTER);
	static JLabel arrival = new JLabel("抵達時間",SwingConstants.CENTER);
	static JLabel  travel = new JLabel("乘車時間",SwingConstants.CENTER);
	static JLabel discount =new JLabel("全票優惠",SwingConstants.CENTER);
	static JLabel discountC =new JLabel("大學生優惠",SwingConstants.CENTER);
	static JLabel [][] arr = new JLabel[5][6];

	
	static JComponent[] jc = {back,status,
							front,
							infoI,
							later,
							trainNbr,depart,arrival,travel,discount,discountC};
	
	static int layout [][] ={
			{ 0, 0, 3, 1, 0, 0, Format.fill[2], Format.anchor[5],1,1 ,0,0,5,1}, { 3, 0, 15, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,5,1},
			
			{ 0, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			{ 3, 1, 15, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			{ 15, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			{ 0, 7, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,5,1}, 
			{ 3, 7, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],60,1 ,0,0,5,1}, 
			{ 6, 7, 3, 1, 0, 0, Format.fill[0],Format. anchor[5],60,1 ,0,0,5,1}, 
			{ 9, 7, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],60,1 ,0,0,5,1}, 
			{ 12, 7, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],60,1 ,0,0,5,1}, 
			{ 15, 7, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],60,1 ,0,0,5,1}, 
			
			};
	
	
	static void setLayout(String s,int j,int i ) { //將單一標籤設定資訊，加入頁面
		JLabel jl = new JLabel(s,SwingConstants.CENTER);
		jl.setFont(Format.f);
		Format.setBlackTrans(jl);
		GridBagConstraints c1 = new GridBagConstraints();
		c1.gridx = 3*i;
		c1.gridy = j+8;
		c1.gridwidth = 3;
		c1.gridheight = 1;
		c1.weightx = 0;
		c1.weighty = 0;
		c1.fill = Format.fill[0];
		c1.anchor =Format.anchor[5];
		c1.ipadx = 1;
		c1.ipady = 1;
		c1.insets = new Insets(0,0,2,1);
		panel.add(jl, c1);
		arr[j][i] = jl;
	}
	
	static void setTable(TrainInfo[] train) { //設置時刻表
		GUITrainInfo[] temp =Format.toGUITrainInfoArray(train);
		for(GUITrainInfo g : temp) {
			System.out.println(g);
		}		
		table = Format.sortByStart(temp);
		for(GUITrainInfo g : table) {
			System.out.println(g);
		}		
	}
	
	static void set() { //加上車次資訊方塊
		for(int j = 0 ; j < 5 ; j++) {
			String[] s = {" "," "," "," "," "," "};
			for(int i = 0 ; i <s.length  ; i++) {
				setLayout(s[i],j,i );
			}
		}
	}
	
	static boolean setTrain(Timing t) { //設定各車次資訊
		int nbr = 0;
		GUITrainInfo[] temp;
		startStation = table[0].startingStation;
		arrivalStation = table[0].endingStation;
		for(int i = 0 ; i < table.length ; i++) {
			if(table[i].depatureTimes.get(0).larger(t)) {
				firstIndex = i;
				break;
			}
		}
		System.out.println("table長度 = "+table.length);
		System.out.println("firstIndex = "+firstIndex);
		if(firstIndex == -1 ) {
			return false;
		}else {
			nbr = table.length - firstIndex ;
			no = nbr;
			System.out.println("nbr = "+nbr);
			temp = new GUITrainInfo[nbr];
			int index = 0;
			for (int i = 0; i < table.length; i++) {
				if (table[i].depatureTimes.get(0).larger(t)) {
					temp[index] = table[i];
					index++;
				}
			}
			
			for (int j = 0; j < ((nbr > 5) ? 5 : nbr); j++) {
				String no = temp[j].trainNo;
				Timing startTime = temp[j].staToTime.get(startStation);
				String start = startTime.toString();
				Timing arrivalTime = temp[j].staToTime.get(arrivalStation);
				String arrival = arrivalTime.toString();
				Timing travelTime = arrivalTime.minus(startTime);
				String travel = travelTime.toString();
				String discount = Format.discountToString(temp[j].earlyDiscount, true);
				String discountC = Format.discountToString(temp[j].universityDiscount, false);
				String[] s = { no, start, arrival, travel, discount, discountC };

				for (int k = 0; k < s.length; k++) {
					arr[j][k].setText(s[k]);
				}
			}
			return true;
		}
	}
	
	static void earlier() { //將資訊往前五班次
		firstIndex = firstIndex-5;
		if(no<5) {
			firstIndex = table.length -5;
			no = 5;
		}
		
		if(firstIndex < 0) {
			firstIndex = 0;
		}
		
		for (int j = firstIndex; j < firstIndex+5; j++) {
			String no = table[j].trainNo;
			Timing startTime = table[j].staToTime.get(startStation);
			String start = startTime.toString();
			Timing arrivalTime = table[j].staToTime.get(arrivalStation);
			String arrival = arrivalTime.toString();
			Timing travelTime = arrivalTime.minus(startTime);
			String travel = travelTime.toString();
			String discount = Format.discountToString(table[j].earlyDiscount, true);
			String discountC = Format.discountToString(table[j].universityDiscount, false);
			
			String[] s = { no, start, arrival, travel, discount, discountC };
			
			for (int k = 0; k < s.length; k++) {
				arr[j-firstIndex][k].setText(s[k]);
			}
		}
	}
	
	static void later() { //將資訊往後五班次
		firstIndex = firstIndex+5;
		if (no > 4) {

			if (firstIndex > table.length - 5) {
				firstIndex = table.length - 5;
			}

			for (int j = firstIndex; j < firstIndex + 5; j++) {
				String no = table[j].trainNo;
				Timing startTime = table[j].staToTime.get(startStation);
				String start = startTime.toString();
				Timing arrivalTime = table[j].staToTime.get(arrivalStation);
				String arrival = arrivalTime.toString();
				Timing travelTime = arrivalTime.minus(startTime);
				String travel = travelTime.toString();
				String discount = Format.discountToString(table[j].earlyDiscount, true);
				String discountC = Format.discountToString(table[j].universityDiscount, false);

				String[] s = { no, start, arrival, travel, discount, discountC };

				for (int k = 0; k < s.length; k++) {
					arr[j - firstIndex][k].setText(s[k]);
				}
			}
		}
	}
	
	static void setInfo(int year, int month, int day , String station) { //設定最上方資訊欄內容
		Font f = new Font("微軟正黑體", Font.BOLD, 17);
		infoI.setFont(f);
		Format.setBlackTrans(infoI);
		String info;
		String date = year+"/"+month+"/"+day+" | ";
		info =  date + station;
		infoI.setText(info);
		
	}

	static void reset() { //重整頁面
		for(JLabel[] j : arr) {
			for(JLabel a : j) {
				a.setText(" ");
			}
		}
		firstIndex = -1;
	}
	
	public void addListener() {	//加入所有監聽器
		back.addMouseListener(new BackListener());
		front.addMouseListener(new FrontL());
		later.addMouseListener(new LaterL());
	}

	class BackListener extends HsrGUI.BackListener{ //回上頁
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			ConditionSearch.panel.setVisible(true);
			reset();
		}
	}
	
	class FrontL extends AllListener{ //往前五個車次，若到頂則不動作
		@Override
		public void mouseClicked(MouseEvent e) {
			earlier();
		}
	}
	
	class LaterL extends AllListener{//往後五個車次，若到底則不動作
		@Override
		public void mouseClicked(MouseEvent e) {
			later();
		}
	}
}
