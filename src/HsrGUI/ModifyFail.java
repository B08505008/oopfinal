package HsrGUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class ModifyFail {
	
	ModifyFail(){
		panel.setLayout(new GridBagLayout());
		Format.setFormat(jc);
		topic.setFont(Format.f16);
		for (int i = 0; i < jc.length; i++) {
			Format.add(panel,jc,layout,i);
		}
		Format.setBlackTrans(hint);
		Format.setWhiteBlack(back);
		Format.setWhiteBlack(modify);
		addListener();
	}
	
	static JPanel panel = new JPanel();
	static JLabel topic = new JLabel("            更改失敗！", SwingConstants.CENTER);
	
	static JButton back = new JButton("返回首頁");
	static JButton modify = new JButton("更改條件");
	static JLabel hint = new JLabel(" ", SwingConstants.CENTER);
	static JComponent[] jc = {topic,
							  hint,
							  modify,back};
	
	static int layout [][] ={
			{ 0, 0, 12, 1, 0, 0, Format.fill[0], Format.anchor[5],1,10 ,0,0,5,1},
			
			{ 0, 1, 12, 1, 0, 0, Format.fill[0], Format.anchor[5],1,10 ,0,0,5,1},
			
			
			{ 0, 6, 6, 1,0 , 0, Format.fill[3], Format.anchor[1],20,1 ,0,0,5,0},
			{ 6, 6, 6, 1,0 , 0, Format.fill[3], Format.anchor[1],20,1 ,0,0,5,0},
			};
	
	public void addListener() {//加入所有監聽器
		back.addMouseListener(new BackListener());
		modify.addMouseListener(new ModifyL());
	}

	class BackListener extends HsrGUI.BackListener{//返回首頁
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			HomePage.panel.setVisible(true);
		}
	}
	
	class ModifyL extends AllListener{ //返回訂票頁面
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			Modify.panel.setVisible(true);
		}
	}
}
