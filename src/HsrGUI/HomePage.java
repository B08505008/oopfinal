package HsrGUI;

import java.awt.*;
import java.awt.event.*;
import HSR.*;

import javax.swing.*;

public class HomePage {
	HomePage(){
		panel.setLayout(new GridBagLayout());
		topic.setFont(new Font("稬硁タ堵砰",Font.BOLD,30));
		etopic.setFont(new Font("稬硁タ堵砰",Font.BOLD,15));
		
		for (int i = 2; i < jc.length; i++) {
			jc[i].setBackground(Color.ORANGE);
			jc[i].setFont(Format.f);
		}
		
		for (int i = 0; i < jc.length; i++) {
			Format.add(panel,jc,layout,i);
		}
		
		setSubButton();
		setEmpty();
		
		basicBook.setVisible(false);
		twoWayBook.setVisible(false);
		
		idSearch.setVisible(false);
		infoSearch.setVisible(false);
		modify.setVisible(false);
		
		all.setVisible(false);
		timeSearch.setVisible(false);
		
		addListener();
	}
	
	static JPanel panel = new JPanel();
	static JLabel  topic = new JLabel("芖蔼臟璹布╰参",SwingConstants.CENTER);
	static JLabel  etopic = new JLabel("TAIWAN HIGH SPEED RAIL BOOKING SYSTEM",SwingConstants.CENTER);	
	static JButton book = new JButton("/兵ン璹布");
	static JButton cancel = new JButton("琩高/э璹布");
	static JButton search = new JButton(" ó痁Ω琩高 ");
	
	static JButton basicBook = new JButton("璹布");
	static JButton twoWayBook = new JButton("ㄓ璹布");
	
	static JButton idSearch = new JButton("璹絏琩高");
	static JButton infoSearch = new JButton("璹戈癟琩高");
	static JButton modify = new JButton("э/璹");
	
	static JButton all = new JButton("场ㄨ");
	static JButton timeSearch = new JButton("ㄌ兵ン琩高");
	
	static JLabel empty = new JLabel(" "); 
	static JLabel empty2 = new JLabel(" "); 
	static JLabel[] empArr =  {empty,empty2};
	
	static void setEmpty() {
		for(JLabel j : empArr) {
			j.setOpaque(false);
		}
	}
	
	static JComponent[] jc = {topic,etopic,
							  book,cancel,search,
							  basicBook,twoWayBook,
							  idSearch,infoSearch,modify,
							  all,timeSearch,empty,empty2};
	private int layout[][] = {
			   { 0, 0, 6, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1,0,0,0,0 }, 
			   { 0, 1, 6, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,10,0},
			   
			   { 0, 3, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,10,0,0,0,0},
			   { 2, 3, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,0,0},
			   { 4, 3, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,0,0},
	
			   { 0, 4, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,10,0,0,0,0},
			   { 2, 4, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,0,0},
			   
			   { 0, 4, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,10,0,0,0,0},
			   { 2, 4, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,0,0},
			   { 4, 4, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,0,0},
	
				{ 2, 4, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,10,0,0,0,0},
				{ 4, 4, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,0,0},
				
				{ 4, 4, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,21,0,0,0,0},
				{ 0, 5, 12, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,101,0,0,0,0}};
	
	void addListener() {
		book.addMouseListener(new BookListener());
		cancel.addMouseListener(new CancelListener());
		search.addMouseListener(new SearchListener());
		
		basicBook.addMouseListener(new BasicBookL());
		twoWayBook.addMouseListener(new TwoWayBookL());
		
		idSearch.addMouseListener(new idSearchL());
		infoSearch.addMouseListener(new infoSearchL());
		modify.addMouseListener(new modifyL());
		
		all.addMouseListener(new allListener());
		timeSearch.addMouseListener(new timeSearchListener());
	}
	
	void setSubButton() {
		for(int i = 5 ; i<jc.length-1 ; i++) {
			Format.setWhiteBlack(jc[i]);
		}
	}
	
	class BookListener extends AllListener { //陪ボㄢ贺璹布秙

		@Override
		public void mouseEntered(MouseEvent e) {
			empty.setVisible(false);
			Format.setWhiteBlack(book);
			basicBook.setVisible(true);
			twoWayBook.setVisible(true);

		}

		@Override
		public void mouseExited(MouseEvent e) {
			empty.setVisible(true);
			Format.setOrangeBlack(book);
			basicBook.setVisible(false);
			twoWayBook.setVisible(false);

		}
	}
	
	class BasicBookL extends BookListener{ //秨币璹布
		
		@Override
		public void mouseClicked(MouseEvent e) {
			BasicBook.reset();
			panel.setVisible(false);
			BasicBook.panel.setVisible(true);
		}
		
		@Override
		public void mouseEntered(MouseEvent e) {
			empty.setVisible(false);
			Format.setWhiteBlack(book);
			Format.setBlackWhite(basicBook);
			basicBook.setVisible(true);
			twoWayBook.setVisible(true);


		}

		@Override
		public void mouseExited(MouseEvent e) {
			empty.setVisible(true);
			Format.setOrangeBlack(book);
			Format.setWhiteBlack(basicBook);
			basicBook.setVisible(false);
			twoWayBook.setVisible(false);
		}
	}
	
	class TwoWayBookL extends BookListener{//秨币ㄓ璹布
		@Override
		public void mouseClicked(MouseEvent e) {
			TwoWayBook.reset();
			panel.setVisible(false);
			TwoWayBook.panel.setVisible(true);
		}
		
		@Override
		public void mouseEntered(MouseEvent e) {
			empty.setVisible(false);
			Format.setWhiteBlack(book);
			Format.setBlackWhite(twoWayBook);
			basicBook.setVisible(true);
			twoWayBook.setVisible(true);

		}

		@Override
		public void mouseExited(MouseEvent e) {
			empty.setVisible(true);
			Format.setOrangeBlack(book);
			Format.setWhiteBlack(twoWayBook);
			basicBook.setVisible(false);
			twoWayBook.setVisible(false);
		}
	}
	
	class CancelListener extends AllListener{ //陪ボ贺琩高/э秙
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setWhiteBlack(cancel);
			idSearch.setVisible(true);
			infoSearch.setVisible(true);
			modify.setVisible(true);

		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setOrangeBlack(cancel);
			idSearch.setVisible(false);
			infoSearch.setVisible(false);
			modify.setVisible(false);

		}
	}
	
	class idSearchL extends CancelListener{ //秨币璹絏琩高
		
		@Override
		public void mouseClicked(MouseEvent e) {
			IdSearch.reset();
			panel.setVisible(false);
			IdSearch.panel.setVisible(true);
		}
		
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setWhiteBlack(cancel);
			Format.setBlackWhite(idSearch);
			idSearch.setVisible(true);
			infoSearch.setVisible(true);
			modify.setVisible(true);

		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setOrangeBlack(cancel);
			Format.setWhiteBlack(idSearch);
			idSearch.setVisible(false);
			infoSearch.setVisible(false);
			modify.setVisible(false);

		}
	}
	
	class infoSearchL extends CancelListener{//秨币璹戈癟琩高
		
		@Override
		public void mouseClicked(MouseEvent e) {
			InfoSearch.status.setText("璹戈癟琩高");
			InfoSearch.reset();
			panel.setVisible(false);
			InfoSearch.panel.setVisible(true);
		}
		
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setWhiteBlack(cancel);
			Format.setBlackWhite(infoSearch);
			idSearch.setVisible(true);
			infoSearch.setVisible(true);
			modify.setVisible(true);

		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setOrangeBlack(cancel);
			Format.setWhiteBlack(infoSearch);
			idSearch.setVisible(false);
			infoSearch.setVisible(false);
			modify.setVisible(false);

		}
	}
	
	class modifyL extends CancelListener{ //秨币э癶布
		@Override
		public void mouseClicked(MouseEvent e) {
			InfoSearch.status.setText("э/璹");
			InfoSearch.reset();
			panel.setVisible(false);
			InfoSearch.panel.setVisible(true);
		}
		
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setWhiteBlack(cancel);
			Format.setBlackWhite(modify);
			idSearch.setVisible(true);
			infoSearch.setVisible(true);
			modify.setVisible(true);

		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setOrangeBlack(cancel);
			Format.setWhiteBlack(modify);
			idSearch.setVisible(false);
			infoSearch.setVisible(false);
			modify.setVisible(false);

		}
	}
	
	class SearchListener extends AllListener{//陪ボㄢ贺琩高秙

		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setWhiteBlack(search);
			all.setVisible(true);
			timeSearch.setVisible(true);

		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setOrangeBlack(search);
			all.setVisible(false);
			timeSearch.setVisible(false);

		}
	}
	
	class allListener extends AllListener{ //秨币场ㄨ
		
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false); 
			AllTrainN.panel.setVisible(true);
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setWhiteBlack(search);
			Format.setBlackWhite(all);
			all.setVisible(true);
			timeSearch.setVisible(true);

		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setOrangeBlack(search);
			Format.setWhiteBlack(all);
			all.setVisible(false);
			timeSearch.setVisible(false);

		}
	}
	
	class timeSearchListener extends AllListener{//秨币ㄌ兵ン痁Ω琩高
		
		@Override
		public void mouseClicked(MouseEvent e) {
			ConditionSearch.reset();
			panel.setVisible(false);
			ConditionSearch.panel.setVisible(true);
		}


		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setWhiteBlack(search);
			Format.setBlackWhite(timeSearch);
			all.setVisible(true);
			timeSearch.setVisible(true);

		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setOrangeBlack(search);
			Format.setWhiteBlack(timeSearch);
			all.setVisible(false);
			timeSearch.setVisible(false);

		}
	}
	
}
