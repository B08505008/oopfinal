package HsrGUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class BasicResult {
	
	BasicResult(){
		panel.setLayout(new GridBagLayout());
		Format.setFormat(jc);
		topic.setFont(Format.f16);
		for(JComponent j : jcbt) {
			Format.setBlackTrans(j);
		}
		for (int i = 0; i < jc.length; i++) {
			Format.add(panel,jc,layout,i);
		}
		Format.setWhiteBlack(conti);
		Format.setWhiteBlack(back);
		Format.setWhiteBlack(modify);
		addListener();
	}
	
	static JPanel panel = new JPanel();
	static JLabel  status = new JLabel("一般訂票",SwingConstants.CENTER);
	static JLabel topic = new JLabel("            訂位成功！", SwingConstants.CENTER);
	static JButton conti = new JButton("繼續訂位");
	static JButton back = new JButton("返回首頁");
	static JButton modify = new JButton("更改訂位");
	static JLabel bid = new JLabel("訂位代碼", SwingConstants.CENTER);
	static JLabel uid = new JLabel("訂位人代碼", SwingConstants.CENTER);
	static JLabel date = new JLabel("乘車日期", SwingConstants.CENTER);
	static JLabel trainNbr = new JLabel("搭乘車次", SwingConstants.CENTER);
	static JLabel station = new JLabel("起訖時間", SwingConstants.CENTER);
	static JLabel type = new JLabel("車廂種類", SwingConstants.CENTER);
	static JLabel price = new JLabel("票價", SwingConstants.CENTER);
	static JLabel seat= new JLabel("座位資訊", SwingConstants.CENTER);
	static JLabel bidI = new JLabel("202106192055", SwingConstants.CENTER);
	static JLabel uidI = new JLabel(" ", SwingConstants.CENTER);
	static JLabel dateI = new JLabel(" ", SwingConstants.CENTER);
	static JLabel stationI = new JLabel(" ", SwingConstants.CENTER);
	static JLabel trainNbrI = new JLabel("888", SwingConstants.CENTER);
	static JLabel typeI = new JLabel("標準車廂", SwingConstants.CENTER);
	static JLabel seatI = new JLabel("  3車20號、3車21號、3車22號、3車22號", SwingConstants.CENTER);
	static JLabel priceI = new JLabel("共 1450 元，請在2021 年 6 月 25 日 23:59 前 完成付款", SwingConstants.CENTER);
	static JComponent[] jc = {topic,
							bid,bidI,uid,uidI,
							date,dateI,trainNbr,trainNbrI,
							station,stationI,type,typeI,
							seat,seatI,
							price,priceI,
							conti,modify,back};
	
	static JComponent[] jcbt = {
			bidI,
			uidI,
			trainNbrI,dateI,
			stationI,
			seatI,
			typeI,
			priceI,
			};
	
	static int layout [][] ={
			{ 0, 0, 12, 1, 0, 0, Format.fill[0], Format.anchor[5],1,10 ,0,0,5,1},
			
			{ 0, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],30,10 ,0,0,5,0},{ 3, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			{ 6, 1, 2, 1,0 , 0, Format.fill[1], Format.anchor[0],15,1 ,0,0,5,0},{ 8, 1, 4, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			{ 0, 3, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},{ 3, 3, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			{ 6, 3, 2, 1,0 , 0, Format.fill[1], Format.anchor[0],30,10 ,0,0,5,0},{ 8, 3, 4, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			{ 0, 4, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},{ 3, 4, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],50,1 ,0,0,5,0},
			{ 6, 4, 2, 1,0 , 0, Format.fill[1], Format.anchor[0],30,10 ,0,0,5,0},{ 8, 4, 4, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			{ 0, 5, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},{ 3, 5, 9, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			{ 0, 7, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},{ 3, 7, 9, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			{ 6, 9, 2, 1,0 , 0, Format.fill[3], Format.anchor[1],15,1 ,0,0,5,0},{ 8, 9, 2, 1,0 , 0, Format.fill[3], Format.anchor[1],15,1 ,0,0,5,0},
			{ 10, 9, 2, 1,0 , 0, Format.fill[3], Format.anchor[1],15,1 ,0,0,5,0},
			};
	
	
	static void result(int bid,String uid,String no,String date,String travel,String type, String seat, String price) { //設定顯示資訊
		bidI.setText(Integer.toString(bid));
		uidI.setText(uid);
		trainNbrI.setText(no);
		dateI.setText(date);
		stationI.setText(travel);
		typeI.setText(type);
		seatI.setText(seat);
		priceI.setText(price);
	}
	
	public void addListener() {//加入所有監聽器
		back.addMouseListener(new BackListener());
		conti.addMouseListener(new ContiL());
		modify.addMouseListener(new ModifyL());
	}
	
	class BackListener extends HsrGUI.BackListener{//返回首頁
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			HomePage.panel.setVisible(true);
		}
	}
	
	class ContiL extends AllListener{ //返回一般訂票
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			BasicBook.panel.setVisible(true);
		}
	}
	
	class ModifyL extends AllListener{ //跳至更改定位，雙代碼直接帶入
		@Override
		public void mouseClicked(MouseEvent e) {
			InfoSearch.status.setText("更改/取消訂位");
			InfoSearch.uidI.setText(uidI.getText());
			InfoSearch.bidI.setText(bidI.getText());
			panel.setVisible(false);
			InfoSearch.panel.setVisible(true);
		}
	}
}
