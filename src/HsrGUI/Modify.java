package HsrGUI;
import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;

import HSR.BookException;
import HSR.DB;
import HSR.Order;
import HSR.Ticket;


public class Modify {
	Modify(){
		panel.setLayout(new GridBagLayout());
		Format.setFormat(jc);
		trainNbrI.setFont(Format.f);
		Format.setYear(yearI);
		Format.setMonth(monthI);
		Format.setDay(dayI,HSR_GUI.day31);
		Format.setHour(hourI);
		Format.setMeans(meansI);
		Format.setSeat(seatI);
		setDate();
	
		Format.setBlackTrans(stationI);
		Format.setBlackTrans(year);
		Format.setBlackTrans(month);
		Format.setBlackTrans(day);
		Format.setBlackTrans(typeI);
		Format.setBlackTrans(ticketI);
		
		Format.setRedTrans(hint);
		//trainNbrI.setVisible(false);
		
		addListener();

		for (int i = 0; i < jc.length; i++) {
			Format.add(panel,jc,layout,i);
		}
	}
	
	

	static JPanel panel = new JPanel();
	static JButton back = new JButton("< 回上頁");
	static JLabel status = new JLabel("更改行程", SwingConstants.CENTER);
	static JLabel station = new JLabel("起訖站", SwingConstants.CENTER);
	static JLabel stationI = new JLabel("南港  往  台北", SwingConstants.CENTER);
	static JLabel type = new JLabel("車廂種類", SwingConstants.CENTER);
	static JLabel typeI = new JLabel("標準車廂", SwingConstants.CENTER);
	static JLabel seat = new JLabel("座位喜好", SwingConstants.CENTER);
	static JLabel date = new JLabel("乘車日期", SwingConstants.CENTER);
	static JLabel means = new JLabel("改乘車次", SwingConstants.CENTER);
	static JLabel time = new JLabel("出發時間", SwingConstants.CENTER);
	static JLabel number = new JLabel("車次", SwingConstants.CENTER);
	static JLabel ticket = new JLabel("票數", SwingConstants.CENTER);
	static JLabel ticketI = new JLabel("全票 2 張  孩童 1 張", SwingConstants.CENTER);
	
	static JComboBox seatI= new JComboBox();
	
	static JComboBox yearI = new JComboBox();
	static JLabel 	 year  = new JLabel("年",SwingConstants.CENTER);
	static JComboBox monthI= new JComboBox();
	static JLabel    month = new JLabel("月",SwingConstants.CENTER);
	static JComboBox dayI  = new JComboBox();
	static JLabel day      = new JLabel("日",SwingConstants.CENTER);

	static JComboBox hourI  = new JComboBox();
	static JComboBox meansI = new JComboBox();
	static JTextField trainNbrI = new JTextField();
	
	static JCheckBox discount = new JCheckBox("限尚有優惠之車次");
	
	static JButton book = new JButton("開始更改");
	static JLabel hint = new JLabel(" ");
	
	static JComponent[] jc ={back,status,
							  station,stationI,
							  date,yearI,year,monthI,month,dayI,day,
							  means,/*meansI,time,hourI,*/
							  trainNbrI,
							  type,typeI,
							 // seat,seatI,
							  ticket,ticketI,
							  discount,book,hint};
	
	static int layout[][] = {
			{ 0, 0, 3, 1, 0, 0, Format.fill[3], Format.anchor[5] ,1,1,0,0,10,2}, { 3, 0, 8, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,10,0},
			
			{ 0, 2, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},{ 3, 2, 8, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,3,5,0},
			
			{ 0, 3, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0}, 
			{ 3, 3, 2, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,3,5,0},{ 5, 3, 2, 1, 0, 0,Format.fill[0], Format.anchor[0] ,10,1,0,0,5,0},
			{ 7, 3, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 8, 3, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 9, 3, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 10, 3, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			
			{ 0, 4, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},/*{ 3, 4, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,3,5,0},
			{ 6, 4, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,30,1,0,10,5,5},{ 9, 4, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},*/
			{ 3, 4, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,8,0,3,5,0},
			
			{ 0, 5, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 5, 8, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,3,5,0},
			//{ 6, 5, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,10,5,5},{ 9, 5, 2, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},
			
			{ 0, 7, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},
			{ 3, 7, 8, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			
			{ 0, 8, 4, 1, 0, 0,Format.fill[0], Format.anchor[7] ,1,1,0,0,0,0},{ 9, 8, 2, 1, 0, 0,Format.fill[0], Format.anchor[0] ,20,1,0,0,0,0},
			{ 4, 8, 2, 1, 0, 0,Format.fill[3], Format.anchor[0] ,1,1,0,0,0,0},
			};
	static void result(String travel,String tic) {
		stationI.setText(travel);
		ticketI.setText(tic);
	}
	
	 void addListener() { //加入監聽器
		back.addMouseListener(new BackListener());
		meansI.addActionListener(new MeansIListener());
		monthI.addActionListener(new MonthIListener());
		book.addMouseListener(new BookListener());
	}
	
	static void reset()	{ //重整頁面
		hint.setText(" ");
	}
	
	static void setDate() { //設定今日日期，離現在最近且較早之半整點
		Date date = new Date();
		dayI.setSelectedItem(date.getDate());
		monthI.setSelectedItem(date.getMonth()+1);
		yearI.setSelectedItem(date.getYear());
		String hour = Integer.toString(date.getHours())+" : "+((date.getMinutes()>29)?"30":"00");
		hourI.setSelectedItem(hour);
		
	}
	
	class BackListener extends HsrGUI.BackListener{ //回上頁
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			InfoSearchResult.panel.setVisible(true);
		}
	}
	
	static void changeMeans(String s) { //依據訂票方式而更換顯示方塊
		if(s.equals("依車次")) {
			time.setText("搭乘車次");
			hourI.setVisible(false);
			trainNbrI.setVisible(true);
		}
		else {
			time.setText("出發時間");
			trainNbrI.setVisible(false);
			hourI.setVisible(true);
		}
		
	}
	
	class MeansIListener implements ActionListener{ //依據訂票方式而更換顯示方塊
		@Override
		public void actionPerformed(ActionEvent e) {
			changeMeans((String)meansI.getSelectedItem());
		}
	}
	
	class MonthIListener implements ActionListener{ //日期選項隨選擇月份改變
		@Override
		public void actionPerformed(ActionEvent e) {
			int i = dayI.getSelectedIndex();
			switch (monthI.getSelectedIndex()+1){
			case 1:case 3:case 5:case 7:case 8:case 10:case 12:
				Format.setDay(dayI,HSR_GUI.day31);
				break;
			case 4:case 6:case 9:case 11:
				Format.setDay(dayI,HSR_GUI.day30);
				break;
			case 2:
				Format.setDay(dayI,HSR_GUI.day28);
				break;
			}
			if(dayI.getItemCount()>i) {
				dayI.setSelectedIndex(i);
			}
			else {
				dayI.setSelectedIndex(dayI.getItemCount()-1);
			}
			
		}
	}
	
	class BookListener extends AllListener{ //車次訂票跳至成功失敗，時間訂票跳出可選車次
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setBlackOrange(book);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setOrangeBlack(book);
		}
		
		@Override
		public void mouseClicked(MouseEvent e) {
			if (trainNbrI.getText().equals("")) {
				hint.setText("請輸入車次");
			}else {
				hint.setText(" ");
					String uid = InfoSearchResult.t[0].uid;
					String trainNbr  = trainNbrI.getText();	
					String startID = InfoSearchResult.t[0].startStation;
					String stopID =  InfoSearchResult.t[0].endStation;
					String start = Format.transStationID(startID);
					String stop = Format.transStationID(stopID);
					
					int year = (int)yearI.getSelectedItem();
					int month = (int)monthI.getSelectedItem();
					int day = (int)dayI.getSelectedItem();
					String dateStd = Format.toStdDate(year, month, day);
					boolean std = (typeI.getText().equals("標準車廂"))?true:false;
					int per = Format.toSeatPre(seatI);
					int[] amounts = {0,0,0,0,0,0};
					for(int i = 0; i< InfoSearchResult.t.length;i++) {
						System.out.println("type = "+InfoSearchResult.t[i].type);
						if(InfoSearchResult.t[i].type==4) {
							amounts[0]++;
						}else {
							amounts[InfoSearchResult.t[i].type]++;
						}
					}
					for(int j :amounts) {
						System.out.println("amoounts = "+j);
					}
					Order  o = new Order(uid, trainNbr, startID, stopID, dateStd, std, per, amounts);
					
					try {
						Ticket[] t = DB.buyticket(o);
						for(Ticket b:t) {
							System.out.println(b.toString());
						}
						String date = Format.dateToString(yearI,monthI,dayI);
						String travel = Format.travelToString(t[0].startTime,start,t[0].endTime,stop);
						String no = trainNbrI.getText();
						String type = typeI.getText();
						String seat = Format.seatInfo(t);
						String price = Format.priceInfo(t);
						ModifyResult.result(o.code,uid,no,date,travel,type,seat, price);
						for(Ticket tic : InfoSearchResult.t) {
							DB.cancel(tic);
						}
						panel.setVisible(false);
						ModifyResult.panel.setVisible(true);
					}catch(BookException be) {
						ModifyFail.hint.setText(be.getErrMsg());
						panel.setVisible(false);
						ModifyFail.panel.setVisible(true);
					}
				
			}
		}
	}
	
}
