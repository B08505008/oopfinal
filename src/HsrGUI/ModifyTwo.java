package HsrGUI;
import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;


public class ModifyTwo {
	ModifyTwo(){
		panel.setLayout(new GridBagLayout());
		Format.setFormat(jc);
		trainNbrI.setFont(Format.f);
		setYear(yearI);
		setMonth(monthI);
		setDay(dayI,HSR_GUI.day31);
		setYear(year2I);
		setMonth(month2I);
		setDay(day2I,HSR_GUI.day31);
		setHour(hourI);
		setHour(hour2I);
		setMeans(meansI);
		setSeat(seatI);
		setDate();
		Format.setBlackTrans(to);
		Format.setBlackTrans(year);
		Format.setBlackTrans(month);
		Format.setBlackTrans(day);
		Format.setBlackTrans(year2);
		Format.setBlackTrans(month2);
		Format.setBlackTrans(day2);
		Format.setBlackTrans(time);
		Format.setBlackTrans(time2);
		Format.setBlackTrans(uidex);
		Format.setRedTrans(hint);
		Format.setBlackTrans(ticketI);
		Format.setBlackTrans(typeI);
		uidex.setFont(Format.fs);
		trainNbrI.setVisible(false);
		trainNbr2I.setVisible(false);
		
		addListener();

		for (int i = 0; i < jc.length; i++) {
			Format.add(panel,jc,layout,i);
		}
		Format.add(panel, jc, layout, 0);
	}
	
	

	static JPanel panel = new JPanel();
	static JButton back = new JButton("< 回上頁");
	static JLabel status = new JLabel("更改行程", SwingConstants.CENTER);
	static JLabel station = new JLabel("起訖站", SwingConstants.CENTER);
	static JLabel type = new JLabel("車廂種類", SwingConstants.CENTER);
	static JLabel typeI = new JLabel("標準車廂", SwingConstants.CENTER);
	static JLabel seat = new JLabel("座位喜好", SwingConstants.CENTER);
	static JLabel date = new JLabel("去程日期", SwingConstants.CENTER);
	static JLabel date2 = new JLabel("回程日期", SwingConstants.CENTER);
	static JLabel means = new JLabel("訂位方式", SwingConstants.CENTER);
	static JLabel time = new JLabel("去程", SwingConstants.CENTER);
	static JLabel time2 = new JLabel("回程", SwingConstants.CENTER);
	static JLabel number = new JLabel("車次", SwingConstants.CENTER);
	static JLabel ticket = new JLabel("票數", SwingConstants.CENTER);
	static JLabel ticketI = new JLabel("  ", SwingConstants.CENTER);
	
	static JLabel uid = new JLabel("訂票人代碼", SwingConstants.CENTER);
	static JTextField uidI = new JTextField();
	static JLabel uidex = new JLabel("本國籍：身分證字號 / 非本國籍：護照號碼",SwingConstants.CENTER);
	

	static JLabel to        = new JLabel("台北  往  台中",SwingConstants.CENTER);
	
	
	
	
	static JComboBox seatI= new JComboBox();
	
	static JComboBox yearI = new JComboBox();
	static JLabel 	 year  = new JLabel("年",SwingConstants.CENTER);
	static JComboBox monthI= new JComboBox();
	static JLabel    month = new JLabel("月",SwingConstants.CENTER);
	static JComboBox dayI  = new JComboBox();
	static JLabel day      = new JLabel("日",SwingConstants.CENTER);
	
	static JComboBox year2I = new JComboBox();
	static JLabel 	 year2  = new JLabel("年",SwingConstants.CENTER);
	static JComboBox month2I= new JComboBox();
	static JLabel    month2= new JLabel("月",SwingConstants.CENTER);
	static JComboBox day2I  = new JComboBox();
	static JLabel day2      = new JLabel("日",SwingConstants.CENTER);
	
	
	static JComboBox hourI  = new JComboBox();
	static JComboBox hour2I  = new JComboBox();
	
	
	static JComboBox meansI = new JComboBox();
	
	static JComboBox timeI = new JComboBox();
	static JTextField trainNbrI = new JTextField();
	static JTextField trainNbr2I = new JTextField();
	
	static JTextField numberI = new JTextField();
	
	
	
	static JCheckBox discount = new JCheckBox("限尚有優惠之車次");
	static JButton book = new JButton("開始更改");
	static JLabel hint = new JLabel(" ");
	
	private JComponent[] jc ={back,status,
							  station,to,
							  date,yearI,year,monthI,month,dayI,day,
							  date2,year2I,year2,month2I,month2,day2I,day2,
							  means,meansI,time,hourI,time2,hour2I,
							  trainNbrI, trainNbr2I,
							  type,typeI,
							  seat,seatI,
							  ticket,ticketI,
							  discount,book,hint};
	
	private int layout[][] = {
			{ 0, 0, 3, 1, 0, 0, Format.fill[3], Format.anchor[5] ,1,1,0,0,10,2}, { 3, 0, 12, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,10,0},
			
			
			{ 0, 4, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},{ 3, 4, 12, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},
			
			{ 0, 6, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0}, 
			{ 3, 6, 4, 1, 0, 0,Format.fill[0], Format.anchor[0] ,20,1,0,0,5,0},{ 7, 6, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,10,1,0,0,5,0},
			{ 8, 6, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 9, 6, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 10, 6, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 11, 6, 2, 1, 0, 0,Format.fill[1], Format.anchor[0] ,1,1,0,0,5,0},
			
			{ 0, 8, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0}, 
			{ 3, 8, 4, 1, 0, 0,Format.fill[0], Format.anchor[0] ,20,1,0,0,5,0},{ 7, 8, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,10,1,0,0,5,0},
			{ 8, 8, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 9, 8, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 10, 8, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 11, 8, 2, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			
			{ 0, 10, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 10, 4, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,3,5,0},
			{ 7, 10, 1, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,10,5,5},{ 8, 10, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 10, 10, 1, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 11, 10, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			
			{ 8, 10, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 11, 10, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,71,1,0,0,5,0},
			
			{ 0, 12, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 12, 4, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},
			{ 7, 12, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,10,5,5},{ 10, 12, 3, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},
			
			{ 0, 14, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},
			{ 3, 14, 10, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			
			{ 0, 16, 4, 1, 0, 0,Format.fill[0], Format.anchor[7] ,1,1,0,0,0,0},{ 10, 16, 3, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,0,0},
			{ 4, 16, 6, 1, 0, 0,Format.fill[3], Format.anchor[0] ,1,1,0,0,0,0},
			};
	private int trainNumberLayout[][] ={
			{ 3, 4, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
	};
	
	private void addListener() {
		back.addMouseListener(new BackListener());
		meansI.addActionListener(new MeansIListener());
		monthI.addActionListener(new MonthIListener());
		book.addMouseListener(new BookListener());
		
	}
	
	static void setStation(JComboBox jcbbx) {
		for (int i = 0;i<HSR_GUI.station.length;i++ ) {
			jcbbx.addItem(HSR_GUI.station[i]);
		}
	}
	
	static void setYear(JComboBox jcbbx) {
		for (int i = 0;i<HSR_GUI.year.length;i++ ) {
			jcbbx.addItem(HSR_GUI.year[i]);
		}
	}
	
	static void setMonth(JComboBox jcbbx) {
		for (int i = 0;i<HSR_GUI.month.length;i++ ) {
			jcbbx.addItem(HSR_GUI.month[i]);
		}
	}
	
	static void setDay(JComboBox jcbbx,int[] ia) {
		jcbbx.removeAllItems();
		for (int i = 0;i<ia.length;i++ ) {
			jcbbx.addItem(ia[i]);
		}
	}
	
	static void setHour(JComboBox jcbbx) {
		jcbbx.removeAllItems();
		for (int i = 6;i<24;i++ ) {
			jcbbx.addItem(Integer.toString(i)+" : 00");
			jcbbx.addItem(Integer.toString(i)+" : 30");
		}
	}
	
	static void setMin(JComboBox jcbbx) {
		jcbbx.removeAllItems();
		jcbbx.addItem(00);
		jcbbx.addItem(30);
	}
	
	static void setMeans(JComboBox jcbbx) {
		for (int i = 0;i<HSR_GUI.means.length;i++ ) {
			jcbbx.addItem(HSR_GUI.means[i]);
		}
	}
	
	static void setType(JComboBox jcbbx) {
		for (int i = 0;i<HSR_GUI.carType.length;i++ ) {
			jcbbx.addItem(HSR_GUI.carType[i]);
		}
	}
	
	static void setSeat(JComboBox jcbbx) {
		for (int i = 0;i<HSR_GUI.seatPrefer.length;i++ ) {
			jcbbx.addItem(HSR_GUI.seatPrefer[i]);
		}
	}
	
	static void setTicket(JComboBox jcbbx) {
		for (int i = 0;i<HSR_GUI.ticket.length;i++ ) {
			jcbbx.addItem(HSR_GUI.ticket[i]);
		}
	}
	
	static void reset()	{
		
	}
	static void setDate() {
		Date date = new Date();
		dayI.setSelectedItem(date.getDate());
		monthI.setSelectedItem(date.getMonth()+1);
		yearI.setSelectedItem(date.getYear());
		String hour = Integer.toString(date.getHours())+" : "+((date.getMinutes()>29)?"30":"00");
		hourI.setSelectedItem(hour);
		day2I.setSelectedItem(date.getDate());
		month2I.setSelectedItem(date.getMonth()+1);
		year2I.setSelectedItem(date.getYear());
		hour2I.setSelectedItem(hour);
		
	}
	
	class BackListener extends HsrGUI.BackListener{
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			HomePage.panel.setVisible(true);
		}
	}
	
	static void changeMeans(JComponent jc, int layout[][], int i) {
		GridBagConstraints c1 = new GridBagConstraints();
		c1.gridx = layout[i][0];
		c1.gridy = layout[i][1];
		c1.gridwidth = layout[i][2];
		c1.gridheight = layout[i][3];
		c1.weightx = layout[i][4];
		c1.weighty = layout[i][5];
		c1.fill = layout[i][6];
		c1.anchor = layout[i][7];
		c1.ipadx = layout[i][8];
		c1.ipady = layout[i][9];
		c1.insets = new Insets(layout[i][10], layout[i][11], layout[i][12], layout[i][13]);
		System.out.println(panel.getComponent(i));
		panel.getComponent(i).setVisible(false);
		panel.remove(i);
		panel.add(jc, c1, i);
		jc.setVisible(true);
		
	}
	
	static void changeMeans(String s) {
		if(s.equals("依車次")) {
			hourI.setVisible(false);
			hour2I.setVisible(false);
			trainNbrI.setVisible(true);
			trainNbr2I.setVisible(true);
		}
		else {
			trainNbrI.setVisible(false);
			trainNbr2I.setVisible(false);
			hourI.setVisible(true);
			hour2I.setVisible(true);
		}
		
	}
	
	class MeansIListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			changeMeans((String)meansI.getSelectedItem());
		}
	}
	
	class MonthIListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			int i = dayI.getSelectedIndex();
			switch (monthI.getSelectedIndex()+1){
			case 1:case 3:case 5:case 7:case 8:case 10:case 12:
				setDay(dayI,HSR_GUI.day31);
				break;
			case 4:case 6:case 9:case 11:
				setDay(dayI,HSR_GUI.day30);
				break;
			case 2:
				setDay(dayI,HSR_GUI.day28);
				break;
			}
			if(dayI.getItemCount()>i) {
				dayI.setSelectedIndex(i);
			}
			else {
				dayI.setSelectedIndex(dayI.getItemCount()-1);
			}
			
		}
	}
	
	class BookListener extends AllListener{
		
		@Override
		public void mouseClicked(MouseEvent e) {
//			if(uidI.getText().equals("")) {
//				hint.setText("請輸入訂票人代碼");
//			}else if (meansI.getSelectedIndex()==1&&trainNbrI.getText().equals("")) {
//				hint.setText("請輸入車次");
//			}else if (normalI.getSelectedIndex()==0&&childI.getSelectedIndex()==0&&oldI.getSelectedIndex()==0&&loveI.getSelectedIndex()==0) {
//				hint.setText("請選擇訂票數量");
//			}else {
				/*hint.setText(" ");
				BasicTime.setInfo();
				BasicTime.result();
				panel.setVisible(false);
				if(meansI.getSelectedIndex()==1) {
					BasicResult.uidI.setText(uidI.getText());
					BasicResult.dateI.setText(Format.dateToString((int)yearI.getSelectedItem(), (int)monthI.getSelectedItem(), (int)dayI.getSelectedItem()));
					BasicResult.stationI.setText(Format.travelToString("18:00", (String)startI.getSelectedItem(), "19:00",(String)stopI.getSelectedItem() ));
					BasicResult.panel.setVisible(true);
				}else {*/
					panel.setVisible(false);
					TwoWayResult.panel.setVisible(true);
				//}
				
			}
		
		
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setBlackOrange(book);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setOrangeBlack(book);
		}
	}
	
	

}
