package HsrGUI;

import javax.swing.*;

import javax.swing.event.*;

import HSR.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.Date;

public class Format {
	static JButton jb = new JButton();
	static JTextField jtf = new JTextField();
	static JLabel jl = new JLabel();
	static JCheckBox jckb = new JCheckBox();
	static JComboBox jcbb = new JComboBox();

	static final int fill[] = { GridBagConstraints.BOTH, GridBagConstraints.VERTICAL, GridBagConstraints.HORIZONTAL,
			GridBagConstraints.NONE };
	static final int anchor[] = { GridBagConstraints.CENTER, GridBagConstraints.EAST, GridBagConstraints.SOUTHEAST,
			GridBagConstraints.SOUTH, GridBagConstraints.SOUTHWEST, GridBagConstraints.WEST,
			GridBagConstraints.NORTHWEST, GridBagConstraints.NORTH, GridBagConstraints.NORTHEAST };
	
	static Font fb = new Font("微軟正黑體", Font.BOLD, 30);
	static Font f16 = new Font("微軟正黑體", Font.BOLD, 16);
	static Font f = new Font("微軟正黑體", Font.BOLD, 15);
	static Font fs = new Font("微軟正黑體", Font.BOLD, 13);
	static String[] day = {"一","二","三","四","五","六","日",};
	
	static void add(JPanel panel, JComponent[] jc, int[][] layout, int i) { //將JComponent以指定格式位置加入panel
		GridBagConstraints c1 = new GridBagConstraints();
		c1.gridx = layout[i][0];
		c1.gridy = layout[i][1];
		c1.gridwidth = layout[i][2];
		c1.gridheight = layout[i][3];
		c1.weightx = layout[i][4];
		c1.weighty = layout[i][5];
		c1.fill = layout[i][6];
		c1.anchor = layout[i][7];
		c1.ipadx = layout[i][8];
		c1.ipady = layout[i][9];
		c1.insets = new Insets(layout[i][10], layout[i][11], layout[i][12], layout[i][13]);
		panel.add(jc[i], c1);
	}
	
	static void add(JPanel panel, JComponent jc, int[] layout) { //將JComponent以指定格式位置加入panel
		GridBagConstraints c1 = new GridBagConstraints();
		c1.gridx = layout[0];
		c1.gridy = layout[1];
		c1.gridwidth = layout[2];
		c1.gridheight = layout[3];
		c1.weightx = layout[4];
		c1.weighty = layout[5];
		c1.fill = layout[6];
		c1.anchor = layout[7];
		c1.ipadx = layout[8];
		c1.ipady = layout[9];
		c1.insets = new Insets(layout[10], layout[11], layout[12], layout[13]);
		panel.add(jc, c1);
	}

	static void setFormat(JComponent[] jc) { //根據JComponent類型設定格式
		setButton(jc);
		setLabel(jc);
		setCheckBox(jc);
		setComboBox(jc);
		setTextField(jc);
	}

	static void setTextField(JComponent[] jc) { //設定JTextField格式
		for (int i = 0; i < jc.length; i++) {
			if (jc[i].getClass() == jtf.getClass()) {
				jc[i].setFont(f);

			}
		}

	}

	static void setButton(JComponent[] jc) {//設定JButton格式
		for (int i = 0; i < jc.length; i++) {
			if (jc[i].getClass() == jb.getClass()) {
				jc[i].setFont(f);
				setOrangeBlack(jc[i]);
			}
		}
	}

	static void setLabel(JComponent[] jc) {//設定JLabel格式
		for (int i = 0; i < jc.length; i++) {
			if (jc[i].getClass() == jl.getClass()) {
				jc[i].setFont(f);
				setOrangeBlack(jc[i]);
			}
		}
	}

	static void setCheckBox(JComponent[] jc) {//設定JCheckBox格式
		for (int i = 0; i < jc.length; i++) {
			if (jc[i].getClass() == jckb.getClass()) {
				jc[i].setFont(f);
			}
		}
	}

	static void setComboBox(JComponent[] jc) {//設定JComboBox格式
		for (int i = 0; i < jc.length; i++) {
			if (jc[i].getClass() == jcbb.getClass()) {
				jc[i].setFont(f);
				jc[i].setForeground(Color.BLACK);
				jc[i].setOpaque(true);
				jc[i].setBackground(Color.WHITE);
			}
		}
	}

	static void setWhiteBlack(JComponent jc) {//將JComponent設為白底黑字
		jc.setOpaque(true);
		jc.setForeground(Color.BLACK);
		jc.setBackground(Color.WHITE);
	}

	static void setBlackTrans(JComponent jc) {//將JComponent設為透明底黑字
		jc.setOpaque(false);
		jc.setForeground(Color.BLACK);
	}
	
	static void setOrangeBlack(JComponent jc) {//將JComponent設為橘底黑字
		jc.setOpaque(true);
		jc.setForeground(Color.BLACK);
		jc.setBackground(Color.ORANGE);
	}
	
	static void setBlackOrange(JComponent jc) {//將JComponent設為黑底橘字
		jc.setOpaque(true);
		jc.setForeground(Color.ORANGE);
		jc.setBackground(Color.BLACK);
	}

	static void setBlackWhite(JComponent jc) {//將JComponent設為黑底白字
		jc.setOpaque(true);
		jc.setForeground(Color.WHITE);
		jc.setBackground(Color.BLACK);
		
	}
	
	static void setRedTrans(JComponent jc) {//將JComponent設為透明底紅字
				jc.setForeground(Color.RED);
				jc.setOpaque(false);
	}
	
	static String toStdDate(int i, int j, int k) {
		String month = (j<10)?("0"+j):Integer.toString(j);
		String day 	 = (k<10)?("0"+k):Integer.toString(k);
		return i+month+day;
	}
	
	static String dateToString(int i,int j,int k) {//將年月日轉成字串形式
		return i+" 年 "+j+" 月 "+k+" 日 ";
	}
	
	static String dateToString(JComboBox yearI,JComboBox monthI,JComboBox dayI) {//將年月日轉成字串形式
		return (int)yearI.getSelectedItem()+" 年 "+(int)monthI.getSelectedItem()+" 月 "+ (int)dayI.getSelectedItem()+" 日 ";
	}
	
	static String stationToString(String i , String j) {//將起訖站轉成字串形式
		return i +  " 往 " + j;
	}
	
	static String travelToString(Timing h, String i ,Timing j ,String k) {//將起訖站時間轉成字串形式
		return h+ " "+i +  "  -  " + j +" "+k;
	}
	
	static String transStationID(String s){//輸入站名ID，回傳站名
		switch (s) {
			case"0990":
				return "南港";
			case"1000":
				return "台北";
			case"1010":
				return "板橋";
			case"1020":
				return "桃園";
			case"1030":
				return "新竹";
			case"1035":
				return "苗栗";
			case"1040":
				return "台中";
			case"1043":
				return "彰化";
			case"1047":
				return "雲林";			
			case"1050":
				return "嘉義";
			case"1060":
				return "台南";
			case"1070":
				return "左營";
			default:
				return null;
		}
	}
	
	static String toStationID(String s){//輸入站名，回傳站名ID
		switch (s) {
		case"南港":
			return "0990";
		case"台北":
			return "1000";
		case"板橋":
			return "1010";
		case"桃園":
			return "1020";
		case"新竹":
			return "1030";
		case"苗栗":
			return "1035";
		case"台中":
			return "1040";
		case"彰化":
			return "1043";
		case"雲林":
			return "1047";			
		case"嘉義":
			return "1050";
		case"台南":
			return "1060";
		case"左營":
			return "1070";
		default:
			return null;
	}
	}
	
	static String serviceToString(int[] i) {//將停駛日轉成字串形式
		String service="";
		for(int j = 0;j<i.length;j++) {
			if(i[j]==1) {
				service+=day[j];
			}
		}
		
		if(service.equals("一二三四五")) {
			service = "平日";
		}else if(service.equals("一二三四五六日")) {
			service = " ";
		}else if(service.equals("六日")) {
			service = "假日";
		}
		
		return service;
	}
	
	static String discountToString(double d, boolean ear) { //將折數轉為字串
		int i = (int) (100*d);

		if(i == 100||i==0) {
			return "無";
		}
		String discount = Integer.toString(i);
		
		if(discount.charAt(1) == '0') {
			discount = Character.toString(discount.charAt(0));
		}
		
		if(ear == true) {
			discount =  "早鳥 "+discount+" 折";
		}else {
			discount =  discount+" 折";
		}
		return discount;		
	}
	
	static GUITrainInfo[] sortByStart (GUITrainInfo[] temp) { //依起站時間分類車次
		GUITrainInfo[] sorted = new GUITrainInfo[temp.length];
		System.out.println("sortByStart");
		for (int i = 0; i < temp.length; i++) {
			Timing st = new Timing("24:00");
			int si = i;
			for (int k = 0; k < temp.length; k++) {
				if (temp[k] != null) {
					st = temp[k].staToTime.get(temp[k].startingStation);
					si = k;
					break;
				}

			}

		for (int j = 0; j < temp.length; j++) {
			if (temp[j] != null) {
				if (st.larger(temp[j].staToTime.get(temp[j].startingStation))) {
					st = temp[j].staToTime.get(temp[j].startingStation);
					si = j;
				}
			}
		}

			sorted[i] = temp[si];
			temp[si] = null;
		}
		return sorted;
	}
	
	static GUITrainInfo[] sortByTc (GUITrainInfo[] temp) { //依台中離站時間分類車次
		GUITrainInfo[] sorted = new GUITrainInfo[temp.length];

		for (int i = 0; i < temp.length; i++) {
			Timing st = new Timing("24:00");
			int si = i;
			for (int k = 0; k < temp.length; k++) {
				if (temp[k] != null) {
					st = temp[k].tc;
					si = k;
					break;
				}

			}

		for (int j = 0; j < temp.length; j++) {
			if (temp[j] != null) {
				if (st.larger(temp[j].tc)) {
					st = temp[j].tc;
					si = j;
				}
			}
		}

			sorted[i] = temp[si];
			temp[si] = null;
		}
		return sorted;
	}
	
	static GUITrainInfo toGUITrainInfo(TrainInfo t) { //將TrainInfo物件轉成GUITrainInfo物件
		String trainNo = t.getTrainNo();
		int direction = t.getDirection();
		String startingStation = Format.transStationID(t.getStartingStation());
		String endingStation = Format.transStationID(t.getEndingStation());
		ArrayList<String> stopStations = t.getStopStations();
		ArrayList<Timing> departureTimes = new ArrayList<>();
		for (int j = 0; j < t.getDepatureTimes().size(); j++) {
			departureTimes.add(new Timing(t.getDepatureTimes().get(j)));
		}
		int[] service = t.getService();
		double uni = t.universityDiscount;
		double ear = t.earlyDiscount;
		GUITrainInfo temp  = new GUITrainInfo(trainNo, direction, startingStation, endingStation, stopStations,
				departureTimes, service,uni,ear);
		return temp;
	}
	
	static GUITrainInfo[] toGUITrainInfoArray(TrainInfo[] all, int direction ) { //將特定方向之TrainInfo取出轉成GUITrainInfo陣列
		int nbr = 0;
		
		for(TrainInfo t : all) { 
			if(t.getDirection()==direction) {
				continue;
			}else {
				nbr++;
			}
		}
		
		GUITrainInfo[] temp = new GUITrainInfo[nbr];
		int counter = 0;
		
		for(int i = 0;i<all.length;i++) { 
			if(all[i].getDirection()==direction) {
				continue;
			}else {
				temp[counter] = toGUITrainInfo(all[i]);
				counter++;
			}
		}
		
		return temp;
	}
	
	static GUITrainInfo[] toGUITrainInfoArray(TrainInfo[] all) {//將TrainInfo陣列轉成GUITrainInfo陣列
		GUITrainInfo[] temp = new GUITrainInfo[all.length];
		
		for(int i = 0;i<all.length;i++) { //設定Table
			temp[i] = Format.toGUITrainInfo(all[i]);
		}
		
		return temp;
	}

	static boolean invalidDate(int i , int j) { //回傳是否為無效訂票日期
		Date d = new Date();
		int month = d.getMonth()+1;
		int day = d.getDate();
		
		if(month > i) {
			return true;
		}else if(month == i) {
			if(j<day) {
				return true;
			}else if((j-day)>28) {
				return true;
			}else {
				return false;
			}
		}else if(i-month > 1){
			return true;
		}else {
			switch (month) {
			case 1:case 3:case 5:case 7:case 8:case 10:case 12:
				if(j+31-day > 28) {
					return true;
				}else {
					return false;
				}
			case 4:case 6:case 9:case 11:
				if(j+30-day > 28) {
					return true;
				}else {
					return false;
				}
			case 2:
				if(j+28-day > 28) {
					return true;
				}else {
					return false;
				}
			default:
				return false;
			}
		}
	}
	
	static void setYear(JComboBox jcbbx) { //加入年的選項至JComboBox
		for (int i = 0;i<HSR_GUI.year.length;i++ ) {
			jcbbx.addItem(HSR_GUI.year[i]);
		}
	}
	
	static void setMonth(JComboBox jcbbx) {//加入月的選項至JComboBox
		for (int i = 0;i<HSR_GUI.month.length;i++ ) {
			jcbbx.addItem(HSR_GUI.month[i]);
		}
	}
	
	static void setDay(JComboBox jcbbx,int[] ia) {//加入日的選項至JComboBox
		jcbbx.removeAllItems();
		for (int i = 0;i<ia.length;i++ ) {
			jcbbx.addItem(ia[i]);
		}
	}
	
	static void setHour(JComboBox jcbbx) {//加入時間的選項至JComboBox
		jcbbx.removeAllItems();
		for (int i = 6;i<24;i++ ) {
			jcbbx.addItem(Integer.toString(i)+":00");
			jcbbx.addItem(Integer.toString(i)+":30");
		}
	}
	
	static void setMeans(JComboBox jcbbx) {//加入訂票方式的選項至JComboBox
		for (int i = 0;i<HSR_GUI.means.length;i++ ) {
			jcbbx.addItem(HSR_GUI.means[i]);
		}
	}
	
	static void setType(JComboBox jcbbx) {//加入車廂種類的選項至JComboBox
		for (int i = 0;i<HSR_GUI.carType.length;i++ ) {
			jcbbx.addItem(HSR_GUI.carType[i]);
		}
	}
	
	static void setSeat(JComboBox jcbbx) {//加入座位喜好的選項至JComboBox
		for (int i = 0;i<HSR_GUI.seatPrefer.length;i++ ) {
			jcbbx.addItem(HSR_GUI.seatPrefer[i]);
		}
	}
	
	static void setTicket(JComboBox jcbbx) {//加入票數的選項至JComboBox
		for (int i = 0;i<HSR_GUI.ticket.length;i++ ) {
			jcbbx.addItem(HSR_GUI.ticket[i]);
		}
	}
	
	static void setTicketTwoWay(JComboBox jcbbx) {//加入票數的選項至JComboBox(來回票用)
		for (int i = 0;i<HSR_GUI.ticketTwoWay.length;i++ ) {
			jcbbx.addItem(HSR_GUI.ticketTwoWay[i]);
		}
	}
	
	static void setStation(JComboBox jcbbx) {//加入車站的選項至JComboBox
		for (int i = 0;i<HSR_GUI.station.length;i++ ) {
			jcbbx.addItem(HSR_GUI.station[i]);
		}
	}
	
	static int toSeatPre(JComboBox j) { //設定座位喜好代碼
		switch(j.getSelectedIndex()) {
			case 0:
			return 3;
			case 1:
				return 0;
			case 2:
				return 2;
			default:
				return 3;
		}
	}
	
	static int[] toAmounts (JComboBox[] j) { //設定訂票數量
		int[] i = new int[6];
		int index = 0;
		for(int k = 0 ; k < i.length ; k++) {
			if(k == 4) {
				i[k] = 0;
				continue;
			}else {
				i[k] = j[index].getSelectedIndex();
			}
			index++;
		}
		return i ;
	}
	
	static String seatInfo (Ticket[] t) { //設定顯示座位資訊
		String info = "";
		for(int i = 0 ; i < t.length ; i++) {
			if(info!="") {
				info += "、";
			}
			String seat = t[i].seat.getCar()+ "車" + t[i].seat.getRow() + "排" + t[i].seat.getSeatCode();
			info+=seat;
		}
		return info;
	}
	
	static String seatInfo (Ticket t) { //設定顯示座位資訊
		String seat = t.seat.getCar()+ "車" + t.seat.getRow() + "排" + t.seat.getSeatCode();
		return seat;
	}
	
	static String priceInfo (Ticket[] t) { //設定顯示付款資訊
		String info = "";
		int price = 0 ;
		for(int i = 0 ; i < t.length ; i++) {
			price+=t[i].price;
		}
		int date[] = dateToInt(t[0].payDeadLine);
		info = "共 "+price+" 元，請在 "+date[0]+" 年 "+date[1]+" 月 "+date[2]+" 日前 完成付款";
		return info;
	}
	
	 static int[] dateToInt(String d){ //將日期轉換成整數陣列
		int[] r = new int[3];			
		r[0] = Integer.parseInt(d.substring(0, 4));
		r[1] = Integer.parseInt(d.substring(4, 6));
		r[2] = Integer.parseInt(d.substring(6, 8));
		return r;			
	}
	 
	 static String stdDateToString (String d) { //將標準日期字串轉換
		 	int date[] = dateToInt(d);
		 	return date[0]+" 年 "+date[1]+" 月 "+date[2]+" 日 ";
	}
	 static String stdDateNow () { //產生今天標準日期字串
		 Date now = new Date();
		 String year = Integer.toString(now.getYear());
		 String month = (now.getMonth()+1 < 10)?("0"+Integer.toString(now.getMonth()+1)):Integer.toString(now.getMonth()+1);
		 String day = (now.getDate() < 10)?("0"+Integer.toString(now.getDate())):Integer.toString(now.getDate());
		 return year+month+day;
	}
	 static String toAmountInfo(int[] i) {
		 String info = "";
		for(int j = 0; j<6;j++) {
			String type;
			switch (j) {
			case 0:case 4:
				type = "全票";
				break;
			case 1:
				type = "孩童";
				break;
			case 2:
				type =  "敬老";
				break;
			case 3:
				type = "愛心";
				break;
			case 5:
				type = "大學生";
				break;
			default:
				type = "無效票";
			}
			if(i[j]!=0) {
				if(!info.equals("")) {
					info+="、";
				}
				info+= (type+i[j]+"張");
			}
			
			
		}
		return info;
	 }
}
