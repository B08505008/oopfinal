package HsrGUI;

import java.awt.*;
import java.awt.event.*;
import java.util.Date;
import javax.swing.*;
import HSR.*;

public class InfoSearch {
	InfoSearch(){
		panel.setLayout(new GridBagLayout());
		Format.setFormat(jc);
		bidI.setFont(Format.f);
		Format.setRedTrans(hint);
		uidex.setFont(Format.fs);
		Format.setBlackTrans(uidex);
		addListener();
		for (int i = 0; i < jc.length; i++) {
			Format.add(panel,jc,layout,i);
		}
	}
	
	static JPanel panel = new JPanel();
	static JButton back = new JButton("< 回上頁");
	static JLabel status = new JLabel("訂位資訊查詢", SwingConstants.CENTER);
	static JLabel uid = new JLabel("訂票人代碼", SwingConstants.CENTER);
	static JLabel bid = new JLabel("訂位代碼", SwingConstants.CENTER);
	static JTextField uidI = new JTextField();
	static JLabel uidex = new JLabel("身分證/護照號碼",SwingConstants.CENTER);
	static JTextField bidI = new JTextField();
	static JButton search = new JButton("開始查詢");
	static JLabel hint = new JLabel(" ",SwingConstants.CENTER);
	static JComponent[] jc ={back,status,
							 uid, uidI,uidex,
							 bid,bidI,search,
							 hint,};
	
	static int layout[][] = {
			{ 0, 0, 3, 1, 0, 0, Format.fill[3], Format.anchor[5] ,1,1,0,0,10,2}, { 3, 0, 12, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,10,0},
			
			{ 0, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 1, 6, 1, 0, 0, Format.fill[0], Format.anchor[5] ,200,5,0,3,5,0},
			{ 9, 1, 2, 1, 0, 0, Format.fill[0], Format.anchor[5] ,20,5,0,0,5,0},
		
			{0, 4, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 3, 4, 6, 1, 0, 0, Format.fill[0], Format.anchor[0] ,200,5,0,3,5,0},
			{ 9, 4, 3, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,3,5,0},
			
			{ 3, 5, 6, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,3,5,0},
			};
	
	 void addListener() {
		back.addMouseListener(new BackListener());
		search.addMouseListener(new SearchListener());
	}
	 
	class BackListener extends HsrGUI.BackListener{ //回上頁
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			HomePage.panel.setVisible(true);
		}
	}
	
	static void reset() {
		uidI.setText("");
		bidI.setText("");
	}
	
	class SearchListener extends AllListener{ //搜尋訂位資訊
		@Override
		public void mouseClicked(MouseEvent e) {
			if (uidI.getText().equals("") || bidI.getText().equals("")) {
				hint.setText("請完整輸入資訊");
			} else {
				String uid = uidI.getText();
				int bid = Integer.parseInt(bidI.getText());
				try {
					Ticket[] t = DB.search(uid, bid);
					for (Ticket b : t) {
						System.out.println(b.toString());
					}
					Timing now = new Timing(new Date());
					Timing trainTime = t[0].startTime;
					String date = Format.stdDateToString(t[0].Date);
					String start = Format.transStationID(t[0].startStation);
					String stop = Format.transStationID(t[0].endStation);
					String travel = Format.travelToString(t[0].startTime, start, t[0].endTime, stop);
					String no = t[0].trainNo;
					String type = (t[0].standard) ? "標準車廂" : "商務車廂";
					String seat = Format.seatInfo(t);
					String price = Format.priceInfo(t);
					String sta = Format.stationToString(start, stop);
					InfoSearchResult.result(t[0].code, uid, no, date, travel, type, seat, price, t, sta);
					panel.setVisible(false);
					InfoSearchResult.panel.setVisible(true);
				} catch (BookException be) {
					hint.setText(be.getErrMsg());
				}

			}

		}
		
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setBlackOrange(search);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setOrangeBlack(search);
		}
	}
	

}
