package HsrGUI;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class HSR_GUI {
	static JFrame frame = new JFrame("台灣高鐵訂票系統 Taiwan High Speed Rail Booking System");
	private Font f= new Font("微軟正黑體",Font.BOLD,15);
	private HomePage hp=new HomePage();
	private BasicBook bb = new BasicBook();
	private BasicTime bt = new BasicTime();
	private BasicResult br = new BasicResult();
	private BasicFail bf = new BasicFail();
	private TwoWayBook twb = new TwoWayBook();
	private IdSearch ids = new IdSearch();
	private IdSearchResult idsr = new IdSearchResult();
	private InfoSearch ifs = new InfoSearch();
	private InfoSearchResult ifsr = new InfoSearchResult();
	private InfoSearchResultTwo ifsr2 = new InfoSearchResultTwo();
	private Modify m = new Modify();
	private ModifyTwo mt = new ModifyTwo();
	private ModifyResult mr = new ModifyResult();
	private AllTrainN atn = new AllTrainN();
	private AllTrainS ats = new AllTrainS();
	private ConditionSearch cs = new ConditionSearch();
	private ConditionResult cdr = new ConditionResult();
	private PartCancel pc = new PartCancel();
	private CancelCheck cc = new CancelCheck();
	private CancelResult cr = new CancelResult();
	private TwoWayResult twr = new TwoWayResult();
	
	static String[] station = {"南港","台北","板橋","桃園","新竹","苗栗","台中","彰化","雲林","嘉義","台南","左營"};
	static String[] stationR = {"左營","台南","嘉義","雲林","彰化","台中","苗栗","新竹","桃園","板橋","台北","南港"};
	static int[] year = {2021};
	static int[] month = {1,2,3,4,5,6,7,8,9,10,11,12};
	static int[] day31 = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31};
	static int[] day28 = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28};
	static int[] day30 = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30};
	static String[] means = {"依時間","依車次"};
	static String[] carType = {"標準車廂","商務車廂"};
	static String[] seatPrefer = {"無","靠窗","靠走道"};
	static String[] ticketType = {"全票","孩童","敬老","愛心","大學生"};
	static String[] discountType = {"不指定","早鳥優惠","大學生優惠","任一"};
	static int[] ticket = {0,1,2,3,4,5,6,7,8,9,10};
	static int[] ticketTwoWay = {0,1,2,3,4,5};
	
	public HSR_GUI() {
		frame.setSize(1200, 710);
		frame.setLayout(new GridBagLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.add(HomePage.panel);
		frame.add(BasicBook.panel);
		frame.add(BasicTime.panel);
		frame.add(BasicResult.panel);
		frame.add(BasicFail.panel);
		frame.add(TwoWayBook.panel);
		frame.add(IdSearch.panel);
		frame.add(IdSearchResult.panel);
		frame.add(InfoSearch.panel);
		frame.add(InfoSearchResult.panel);
		frame.add(Modify.panel);
		frame.add(mt.panel);
		frame.add(ModifyResult.panel);
		frame.add(atn.panel);
		frame.add(ats.panel);
		frame.add(ConditionSearch.panel);
		frame.add(PartCancel.panel);
		frame.add(cc.panel);
		frame.add(cr.panel);
		frame.add(twr.panel);
		frame.add(ifsr2.panel);
		frame.add(cdr.panel);
	
		BasicBook.panel.setVisible(false);
		BasicTime.panel.setVisible(false);
		BasicResult.panel.setVisible(false);
		BasicFail.panel.setVisible(false);
		TwoWayBook.panel.setVisible(false);
		IdSearch.panel.setVisible(false);
		IdSearchResult.panel.setVisible(false);
		InfoSearch.panel.setVisible(false);
		InfoSearchResult.panel.setVisible(false);
		Modify.panel.setVisible(false);
		mt.panel.setVisible(false);
		ModifyResult.panel.setVisible(false);
		atn.panel.setVisible(false);
		ats.panel.setVisible(false);
		ConditionSearch.panel.setVisible(false);
		PartCancel.panel.setVisible(false);
		cc.panel.setVisible(false);
		cr.panel.setVisible(false);
		cdr.panel.setVisible(false);
		twr.panel.setVisible(false);
		ifsr2.panel.setVisible(false);
		
		frame.setVisible(true);

	}
		
}
