package HsrGUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class CancelCheck {
	
	CancelCheck(){
		panel.setLayout(new GridBagLayout());
		Format.setFormat(jc);
		topic.setFont(Format.f16);
		
		for (int i = 0; i < jc.length; i++) {
			Format.add(panel,jc,layout,i);
		}
		Format.setBlackTrans(hint);
		Format.setWhiteBlack(reselect);
		Format.setWhiteBlack(back);
		Format.setWhiteBlack(yes);
		
		addListener();
	}
	
	static JPanel panel = new JPanel();
	static JLabel  status = new JLabel("一般訂票",SwingConstants.CENTER);
	static JLabel topic = new JLabel("以下是您將退訂的票券", SwingConstants.CENTER);
	static JButton reselect = new JButton("重選車票");
	static JButton back = new JButton("放棄退票");
	static JButton yes = new JButton("確定退票");
	static JLabel hint = new JLabel(" ", SwingConstants.CENTER);
	static JComponent[] jc = {topic,
							  hint,
							  reselect,yes,back};
	static int layout [][] ={
			{ 0, 0, 12, 1, 0, 0, Format.fill[0], Format.anchor[5],1,10 ,0,0,5,1},
			
			{ 0, 1, 12, 1, 0, 0, Format.fill[0], Format.anchor[5],1,10 ,0,0,5,1},
			
			
			{ 4, 6, 3, 1,0 , 0, Format.fill[3], Format.anchor[1],20,1 ,0,0,5,0},{ 7, 6, 2, 1,0 , 0, Format.fill[3], Format.anchor[1],20,1 ,0,0,5,0},
			{ 9, 6, 3, 1,0 , 0, Format.fill[3], Format.anchor[1],20,1 ,0,0,5,0},
			};
	
	public void addListener() {//加入所有監聽器
		back.addMouseListener(new BackListener());
		yes.addMouseListener(new YesL());
		reselect.addMouseListener(new ReselectL());
	}
	
	class BackListener extends HsrGUI.BackListener{//返回首頁
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			HomePage.panel.setVisible(true);
		}
	}
	
	class ReselectL extends AllListener{ //返回選擇車票頁面
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			PartCancel.panel.setVisible(true);
		}
	}
	
	class YesL extends AllListener{ //依情況跳轉至退票成功/失敗頁面
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			CancelResult.panel.setVisible(true);
		}
	}
}
