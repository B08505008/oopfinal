package HsrGUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class InfoSearchResultTwo {
	
	InfoSearchResultTwo(){
		panel.setLayout(new GridBagLayout());
		Format.setFormat(jc);
		for(JComponent j : jcbt) {
			Format.setBlackTrans(j);
		}
		for (int i = 0; i < jc.length; i++) {
			Format.add(panel,jc,layout,i);
		}
		
		Format.setWhiteBlack(cancel);
		Format.setWhiteBlack(part);
		Format.setWhiteBlack(modify);
		
		addListener();
	}
	
	static JPanel panel = new JPanel();
	
	
	static JLabel topic = new JLabel("                                       訂位資訊", SwingConstants.LEFT);
	static JButton part = new JButton("部分退票");
	static JButton back = new JButton("< 回上頁");
	static JButton modify = new JButton("更改訂位");
	static JButton cancel = new JButton("取消訂位");
	
	static JLabel bid = new JLabel("訂位代碼", SwingConstants.CENTER);
	static JLabel uid = new JLabel("訂位人代碼", SwingConstants.CENTER);
	static JLabel price = new JLabel("票價", SwingConstants.CENTER);
	
	static JLabel info = new JLabel("      去程資訊", SwingConstants.CENTER);
	static JLabel date = new JLabel("乘車日期", SwingConstants.CENTER);
	static JLabel trainNbr = new JLabel("搭乘車次", SwingConstants.CENTER);
	static JLabel station = new JLabel("起訖時間", SwingConstants.CENTER);
	static JLabel type = new JLabel("車廂種類", SwingConstants.CENTER);
	static JLabel seat= new JLabel("座位資訊", SwingConstants.CENTER);
	
	static JLabel info2 = new JLabel("      回程資訊", SwingConstants.CENTER);
	static JLabel date2 = new JLabel("乘車日期", SwingConstants.CENTER);
	static JLabel trainNbr2 = new JLabel("搭乘車次", SwingConstants.CENTER);
	static JLabel station2 = new JLabel("起訖時間", SwingConstants.CENTER);
	static JLabel type2 = new JLabel("車廂種類", SwingConstants.CENTER);
	static JLabel seat2 = new JLabel("座位資訊", SwingConstants.CENTER);
	
	static JLabel bidI = new JLabel("202106192055", SwingConstants.CENTER);
	static JLabel uidI = new JLabel("55555", SwingConstants.CENTER);
	static JLabel priceI = new JLabel("共 1450 元，請在2021 年 6 月 25 日 23:59 前 完成付款", SwingConstants.CENTER);
	
	static JLabel dateI = new JLabel("2021 年 6 月 25 日", SwingConstants.CENTER);
	static JLabel stationI = new JLabel("18:00 台北 - 19:00 台中", SwingConstants.CENTER);
	static JLabel trainNbrI = new JLabel("888", SwingConstants.CENTER);
	static JLabel typeI = new JLabel("標準車廂", SwingConstants.CENTER);
	static JLabel seatI = new JLabel("  3車20號、3車21號、3車22號、3車22號", SwingConstants.LEFT);
	
	static JLabel date2I = new JLabel(" ", SwingConstants.CENTER);
	static JLabel station2I = new JLabel(" ", SwingConstants.CENTER);
	static JLabel trainNbr2I = new JLabel("888", SwingConstants.CENTER);
	static JLabel type2I = new JLabel("標準車廂", SwingConstants.CENTER);
	static JLabel seat2I = new JLabel("  3車20號、3車21號、3車22號、3車22號", SwingConstants.LEFT);
	
	
	
	static JLabel empty = new JLabel(" ",SwingConstants.CENTER);

	
	static JComponent[] jc = {back,topic,
							bid,bidI,uid,uidI,
							price,priceI,
							info,
							date,dateI,trainNbr,trainNbrI,
							station,stationI,type,typeI,
							seat,seatI,
							
							info2,
							date2,dateI,trainNbr2,trainNbr2I,
							station2,station2I,type2,type2I,
							seat2,seat2I,
							
							modify,part,cancel};
	
	static JComponent[] jcbt = {
			bidI,
			uidI,
			priceI,
			trainNbrI,dateI,
			stationI,
			seatI,
			typeI,
			trainNbr2I,date2I,
			station2I,
			seat2I,
			type2I
			};
	
	static int layout [][] ={
			{ 0, 0, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1,0,0,5,1},{ 3, 0, 9, 1, 0, 0, Format.fill[0], Format.anchor[5],1,10 ,0,0,5,1},
			
			
			
			{ 0, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],30,10 ,0,0,5,0},{ 3,1 , 4, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			{ 7, 1, 2, 1,0 , 0, Format.fill[1], Format.anchor[0],15,1 ,0,0,5,0},{ 9, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			{ 0, 3, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},{ 3, 3, 9, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			
			{ 0, 5, 12, 1, 0, 0, Format.fill[0], Format.anchor[5],1,10 ,0,0,5,1},
			
			{ 0, 6, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},{ 3, 6, 4, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			{ 7, 6, 2, 1,0 , 0, Format.fill[1], Format.anchor[0],30,10 ,0,0,5,0},{ 9, 6, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			{ 0, 8, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},{ 3, 8, 4, 1,0 , 0, Format.fill[0], Format.anchor[0],50,1 ,0,0,5,0},
			{ 7, 8, 2, 1,0 , 0, Format.fill[1], Format.anchor[0],30,10 ,0,0,5,0},{ 9, 8, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			{ 0, 10, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},{ 3, 10, 9, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			{ 0, 12, 12, 1, 0, 0, Format.fill[0], Format.anchor[5],1,10 ,0,0,5,1},
			
			{ 0, 14, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},{ 3, 14, 4, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			{ 7, 14, 2, 1,0 , 0, Format.fill[1], Format.anchor[0],30,10 ,0,0,5,0},{ 9, 14, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			{ 0,16, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},{ 3, 16, 4, 1,0 , 0, Format.fill[0], Format.anchor[0],50,1 ,0,0,5,0},
			{ 7,16, 2, 1,0 , 0, Format.fill[1], Format.anchor[0],30,10 ,0,0,5,0},{ 9, 16, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			{ 0, 18, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,10 ,0,0,5,0},{ 3, 18, 9, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			
			{ 4, 20, 3, 1,0 , 0, Format.fill[3], Format.anchor[1],20,1 ,0,0,5,0},{ 7, 20, 2, 1,0 , 0, Format.fill[3], Format.anchor[1],20,1 ,0,0,5,0},
			{ 9, 20, 3, 1,0 , 0, Format.fill[3], Format.anchor[1],20,1 ,0,0,5,0},
			};
	
	static void result(String bid,String uid,String no,String date,String travel,String type, String seat, String price,
											 String no2,String date2,String travel2,String type2, String seat2) {
		bidI.setText(bid);
		uidI.setText(uid);
		trainNbrI.setText(no);
		dateI.setText(date);
		stationI.setText(travel);
		typeI.setText(type);
		seatI.setText(seat);
		priceI.setText(price);
		trainNbr2I.setText(no2);
		date2I.setText(date2);
		station2I.setText(travel2);
		type2I.setText(type2);
		seat2I.setText(seat2);
	}
	
	public void addListener() {//加入所有監聽器
		back.addMouseListener(new BackListener());
		modify.addMouseListener(new ModifyL());
		part.addMouseListener(new PartL());
		cancel.addMouseListener(new CancelL());
	
	}
	
	class BackListener extends HsrGUI.BackListener{//返回首頁
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			InfoSearch.panel.setVisible(true);
		}
	}
	
	class ModifyL extends AllListener{ //跳至更改行程頁面
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			ModifyTwo.panel.setVisible(true);
		}
	}
	
	class PartL extends AllListener{//跳至部分退票頁面
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			PartCancel.panel.setVisible(true);
		}
	}
	
	class CancelL extends AllListener{//跳至退票確認頁面
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			CancelCheck.panel.setVisible(true);
		}
	}
}
