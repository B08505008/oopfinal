package HsrGUI;
import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import HSR.*;


public class ConditionSearch {
	ConditionSearch(){
		panel.setLayout(new GridBagLayout());
		Format.setFormat(jc);
		Format.setWhiteBlack(exchange);
		Format.setStation(startI);
		Format.setStation(stopI);
		Format.setYear(yearI);
		Format.setMonth(monthI);
		Format.setDay(dayI,HSR_GUI.day31);
		Format.setHour(hourI);
		setDate();
		setDiscount(discountI);
		stopI.setSelectedIndex(1);
		Format.setBlackTrans(to);
		Format.setBlackTrans(year);
		Format.setBlackTrans(month);
		Format.setBlackTrans(day);
		Format.setRedTrans(hint);
		addListener();
		for (int i = 0; i < jc.length; i++) {
			Format.add(panel,jc,layout,i);
		}
	}
	
	static JPanel panel = new JPanel();
	static JButton back = new JButton("< 回上頁");
	static JLabel status = new JLabel("班次查詢", SwingConstants.CENTER);
	static JLabel station = new JLabel("起訖站", SwingConstants.CENTER);
	static JLabel date = new JLabel("乘車日期", SwingConstants.CENTER);
	static JLabel time = new JLabel("出發時間", SwingConstants.CENTER);
	static JLabel discount = new JLabel("限定優惠", SwingConstants.CENTER);
	static JComboBox startI = new JComboBox();
	static JLabel to        = new JLabel("往",SwingConstants.CENTER);
	static JComboBox stopI = new JComboBox();
	static JButton exchange = new JButton("互換");
	static JComboBox yearI = new JComboBox();
	static JLabel 	 year  = new JLabel(" 年 ",SwingConstants.CENTER);
	static JComboBox monthI= new JComboBox();
	static JLabel    month = new JLabel(" 月 ",SwingConstants.CENTER);
	static JComboBox dayI  = new JComboBox();
	static JLabel day      = new JLabel("   日",SwingConstants.LEFT);
	static JComboBox hourI  = new JComboBox();
	static JComboBox discountI  = new JComboBox();
	static JButton search = new JButton("開始查詢");
	static JLabel hint = new JLabel(" ", SwingConstants.CENTER);
	static JComponent[] jc ={back,status,
							  station,startI,to,stopI,exchange,
							  date,yearI,year,monthI,month,dayI,day,
							 time,hourI,discount,discountI,
							 hint,search};
	
	static int layout[][] = {
			{ 0, 0, 3, 1, 0, 0, Format.fill[3], Format.anchor[5] ,1,1,0,0,10,2}, { 3, 0, 12, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,10,0},
			
			{ 0, 2, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5] ,60,1,0,3,5,0},
			{ 6, 2, 1, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 7, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5] ,60,1,0,0,5,0},
			{ 10, 2, 1, 1, 0, 0, Format.fill[3], Format.anchor[5] ,1,1,0,5,5,5},
			
			{ 0, 3, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0}, 
			{ 3, 3, 3, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,3,5,0},{ 6, 3, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 7, 3, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 8, 3, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 9, 3, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 10, 3, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			
			{ 0, 4, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 4, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,3,5,0},
			{ 7, 4, 2, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 9, 4, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,3,5,0},

			{ 3, 8, 6, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,0,0},{ 9, 8, 2, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,0,0},
			};
	
	void addListener() { //加入所有監聽器
		back.addMouseListener(new BackListener());
		monthI.addActionListener(new MonthIListener());
		search.addMouseListener(new SearchListener());
		exchange.addMouseListener(new ExchangeListener());
	}
	
	static void setDiscount(JComboBox jcbbx) { //加入優惠選項
		jcbbx.removeAllItems();
		for (int i = 0;i<HSR_GUI.discountType.length;i++ ) {
			jcbbx.addItem(HSR_GUI.discountType[i]);
		}
	}

	static void reset()	{ //重整頁面
		startI.setSelectedIndex(0);
		stopI.setSelectedIndex(1);
		setDate();
		discountI.setSelectedIndex(0);
		hint.setText(" ");
	}
	static void setDate() { //日期設當日，時間設離現在最近且較早的半時
		Date date = new Date();
		dayI.setSelectedItem(date.getDate());
		monthI.setSelectedItem(date.getMonth()+1);
		yearI.setSelectedItem(date.getYear());
		String hour = Integer.toString(date.getHours())+" : "+((date.getMinutes()>29)?"30":"00");
		hourI.setSelectedItem(hour);
		
	}
	
	static boolean[] wantDiscount() {
		boolean[] b = {false,false};
		switch(discountI.getSelectedIndex()) {
		case 1:
			b[1] = true;
			break;
		case 2:
			b[0] =true;
			break;
		case 3:
			b[0] = true;
			b[1] =true;
			break;
		default:
		}
		return b;
	}
	
	class BackListener extends HsrGUI.BackListener{//返回首頁
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			HomePage.panel.setVisible(true);
		}
	}
	
	class MonthIListener implements ActionListener{//日期選項依選擇月分而改變
		@Override
		public void actionPerformed(ActionEvent e) {
			int i = dayI.getSelectedIndex();
			switch (monthI.getSelectedIndex()+1){
			case 1:case 3:case 5:case 7:case 8:case 10:case 12:
				Format.setDay(dayI,HSR_GUI.day31);
				break;
			case 4:case 6:case 9:case 11:
				Format.setDay(dayI,HSR_GUI.day30);
				break;
			case 2:
				Format.setDay(dayI,HSR_GUI.day28);
				break;
			}
			if(dayI.getItemCount()>i) {
				dayI.setSelectedIndex(i);
			}
			else {
				dayI.setSelectedIndex(dayI.getItemCount()-1);
			}
			
		}
	}
	
	class SearchListener extends AllListener{ //依條件搜尋
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setBlackOrange(search);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setOrangeBlack(search);
		}
		
		@Override
		public void mouseClicked(MouseEvent e) {
			if (startI.getSelectedIndex() == stopI.getSelectedIndex()) {
				hint.setText("起訖站不可為同一站");
			} else {
				String startID = Format.toStationID((String) startI.getSelectedItem());
				String arrivalID = Format.toStationID((String) stopI.getSelectedItem());
				String date = Format.toStdDate((int) yearI.getSelectedItem(), (int) monthI.getSelectedItem(),(int) dayI.getSelectedItem());
				System.out.println(startID + " " + arrivalID + " " + date);
				boolean[] discount = wantDiscount();
				TrainInfo[] result = DB
						.timesearch(new TimeTableSearch(startID, arrivalID, date, discount[0], discount[1]));
				if (result == null) {
					hint.setText("找不到符合班次");
				} else {
					for (TrainInfo b : result) {
						System.out.println(b);
					}
					ConditionResult.setTable(result);
					boolean b = ConditionResult.setTrain(new Timing((String) hourI.getSelectedItem()));
					if (b == true) {
						int year = (int) yearI.getSelectedItem();
						int month = (int) monthI.getSelectedItem();
						int day = (int) dayI.getSelectedItem();
						String station = Format.stationToString((String) startI.getSelectedItem(),
								(String) stopI.getSelectedItem());
						ConditionResult.setInfo(year, month, day, station);
						panel.setVisible(false);
						ConditionResult.panel.setVisible(true);
						hint.setText(" ");
					} else {
						hint.setText("找不到該時間以後之班次");
					}

				}
			}
		}
	}
	
	class ExchangeListener extends AllListener{ //起訖站互換
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setBlackWhite(exchange);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setWhiteBlack(exchange);
		}
		
		@Override
		public void mouseClicked(MouseEvent e) {
			int start = startI.getSelectedIndex();
			int stop = stopI.getSelectedIndex();
			startI.setSelectedIndex(stop);
			stopI.setSelectedIndex(start);
		}
	}
	
}
