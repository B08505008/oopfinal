package HsrGUI;

import java.awt.*;
import java.awt.event.*;
import java.util.Date;
import javax.swing.*;

import HSR.DB;

public class IdSearch {
	IdSearch(){
		panel.setLayout(new GridBagLayout());
		Format.setFormat(jc);
		trainNbrI.setFont(Format.f);
		uidex.setFont(Format.fs);
		Format.setStation(startI);
		Format.setStation(stopI);
		Format.setYear(yearI);
		Format.setMonth(monthI);
		Format.setDay(dayI,HSR_GUI.day31);
		setDate();
		stopI.setSelectedIndex(1);
		Format.setBlackTrans(uidex);
		Format.setBlackTrans(to);
		Format.setBlackTrans(year);
		Format.setBlackTrans(month);
		Format.setBlackTrans(day);
		Format.setRedTrans(hint);
		
		addListener();

		for (int i = 0; i < jc.length; i++) {
			Format.add(panel,jc,layout,i);
		}
	}

	static JPanel panel = new JPanel();
	static JButton back = new JButton("< 回上頁");
	static JLabel status = new JLabel("訂位代碼查詢", SwingConstants.CENTER);
	static JLabel uid = new JLabel("訂票人代碼", SwingConstants.CENTER);
	static JLabel station = new JLabel("起訖站", SwingConstants.CENTER);
	static JLabel date = new JLabel("乘車日期", SwingConstants.CENTER);
	static JLabel number = new JLabel("車次", SwingConstants.CENTER);
	static JTextField uidI = new JTextField();
	static JLabel uidex = new JLabel("身分證/護照號碼",SwingConstants.RIGHT);
	static JComboBox startI = new JComboBox();
	static JLabel to        = new JLabel("往",SwingConstants.CENTER);
	static JComboBox stopI = new JComboBox();
	static JComboBox yearI = new JComboBox();
	static JLabel 	 year  = new JLabel("年",SwingConstants.CENTER);
	static JComboBox monthI= new JComboBox();
	static JLabel    month = new JLabel("月",SwingConstants.CENTER);
	static JComboBox dayI  = new JComboBox();
	static JLabel day      = new JLabel("日",SwingConstants.CENTER);
	static JLabel hint     = new JLabel(" ",SwingConstants.CENTER);
	static JTextField trainNbrI = new JTextField();
	static JButton search = new JButton("開始查詢");
	static JComponent[] jc ={back,status,
							 uid, uidI,uidex,
							 station,startI,to,stopI,
							 date,yearI,year,monthI,month,dayI,day,
							 number,trainNbrI,
							 hint,search};
	
	static int layout[][] = {
			{ 0, 0, 3, 1, 0, 0, Format.fill[3], Format.anchor[5] ,1,1,0,0,10,2}, { 3, 0, 12, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,10,0},
			
			{ 0, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 1, 6, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,5,0,3,5,0},
			{ 9, 1, 2, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,5,0,15,5,0},
			
			{ 0, 3, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 3, 3, 1, 0, 0, Format.fill[0], Format.anchor[5] ,50,1,0,3,5,0},
			{ 6, 3, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 9, 3, 3, 1, 0, 0, Format.fill[0], Format.anchor[5] ,50,1,0,0,5,0},

			{ 0, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0}, 
			{ 3, 2, 3, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,3,5,0},{ 6, 2, 2, 1, 0, 0,Format.fill[0], Format.anchor[0] ,15,1,0,0,5,0},
			{ 8, 2, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 9, 2, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,10,5,0},
			{ 10, 2, 1, 1, 0, 0,Format.fill[3], Format.anchor[0] ,1,1,0,0,5,0},{ 11, 2, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			
			{0, 4, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 3, 4, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,5,0,3,5,0},
			
			{ 3, 5, 9, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 9, 4, 3, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0}
			};
	
	 void addListener() { //加入監聽器
		back.addMouseListener(new BackListener());
		monthI.addActionListener(new MonthIListener());
		search.addMouseListener(new SearchListener());
	}

	static void reset()	{ //重整頁面
		uidI.setText("");
		setDate();
		startI.setSelectedIndex(0);
		stopI.setSelectedIndex(1);
		trainNbrI.setText("");
	}
	
	static void setDate() { //設定日期為今日，時間為最近且較早之半整點
		Date date = new Date();
		dayI.setSelectedItem(date.getDate());
		monthI.setSelectedItem(date.getMonth()+1);
		yearI.setSelectedItem(date.getYear());
		String hour = Integer.toString(date.getHours())+" : "+((date.getMinutes()>29)?"30":"00");
	}
	
	class BackListener extends HsrGUI.BackListener{ //回上頁
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			HomePage.panel.setVisible(true);
		}
	}
	
	class MonthIListener implements ActionListener{ //日期選項根據選擇月份改變
		@Override
		public void actionPerformed(ActionEvent e) {
			int i = dayI.getSelectedIndex();
			switch (monthI.getSelectedIndex()+1){
			case 1:case 3:case 5:case 7:case 8:case 10:case 12:
				Format.setDay(dayI,HSR_GUI.day31);
				break;
			case 4:case 6:case 9:case 11:
				Format.setDay(dayI,HSR_GUI.day30);
				break;
			case 2:
				Format.setDay(dayI,HSR_GUI.day28);
				break;
			}
			if(dayI.getItemCount()>i) {
				dayI.setSelectedIndex(i);
			}
			else {
				dayI.setSelectedIndex(dayI.getItemCount()-1);
			}
			
		}
	}
	
	class SearchListener extends AllListener{ //開始查詢 
		
		@Override
		public void mouseClicked(MouseEvent e) {
			if(uidI.getText().equals("")) {
				hint.setText("請輸入訂票人代碼");
			}else if(startI.getSelectedIndex()==stopI.getSelectedIndex()) {
				hint.setText("起訖站不可為同一站");
			}else if (trainNbrI.getText().equals("")) {
				hint.setText("請輸入車次");
			}else {
				String uid = uidI.getText();
				String trainNbr  = trainNbrI.getText();	
				String start = (String)startI.getSelectedItem();
				String stop = (String)stopI.getSelectedItem();
				String startID = Format.toStationID(start);
				String stopID = Format.toStationID(stop);
				int year = (int)yearI.getSelectedItem();
				int month = (int)monthI.getSelectedItem();
				int day = (int)dayI.getSelectedItem();
				String dateStd = Format.toStdDate(year, month, day);
				int sus = DB.GetCode(uid, startID, stopID, dateStd, trainNbr);
				if(sus!=0) {
					IdSearchResult.bidI.setText(Integer.toString(sus));
					panel.setVisible(false);
					IdSearchResult.panel.setVisible(true);
					hint.setText(" ");
				}else {
					hint.setText("找不到相關代碼");
				}
				
			}
		}
		
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setBlackOrange(search);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setOrangeBlack(search);
		}
	}
	
}
