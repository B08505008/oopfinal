package HsrGUI;
import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import HSR.*;


public class TwoWayBook {
	TwoWayBook(){
		panel.setLayout(new GridBagLayout());
		Format.setFormat(jc);
		Format.setWhiteBlack(exchange);
		trainNbrI.setFont(Format.f);
		Format.setStation(startI);
		Format.setStation(stopI);
		Format.setYear(yearI);
		Format.setMonth(monthI);
		Format.setDay(dayI,HSR_GUI.day31);
		Format.setYear(year2I);
		Format.setMonth(month2I);
		Format.setDay(day2I,HSR_GUI.day31);
		Format.setHour(hourI);
		Format.setHour(hour2I);
		Format.setMeans(meansI);
		Format.setType(typeI);
		Format.setSeat(seatI);
		Format.setTicketTwoWay(normalI);
		Format.setTicketTwoWay(childI);
		Format.setTicketTwoWay(loveI);
		Format.setTicketTwoWay(oldI);
		Format.setTicketTwoWay(collegeI);
		setDate();
		stopI.setSelectedIndex(1);
		Format.setBlackTrans(to);
		Format.setBlackTrans(year);
		Format.setBlackTrans(month);
		Format.setBlackTrans(day);
		Format.setBlackTrans(year2);
		Format.setBlackTrans(month2);
		Format.setBlackTrans(day2);
		Format.setBlackTrans(normal);
		Format.setBlackTrans(child);
		Format.setBlackTrans(old);
		Format.setBlackTrans(love);
		Format.setBlackTrans(college);
		Format.setBlackTrans(time);
		Format.setBlackTrans(time2);
		Format.setBlackTrans(uidex);
		Format.setRedTrans(hint);
		uidex.setFont(Format.fs);
		trainNbrI.setVisible(false);
		trainNbr2I.setVisible(false);
		
		addListener();

		for (int i = 0; i < jc.length; i++) {
			Format.add(panel,jc,layout,i);
		}
		Format.add(panel, jc, layout, 0);
	}
	
	

	static JPanel panel = new JPanel();
	static JButton back = new JButton("< 回上頁");
	static JLabel status = new JLabel("來回訂票", SwingConstants.CENTER);
	static JLabel station = new JLabel("起訖站", SwingConstants.CENTER);
	static JLabel type = new JLabel("車廂種類", SwingConstants.CENTER);
	static JLabel seat = new JLabel("座位喜好", SwingConstants.CENTER);
	static JLabel date = new JLabel("去程日期", SwingConstants.CENTER);
	static JLabel date2 = new JLabel("回程日期", SwingConstants.CENTER);
	static JLabel means = new JLabel("訂位方式", SwingConstants.CENTER);
	static JLabel time = new JLabel("去程", SwingConstants.CENTER);
	static JLabel time2 = new JLabel("回程", SwingConstants.CENTER);
	static JLabel number = new JLabel("車次", SwingConstants.CENTER);
	static JLabel ticket = new JLabel("票數", SwingConstants.CENTER);
	
	static JLabel uid = new JLabel("訂票人代碼", SwingConstants.CENTER);
	static JTextField uidI = new JTextField();
	static JLabel uidex = new JLabel("本國籍：身分證字號 / 非本國籍：護照號碼",SwingConstants.CENTER);
	
	static JComboBox startI = new JComboBox();
	static JLabel to        = new JLabel("往",SwingConstants.CENTER);
	static JComboBox stopI = new JComboBox();
	static JButton exchange = new JButton("互換");
	
	static JComboBox typeI = new JComboBox();
	
	static JComboBox seatI= new JComboBox();
	
	static JComboBox yearI = new JComboBox();
	static JLabel 	 year  = new JLabel("年",SwingConstants.CENTER);
	static JComboBox monthI= new JComboBox();
	static JLabel    month = new JLabel("月",SwingConstants.CENTER);
	static JComboBox dayI  = new JComboBox();
	static JLabel day      = new JLabel("日",SwingConstants.CENTER);
	
	static JComboBox year2I = new JComboBox();
	static JLabel 	 year2  = new JLabel("年",SwingConstants.CENTER);
	static JComboBox month2I= new JComboBox();
	static JLabel    month2= new JLabel("月",SwingConstants.CENTER);
	static JComboBox day2I  = new JComboBox();
	static JLabel day2      = new JLabel("日",SwingConstants.CENTER);
	
	
	static JComboBox hourI  = new JComboBox();
	static JComboBox hour2I  = new JComboBox();
	
	
	static JComboBox meansI = new JComboBox();
	
	static JComboBox timeI = new JComboBox();
	static JTextField trainNbrI = new JTextField();
	static JTextField trainNbr2I = new JTextField();
	
	static JTextField numberI = new JTextField();
	
	static JLabel normal = new JLabel("全票",SwingConstants.CENTER);
	static JLabel child = new JLabel("孩童",SwingConstants.CENTER);
	static JLabel old = new JLabel("敬老",SwingConstants.CENTER);
	static JLabel love = new JLabel("愛心",SwingConstants.CENTER);
	static JLabel college = new JLabel("大學生",SwingConstants.CENTER);
	
	static JComboBox normalI = new JComboBox();
	static JComboBox childI = new JComboBox();
	static JComboBox oldI = new JComboBox();
	static JComboBox loveI = new JComboBox();
	static JComboBox collegeI = new JComboBox();
	
	static JCheckBox discount = new JCheckBox("限尚有優惠之車次");
	static JButton book = new JButton("開始訂票");
	static JLabel hint = new JLabel(" ");
	
	private JComponent[] jc ={back,status,
			 				  uid,uidI,uidex,
							  station,startI,to,stopI,exchange,
							  date,yearI,year,monthI,month,dayI,day,
							  date2,year2I,year2,month2I,month2,day2I,day2,
							  means,meansI,time,hourI,time2,hour2I,
							  trainNbrI, trainNbr2I,
							  type,typeI,
							  seat,seatI,
							  ticket,normal,normalI,child,childI,old,oldI,love,loveI,college,collegeI
							  ,discount,book,hint};
	
	private int layout[][] = {
			{ 0, 0, 3, 1, 0, 0, Format.fill[3], Format.anchor[5] ,1,1,0,0,10,2}, { 3, 0, 12, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,10,0},
			
			{ 0, 2, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,5,0,3,5,0},
			{ 6, 2, 7, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,5,0,0,5,0},
			
			{ 0, 4, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 4, 3, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,3,5,0},
			{ 6, 4, 1, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 7, 4, 3, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},
			{ 10, 4, 2, 1, 0, 0, Format.fill[3], Format.anchor[5] ,1,1,0,5,5,5},
			
			{ 0, 6, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0}, 
			{ 3, 6, 3, 1, 0, 0,Format.fill[0], Format.anchor[0] ,20,1,0,3,5,0},{ 6, 6, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,10,1,0,0,5,0},
			{ 7, 6, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 8, 6, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 9, 6, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 10, 6, 1, 1, 0, 0,Format.fill[1], Format.anchor[0] ,1,1,0,0,5,0},
			
			{ 0, 8, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0}, 
			{ 3, 8, 3, 1, 0, 0,Format.fill[0], Format.anchor[0] ,20,1,0,3,5,0},{ 6, 8, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,10,1,0,0,5,0},
			{ 7, 8, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 8, 8, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 9, 8, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 10, 8, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			
			{ 0, 10, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 10, 4, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,3,5,0},
			{ 7, 10, 1, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,10,5,5},{ 8, 10, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 10, 10, 1, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 11, 10, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			
			{ 8, 10, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 11, 10, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,71,1,0,0,5,0},
			
			{ 0, 12, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 12, 4, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,3,5,0},
			{ 7, 12, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,10,5,5},{ 10, 12, 3, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},
			
			{ 0, 14, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			{ 3, 14, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,15,1,0,3,5,0},{ 4, 14, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 5, 14, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,15,1,0,0,5,0},{ 6, 14, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 7, 14, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,15,1,0,0,5,0},{ 8, 14, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 9, 14, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,15,1,0,0,5,0},{ 10, 14, 1, 1, 0, 0,Format.fill[3], Format.anchor[5] ,1,1,0,0,5,0},
			{ 11, 14, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,15,1,0,0,5,0},{ 12, 14, 1, 1, 0, 0,Format.fill[3], Format.anchor[5] ,1,1,0,0,5,0},
			
			{ 0, 16, 4, 1, 0, 0,Format.fill[0], Format.anchor[7] ,1,1,0,0,0,0},{ 10, 16, 3, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,0,0},
			{ 4, 16, 6, 1, 0, 0,Format.fill[3], Format.anchor[0] ,1,1,0,0,0,0},
			};
	static JComboBox[] ticketI = {childI,oldI,loveI,collegeI,};
	static JComboBox[] other = {meansI,typeI,seatI,startI};
	
	private void addListener() {
		back.addMouseListener(new BackListener());
		meansI.addActionListener(new MeansIListener());
		monthI.addActionListener(new MonthIListener());
		book.addMouseListener(new BookListener());
		exchange.addMouseListener(new ExchangeListener());
	}
	
	static void reset()	{ //重整頁面
		setDate();
		uidI.setText("");
		stopI.setSelectedIndex(1);
		trainNbrI.setText("");
		trainNbr2I.setText("");
		for(JComboBox jcb : other) {
			jcb.setSelectedIndex(0);
		}
		for(JComboBox jcb : ticketI) {
			jcb.setSelectedIndex(0);
		}
		discount.setSelected(false);
		hint.setText(" ");
	}
	
	static void setDate() {  //預設日期為今天日期，時間為離現在最近且比現在早的半點
		Date date = new Date();
		dayI.setSelectedItem(date.getDate());
		monthI.setSelectedItem(date.getMonth()+1);
		yearI.setSelectedItem(date.getYear());
		String hour = Integer.toString(date.getHours())+" : "+((date.getMinutes()>29)?"30":"00");
		hourI.setSelectedItem(hour);
		day2I.setSelectedItem(date.getDate());
		month2I.setSelectedItem(date.getMonth()+1);
		year2I.setSelectedItem(date.getYear());
		hour2I.setSelectedItem(hour);
		
	}
	
	class BackListener extends HsrGUI.BackListener{//回上頁
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			HomePage.panel.setVisible(true);
		}
	}
	
	static void changeMeans(String s) { //依訂票方式更改 車次/時間方塊
		if(s.equals("依車次")) {
			hourI.setVisible(false);
			hour2I.setVisible(false);
			trainNbrI.setVisible(true);
			trainNbr2I.setVisible(true);
		}
		else {
			trainNbrI.setVisible(false);
			trainNbr2I.setVisible(false);
			hourI.setVisible(true);
			hour2I.setVisible(true);
		}
		
	}
	
	class MeansIListener implements ActionListener{//依訂票方式決定車次/時間方塊的顯示與否
		@Override
		public void actionPerformed(ActionEvent e) {
			changeMeans((String)meansI.getSelectedItem());
		}
	}
	
	class MonthIListener implements ActionListener{ //依月份決定日期選單的選項
		@Override
		public void actionPerformed(ActionEvent e) {
			int i = dayI.getSelectedIndex();
			switch (monthI.getSelectedIndex()+1){
			case 1:case 3:case 5:case 7:case 8:case 10:case 12:
				Format.setDay(dayI,HSR_GUI.day31);
				break;
			case 4:case 6:case 9:case 11:
				Format.setDay(dayI,HSR_GUI.day30);
				break;
			case 2:
				Format.setDay(dayI,HSR_GUI.day28);
				break;
			}
			if(dayI.getItemCount()>i) {
				dayI.setSelectedIndex(i);
			}
			else {
				dayI.setSelectedIndex(dayI.getItemCount()-1);
			}
			
		}
	}
	
	class BookListener extends AllListener{//若輸入錯誤、找無符合車次出現提示，其餘則跳轉頁面
		
		@Override
		public void mouseClicked(MouseEvent e) {
			String startSation = (String)startI.getSelectedItem();
			String endSation = (String)stopI.getSelectedItem();
			
			String date1 = Format.toStdDate((int)yearI.getSelectedItem(), (int)monthI.getSelectedItem(), (int)dayI.getSelectedItem());
			String date2 = Format.toStdDate((int)year2I.getSelectedItem(), (int)month2I.getSelectedItem(), (int)day2I.getSelectedItem());
			Timing time1 = new Timing((String)hourI.getSelectedItem());
			Timing time2 = new Timing((String)hour2I.getSelectedItem());
			
			if (uidI.getText().equals("")) {
				hint.setText("請輸入訂票人代碼");
			}else if (startI.getSelectedIndex() == stopI.getSelectedIndex()) {
				hint.setText("起訖站不可為同一站");
			}else if(meansI.getSelectedIndex() == 0 && TransForm.dateSubtraction(date1,date2)<0) {
				hint.setText("回程需比去程晚");
			}else if(meansI.getSelectedIndex() == 0 && TransForm.dateSubtraction(date1,date2)==0 && time1.larger(time2)){
				hint.setText("回程需比去程晚");	
			}else if (Format.invalidDate((int) monthI.getSelectedItem(), (int) dayI.getSelectedItem())) {
				hint.setText("請選擇29日內(含當日)的日期");
			}else if (meansI.getSelectedIndex() == 1 && (trainNbrI.getText().equals("")||trainNbr2I.getText().equals(""))) {
				hint.setText("請完整輸入去回車次");
			}else if(meansI.getSelectedIndex() == 1 && (!(trainNbrI.getText().equals("")) && (!trainNbr2I.getText().equals("")))) {
				System.out.println(trainNbrI.getText());
				System.out.println(trainNbr2I.getText());
				System.out.println(Format.toStationID(startSation));
				System.out.println(Format.toStationID(endSation));
				String timeNo1 = TrainInfoDB.getTime(trainNbrI.getText(), Format.toStationID(startSation), Format.toStationID(endSation));
				String timeNo2 = TrainInfoDB.getTime(trainNbr2I.getText(),Format.toStationID(endSation), Format.toStationID(startSation));
				System.out.println(timeNo1);
				System.out.println(timeNo2);
				if(timeNo1==null) {
					hint.setText("去程車次不存在");
				}else if (timeNo2==null) {
					hint.setText("回程車次不存在");
				}else {
					Timing time3 = new Timing(timeNo1.split(",")[1]);
					System.out.println(time3);
					Timing time4 = new Timing(timeNo2.split(",")[0]);
					System.out.println(time4);
					if(time3.larger(time4)) {
						hint.setText("回程需比去程晚");	
					}
				}
			}else if (normalI.getSelectedIndex() == 0 && childI.getSelectedIndex() == 0 && oldI.getSelectedIndex() == 0
					&& loveI.getSelectedIndex() == 0 && collegeI.getSelectedIndex() == 0) {
				hint.setText("請選擇訂票數量");
			}else if (normalI.getSelectedIndex() + childI.getSelectedIndex() + oldI.getSelectedIndex()
					+ loveI.getSelectedIndex() + collegeI.getSelectedIndex() > 5) {
				hint.setText("訂票數量不可大於10張(來回各5張)");
			}else if (collegeI.getSelectedIndex() != 0 && typeI.getSelectedIndex() == 1) {
				hint.setText("商務車廂不適用大學生優惠");
			}
		}
		
		
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setBlackOrange(book);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setOrangeBlack(book);
		}
	}
	
	class ExchangeListener extends AllListener{//交換起訖站
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setBlackWhite(exchange);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setWhiteBlack(exchange);
		}
		
		@Override
		public void mouseClicked(MouseEvent e) {
			int start = startI.getSelectedIndex();
			int stop = stopI.getSelectedIndex();
			startI.setSelectedIndex(stop);
			stopI.setSelectedIndex(start);
		}
	}

}
