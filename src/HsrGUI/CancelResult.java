package HsrGUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class CancelResult {
	
	CancelResult(){
		panel.setLayout(new GridBagLayout());
		Format.setFormat(jc);
		topic.setFont(Format.f16);
		
		for (int i = 0; i < jc.length; i++) {
			Format.add(panel,jc,layout,i);
		}
		Format.setBlackTrans(hint);
		Format.setWhiteBlack(back);
		Format.setWhiteBlack(conti);
		
		addListener();
	}
	
	static JPanel panel = new JPanel();
	static JLabel  status = new JLabel("一般訂票",SwingConstants.CENTER);
	static JLabel topic = new JLabel("退票成功！", SwingConstants.CENTER);
	static JButton back = new JButton("返回首頁");
	static JButton conti = new JButton("繼續退票");
	static JLabel hint = new JLabel(" ", SwingConstants.CENTER);
	static JComponent[] jc = {topic,
							  //hint,
							  conti,back};
	static int layout [][] ={
			{ 0, 0, 12, 1, 0, 0, Format.fill[0], Format.anchor[5],1,10 ,0,0,5,1},
			
			//{ 0, 1, 12, 1, 0, 0, Format.fill[0], Format.anchor[5],1,10 ,0,0,5,1},
			
			
			{ 0, 6, 6, 1,0 , 0, Format.fill[3], Format.anchor[1],20,1 ,0,0,5,0},{ 6, 6, 6, 1,0 , 0, Format.fill[3], Format.anchor[1],20,1 ,0,0,5,0},
			};

	public void addListener() {//加入所有監聽器
		back.addMouseListener(new BackListener());
		conti.addMouseListener(new ContiL());
	}
	
	class BackListener extends HsrGUI.BackListener{//返回首頁
		@Override
		public void mouseClicked(MouseEvent e) {
			PartCancel.reset();
			panel.setVisible(false);
			HomePage.panel.setVisible(true);
		}
	}
	
	class ContiL extends AllListener{ //跳至更改/取消訂位頁面
		@Override
		public void mouseClicked(MouseEvent e) {
			InfoSearch.status.setText("更改/取消訂位");
			InfoSearch.reset();
			panel.setVisible(false);
			InfoSearch.panel.setVisible(true);
		}
	}
}
