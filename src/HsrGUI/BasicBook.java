package HsrGUI;
import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import HSR.*;


public class BasicBook {
	BasicBook(){
		panel.setLayout(new GridBagLayout());
		Format.setFormat(jc);
		Format.setWhiteBlack(exchange);
		trainNbrI.setFont(Format.f);
		uidex.setFont(Format.fs);
		Format.setStation(startI);
		Format.setStation(stopI);
		Format.setYear(yearI);
		Format.setMonth(monthI);
		Format.setDay(dayI,HSR_GUI.day31);
		Format.setHour(hourI);
		Format.setMeans(meansI);
		Format.setType(typeI);
		Format.setSeat(seatI);
		Format.setTicket(normalI);
		Format.setTicket(childI);
		Format.setTicket(loveI);
		Format.setTicket(oldI);
		Format.setTicket(collegeI);
		setDate();
		stopI.setSelectedIndex(1);
		Format.setBlackTrans(uidex);
		Format.setBlackTrans(to);
		Format.setBlackTrans(year);
		Format.setBlackTrans(month);
		Format.setBlackTrans(day);
		Format.setBlackTrans(normal);
		Format.setBlackTrans(child);
		Format.setBlackTrans(old);
		Format.setBlackTrans(love);
		Format.setBlackTrans(college);
		Format.setRedTrans(hint);
		trainNbrI.setVisible(false);
		addListener();

		for (int i = 0; i < jc.length; i++) {
			Format.add(panel,jc,layout,i);
		}
	}
	
	

	static JPanel panel = new JPanel();
	static JButton back = new JButton("< 回上頁");
	static JLabel status = new JLabel("一般訂票", SwingConstants.CENTER);
	static JLabel uid = new JLabel("訂票人代碼", SwingConstants.CENTER);
	static JLabel station = new JLabel("起訖站", SwingConstants.CENTER);
	static JLabel type = new JLabel("車廂種類", SwingConstants.CENTER);
	static JLabel seat = new JLabel("座位喜好", SwingConstants.CENTER);
	static JLabel date = new JLabel("乘車日期", SwingConstants.CENTER);
	static JLabel means = new JLabel("訂位方式", SwingConstants.CENTER);
	static JLabel time = new JLabel("出發時間", SwingConstants.CENTER);
	static JLabel number = new JLabel("車次", SwingConstants.CENTER);
	static JLabel ticket = new JLabel("票數", SwingConstants.CENTER);
	
	static JTextField uidI = new JTextField();
	static JLabel uidex = new JLabel(" 本國籍：身分證字號 / 非本國籍：護照號碼",SwingConstants.LEFT);
	
	static JComboBox startI = new JComboBox();
	static JLabel to        = new JLabel("往",SwingConstants.CENTER);
	static JComboBox stopI = new JComboBox();
	static JButton exchange = new JButton("互換");
	
	static JComboBox typeI = new JComboBox();
	
	static JComboBox seatI= new JComboBox();
	
	static JComboBox yearI = new JComboBox();
	static JLabel 	 year  = new JLabel("年",SwingConstants.CENTER);
	static JComboBox monthI= new JComboBox();
	static JLabel    month = new JLabel("月",SwingConstants.CENTER);
	static JComboBox dayI  = new JComboBox();
	static JLabel day      = new JLabel("日",SwingConstants.CENTER);
	
	static JComboBox hourI  = new JComboBox();
	
	static JComboBox meansI = new JComboBox();
	
	static JTextField trainNbrI = new JTextField();
	
	static JTextField numberI = new JTextField();
	
	static JLabel normal = new JLabel("全票",SwingConstants.CENTER);
	static JLabel child = new JLabel("孩童",SwingConstants.CENTER);
	static JLabel old = new JLabel("敬老",SwingConstants.CENTER);
	static JLabel love = new JLabel("愛心",SwingConstants.CENTER);
	static JLabel college = new JLabel("大學生",SwingConstants.CENTER);
	
	static JComboBox normalI = new JComboBox();
	static JComboBox childI = new JComboBox();
	static JComboBox oldI = new JComboBox();
	static JComboBox loveI = new JComboBox();
	static JComboBox collegeI = new JComboBox();
	
	static JCheckBox discount = new JCheckBox("限尚有優惠之車次");
	
	static JButton book = new JButton("開始訂票");
	static JLabel hint = new JLabel(" ");
	
	static JComponent[] jc ={back,status,
							  uid, uidI,uidex,
							  station,startI,to,stopI,exchange,
							  date,yearI,year,monthI,month,dayI,day,
							  means,meansI,time,hourI,
							  trainNbrI,
							  type,typeI,
							  seat,seatI,
							  ticket,normal,normalI,child,childI,old,oldI,love,loveI,college,collegeI,
							  discount,book,hint};
	static JComboBox[] ticketI = {normalI,childI,oldI,loveI,collegeI};
	static JComboBox[] other = {meansI,typeI,seatI,startI};
	
	static int layout[][] = {
			{ 0, 0, 3, 1, 0, 0, Format.fill[3], Format.anchor[5] ,1,1,0,0,10,2}, { 3, 0, 10, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,10,0},
			
			{ 0, 1, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 1, 3, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,5,0,3,5,0},
			{ 7, 1, 6, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,5,0,0,5,0},
			
			{ 0, 2, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,3,5,0},
			{ 6, 2, 2, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 8, 2, 3, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},
			{ 11, 2, 2, 1, 0, 0, Format.fill[3], Format.anchor[0] ,1,1,0,5,5,5},
			
			{ 0, 3, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0}, 
			{ 3, 3, 3, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,3,5,0},{ 6, 3, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 7, 3, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 8, 3, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 9, 3, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},{ 10, 3, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			
			{ 0, 4, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 4, 4, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,3,5,0},
			{ 7, 4, 3, 1, 0, 0, Format.fill[1], Format.anchor[0] ,40,1,0,10,5,5},{ 10, 4, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 10, 4, 3, 1, 0, 0, Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			
			{ 0, 5, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},{ 3, 5, 4, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,3,5,0},
			{ 7, 5, 3, 1,0 , 0, Format.fill[1], Format.anchor[0],40,1 ,0,10,5,5},{ 10, 5, 3, 1, 0, 0, Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},
			
			{ 0, 7, 3, 1,0 , 0, Format.fill[0], Format.anchor[0],1,1 ,0,0,5,0},
			{ 3, 7, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,15,1,0,0,5,0},{ 4, 7, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 5, 7, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,15,1,0,0,5,0},{ 6, 7, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 7, 7, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,15,1,0,0,5,0},{ 8, 7, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,5,0},
			{ 9, 7, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,15,1,0,0,5,0},{ 10, 7, 1, 1, 0, 0,Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},
			{ 11, 7, 1, 1, 0, 0,Format.fill[0], Format.anchor[0] ,15,1,0,0,5,0},{ 12, 7, 1, 1, 0, 0,Format.fill[0], Format.anchor[5] ,1,1,0,0,5,0},
			
			{ 0, 8, 4, 1, 0, 0,Format.fill[0], Format.anchor[7] ,1,1,0,0,0,0},{ 10, 8, 3, 1, 0, 0,Format.fill[0], Format.anchor[0] ,1,1,0,0,0,0},
			{ 4, 8, 6, 1, 0, 0,Format.fill[3], Format.anchor[0] ,1,1,0,0,0,0},
			};

	
	 void addListener() { //加入所有監聽器
		back.addMouseListener(new BackListener());
		meansI.addActionListener(new MeansIListener());
		monthI.addActionListener(new MonthIListener());
		book.addMouseListener(new BookListener());
		exchange.addMouseListener(new ExchangeListener());
	}
	
	static void reset()	{ //重整頁面
		setDate();
		uidI.setText("");
		stopI.setSelectedIndex(1);
		trainNbrI.setText("");
		for(JComboBox jcb : other) {
			jcb.setSelectedIndex(0);
		}
		for(JComboBox jcb : ticketI) {
			jcb.setSelectedIndex(0);
		}
		discount.setSelected(false);
		hint.setText(" ");
	}
	
	static void setDate() { //預設日期為今天日期，時間為離現在最近且比現在早的半點
		Date date = new Date();
		dayI.setSelectedItem(date.getDate());
		monthI.setSelectedItem(date.getMonth()+1);
		yearI.setSelectedItem(date.getYear());
		String hour = Integer.toString(date.getHours())+" : "+((date.getMinutes()>29)?"30":"00");
		hourI.setSelectedItem(hour);
		
	}
	
	class BackListener extends HsrGUI.BackListener{ //回上頁
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			HomePage.panel.setVisible(true);
		}
	}
	
	static void changeMeans(String s) { //依訂票方式更改 車次/時間方塊
		if(s.equals("依車次")) {
			time.setText("搭乘車次");
			hourI.setVisible(false);
			trainNbrI.setVisible(true);
		}
		else {
			time.setText("出發時間");
			trainNbrI.setVisible(false);
			hourI.setVisible(true);
		}
		
	}
	
	static boolean[] wantDiscount() {
		boolean[] b = {false,false};
		if(discount.isSelected()) {
			b[0] = true;
			b[1] = true;
		}
		return b;
	}
	
	/*static String dateToString() {  //將選擇日期換成正式格式
		return Format.dateToString((int)yearI.getSelectedItem(), (int)monthI.getSelectedItem(), (int)dayI.getSelectedItem());
	}
	
	/*static String travelToString() { //產出起訖站及離站時間
		return Format.travelToString("18:00", (String)startI.getSelectedItem(), "19:00",(String)stopI.getSelectedItem() );
	}*/
	
	class MeansIListener implements ActionListener{ //依訂票方式決定車次/時間方塊的顯示與否
		@Override
		public void actionPerformed(ActionEvent e) {
			changeMeans((String)meansI.getSelectedItem());
		}
	}
	
	class MonthIListener implements ActionListener{ //依月份決定日期選單的選項
		@Override
		public void actionPerformed(ActionEvent e) {
			int i = dayI.getSelectedIndex();
			switch (monthI.getSelectedIndex()+1){
			case 1:case 3:case 5:case 7:case 8:case 10:case 12:
				Format.setDay(dayI,HSR_GUI.day31);
				break;
			case 4:case 6:case 9:case 11:
				Format.setDay(dayI,HSR_GUI.day30);
				break;
			case 2:
				Format.setDay(dayI,HSR_GUI.day28);
				break;
			}
			if(dayI.getItemCount()>i) {
				dayI.setSelectedIndex(i);
			}
			else {
				dayI.setSelectedIndex(dayI.getItemCount()-1);
			}
			
		}
	}
	
	class BookListener extends AllListener{ //若輸入錯誤、找無符合車次出現提示，其餘則跳轉頁面
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setBlackOrange(book);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setOrangeBlack(book);
		}
		
		@Override
		public void mouseClicked(MouseEvent e) {
			if(uidI.getText().equals("")) {
				hint.setText("請輸入訂票人代碼");
			}else if(startI.getSelectedIndex()==stopI.getSelectedIndex()) {
				hint.setText("起訖站不可為同一站");
			}else if(Format.invalidDate((int)monthI.getSelectedItem(),(int)dayI.getSelectedItem())){
				hint.setText("請選擇29日內(含當日)的日期");
			}else if (meansI.getSelectedIndex()==1&&trainNbrI.getText().equals("")) {
				hint.setText("請輸入車次");
			}else if (normalI.getSelectedIndex()==0&&childI.getSelectedIndex()==0&&oldI.getSelectedIndex()==0&&loveI.getSelectedIndex()==0&&collegeI.getSelectedIndex()==0) {
				hint.setText("請選擇訂票數量");
			}else if(normalI.getSelectedIndex()+childI.getSelectedIndex()+oldI.getSelectedIndex()+loveI.getSelectedIndex()+collegeI.getSelectedIndex()>10) {
				hint.setText("訂票數量不可大於10張");
			}else if (collegeI.getSelectedIndex() != 0 && typeI.getSelectedIndex() == 1) {
				hint.setText("商務車廂不適用大學生優惠");
			}else {
				hint.setText(" ");
				if(meansI.getSelectedIndex()==1) {
					String uid = uidI.getText();
					String trainNbr  = trainNbrI.getText();	
					String start = (String)startI.getSelectedItem();
					String stop = (String)stopI.getSelectedItem();
					String startID = Format.toStationID(start);
					String stopID = Format.toStationID(stop);
					int year = (int)yearI.getSelectedItem();
					int month = (int)monthI.getSelectedItem();
					int day = (int)dayI.getSelectedItem();
					String dateStd = Format.toStdDate(year, month, day);
					System.out.println(year);
					System.out.println(month);
					System.out.println(day);
					System.out.println(dateStd);
					boolean std = (typeI.getSelectedIndex()==0)?true:false;
					int per = Format.toSeatPre(seatI);
					int[] amounts = Format.toAmounts(ticketI);
					Order  o = new Order(uid, trainNbr, startID, stopID, dateStd, std, per, amounts);
					
					try {
						Ticket[] t = DB.buyticket(o);
						for(Ticket b:t) {
							System.out.println(b.toString());
						}
						String date = Format.dateToString(yearI,monthI,dayI);
						String travel = Format.travelToString(t[0].startTime,start,t[0].endTime,stop);
						String no = trainNbrI.getText();
						String type = (String)typeI.getSelectedItem();
						String seat = Format.seatInfo(t);
						String price = Format.priceInfo(t);
						BasicResult.result(o.code,uid,no,date,travel,type,seat, price);
						panel.setVisible(false);
						BasicResult.panel.setVisible(true);
					}catch(BookException be) {
						BasicFail.hint.setText(be.getErrMsg());
						panel.setVisible(false);
						BasicFail.panel.setVisible(true);
					}
					
				}else {
					String startID = Format.toStationID((String) startI.getSelectedItem());
					String arrivalID = Format.toStationID((String) stopI.getSelectedItem());
					String date = Format.toStdDate((int) yearI.getSelectedItem(), (int) monthI.getSelectedItem(),(int) dayI.getSelectedItem());
					System.out.println(startID + " " + arrivalID + " " + date);
					boolean[] discount = wantDiscount();
					TrainInfo[] result = DB.TimingSearch(new TimeTableSearch(startID, arrivalID, date, discount[0], discount[1]));
					if (result == null) {
						hint.setText("找不到符合班次");
					} else {
						for (TrainInfo b : result) {
							System.out.println(b);
						}
						BasicTime.setTable(result);
						boolean b = BasicTime.setTrain(new Timing((String) hourI.getSelectedItem()));
						if (b == true) {
							int year = (int) yearI.getSelectedItem();
							int month = (int) monthI.getSelectedItem();
							int day = (int) dayI.getSelectedItem();
							String station = Format.stationToString((String) startI.getSelectedItem(),
									(String) stopI.getSelectedItem());
							BasicTime.setInfo();
							panel.setVisible(false);
							BasicTime.panel.setVisible(true);
							hint.setText(" ");
						} else {
							hint.setText("找不到該時間以後之班次");
						}
					}
				}
				
			}
		}
	}
	
	class ExchangeListener extends AllListener{ //交換起訖站
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setBlackWhite(exchange);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setWhiteBlack(exchange);
		}
		
		@Override
		public void mouseClicked(MouseEvent e) {
			int start = startI.getSelectedIndex();
			int stop = stopI.getSelectedIndex();
			startI.setSelectedIndex(stop);
			stopI.setSelectedIndex(start);
		}
	}
	
}
