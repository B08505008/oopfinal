package HsrGUI;

import java.awt.*;
import HSR.*;
import java.awt.event.*;
import javax.swing.*;
public class PartCancel {
	
	PartCancel(){
		panel.setLayout(new GridBagLayout());
		Format.setFormat(jc);
		for (int i = 0; i < jc.length; i++) {
			Format.add(panel,jc,layout,i);
		}
		addListener();
	}

	static JPanel panel = new JPanel();
	static JButton back = new JButton("< 回上頁");
	static JLabel  status = new JLabel("部分退票",SwingConstants.CENTER);
	static JLabel infoI = new JLabel(" ", SwingConstants.CENTER);
	static JLabel  seat = new JLabel("座位",SwingConstants.CENTER);
	static JLabel type = new JLabel("票種",SwingConstants.CENTER);
	static JLabel travel =new JLabel("行程",SwingConstants.CENTER);
	static JLabel  choose =new JLabel("是否退票",SwingConstants.CENTER);
	static JButton cancel = new JButton("退訂所選車票");
	
	static JCheckBox c[];
	static Ticket tic[];
	
	static JComponent[] jc = {back,status,
							travel,seat,type,choose,
							};
	
	static int layout [][] ={
			{ 0, 0, 3, 1, 0, 0, Format.fill[2], Format.anchor[5],1,1 ,0,0,5,1}, { 3, 0, 15, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,5,1},
			
			{ 0, 7, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],70,1 ,0,0,5,1}, 
			{ 3, 7, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],70,1 ,0,0,5,1}, 
			{ 6, 7, 3, 1, 0, 0, Format.fill[0],Format. anchor[5],70,1 ,0,0,5,1}, 
			{ 9, 7, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],70,1 ,0,0,5,1}, 
			
			};
	static int[] cancelLayout = { 9, 8, 3, 1, 0, 0, Format.fill[0], Format.anchor[5],1,1 ,0,0,5,1};
	
	static void setResultLayout(String s,int j,int i ) {//將單一標籤設定資訊，加入頁面
		JLabel jl = new JLabel(s,SwingConstants.CENTER);
		jl.setFont(Format.f);
		Format.setBlackTrans(jl);
		GridBagConstraints c1 = new GridBagConstraints();
		c1.gridx = 3*i;
		c1.gridy = j+8;
		c1.gridwidth = 3;
		c1.gridheight = 1;
		c1.weightx = 0;
		c1.weighty = 0;
		c1.fill = Format.fill[0];
		c1.anchor =Format.anchor[5];
		c1.ipadx = 1;
		c1.ipady = 1;
		c1.insets = new Insets(0,0,2,1);
		panel.add(jl, c1);
	}
	
	static void setC(int j,int i ) {//將勾選格加入頁面
		JCheckBox ch = new JCheckBox();
		Format.setBlackTrans(ch);
		GridBagConstraints c1 = new GridBagConstraints();
		c1.gridx = 3*i;
		c1.gridy = j+8;
		c1.gridwidth = 3;
		c1.gridheight = 1;
		c1.weightx = 0;
		c1.weighty = 0;
		c1.fill = Format.fill[3];
		c1.anchor =Format.anchor[0];
		c1.ipadx = 1;
		c1.ipady = 1;
		c1.insets = new Insets(0,0,2,1);
		panel.add(ch, c1);
		c[j] = ch;
	}
	
	static void result(String travel,Ticket[] t) {//將所有資訊加入頁面
		System.out.println("PartCancel.result : travel = ");
		tic =t ;
		c = new JCheckBox[t.length];
		for(int j = 0 ; j < t.length ; j++) {
			String seat = Format.seatInfo(t[j]);
			String ticType = t[j].getTicketType();
			String[] s = {travel,seat,ticType};
			for(int i = 0 ; i <s.length  ; i++) {
				setResultLayout(s[i],j,i );
			}
			setC(j,3);
		}
		cancelLayout[1] = t.length +8;
		Format.setOrangeBlack(cancel);
		cancel.setFont(Format.f);
		Format.add(panel, cancel, cancelLayout);
	}
	
	static void reset() {
		int size = panel.getComponentCount();
		for(int i = size-1; i>5;i-- ) {
			panel.remove(i);
		}
	}
	
	public void addListener() {//加入所有監聽器
		back.addMouseListener(new BackListener());
		cancel.addMouseListener(new CancelL());	
	}
	
	class BackListener extends HsrGUI.BackListener{//回上頁並重整頁面
		@Override
		public void mouseClicked(MouseEvent e) {
			panel.setVisible(false);
			InfoSearchResult.panel.setVisible(true);
			reset();
		}
	}
	
	class CancelL extends AllListener{ //跳至退票結果頁面
		@Override
		public void mouseEntered(MouseEvent e) {
			Format.setBlackOrange(cancel);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Format.setOrangeBlack(cancel);
		}
		@Override
		public void mouseClicked(MouseEvent e) {
			for(int i = 0; i<tic.length;i++) {
				if(c[i].isSelected()) {
					DB.cancel(tic[i]);
				}
			}
			panel.setVisible(false);
			CancelResult.panel.setVisible(true);
			reset();
		}
	}
}
