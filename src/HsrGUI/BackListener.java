package HsrGUI;

import java.awt.event.MouseEvent;

public class BackListener extends AllListener{
	
	@Override
	public void mouseEntered(MouseEvent e) {
		Format.setBlackOrange(AllTrainN.back);
		Format.setBlackOrange(AllTrainS.back);
		Format.setBlackOrange(BasicBook.back);
		Format.setBlackOrange(BasicTime.back);
		Format.setBlackOrange(TwoWayBook.back);
		Format.setBlackOrange(IdSearch.back);
		Format.setBlackOrange(IdSearchResult.back);
		Format.setBlackOrange(InfoSearch.back);
		Format.setBlackOrange(InfoSearchResult.back);
		Format.setBlackOrange(ConditionSearch.back);
		Format.setBlackOrange(ConditionResult.back);
		Format.setBlackOrange(Modify.back);
		Format.setBlackOrange(PartCancel.back);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		Format.setOrangeBlack(AllTrainN.back);
		Format.setOrangeBlack(AllTrainS.back);
		Format.setOrangeBlack(BasicBook.back);
		Format.setOrangeBlack(BasicTime.back);
		Format.setOrangeBlack(TwoWayBook.back);
		Format.setOrangeBlack(IdSearch.back);
		Format.setOrangeBlack(IdSearchResult.back);
		Format.setOrangeBlack(InfoSearch.back);
		Format.setOrangeBlack(InfoSearchResult.back);
		Format.setOrangeBlack(ConditionSearch.back);
		Format.setOrangeBlack(ConditionResult.back);
		Format.setOrangeBlack(Modify.back);
		Format.setOrangeBlack(PartCancel.back);
	}
}
