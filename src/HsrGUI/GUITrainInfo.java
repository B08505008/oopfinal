package HsrGUI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import HSR.*;

public class GUITrainInfo {
	
	public GUITrainInfo(String trainNo, int direction, String startingStation, String endingStation,
			ArrayList<String> stopStations, ArrayList<Timing> depatureTimes, int[] service ,double universityDiscount,double earlyDiscount) {
		super();
		this.trainNo = trainNo;
		this.direction = direction;
		this.startingStation = startingStation;
		this.endingStation = endingStation;
		this.stopStations = stopStations;
		this.depatureTimes = depatureTimes;
		this.service = service;
		this.universityDiscount = universityDiscount;
		this.earlyDiscount = earlyDiscount;
		
		for(int i = 0 ; i< stopStations.size() ;i++) {
			staToTime.put(Format.transStationID(stopStations.get(i)),depatureTimes.get(i) );
		}
		
		tc = staToTime.get("�x��");
		
	}

	@Override
	public String toString() {
		return "TrainInfo [trainNo=" + trainNo + ", direction=" + direction + ", startingStation=" + startingStation
				+ ", endingStation=" + endingStation + ", stopStations=" + stopStations + ", depatureTimes="
				+ depatureTimes + ", service=" + Arrays.toString(service) + ", universityDiscount="
				+ universityDiscount + ", earlyDiscount=" + earlyDiscount+ "]";
	}

	 String trainNo;
	 int direction; // 0:southbound 1:northbound
	 String startingStation; // StationID
	 String endingStation; // StationID

	// stopStations&depatureTimes use the stopping sequence as their index
	 ArrayList<String> stopStations = new ArrayList<String>();
	 ArrayList<Timing> depatureTimes = new ArrayList<Timing>();
	 Map<String,Timing> staToTime = new HashMap<>();
	 Timing tc;

	// From "Monday" to "Sunday"!!!!!!!
	 int[] service = new int[7];
	 public double universityDiscount;
	public double earlyDiscount;

}
