package lunch;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
public class DB {
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost:3306/";
	//  Database credentials
	static final String USER = "root";
	static final String PASS = "12345678";
	static Connection conn = null;
	static Statement stmt = null;
	public static void init() {//初始化資料庫
		createDB.create();
		createTB.create();
	}
	public static boolean exist(String a) {//檢查a這個店名存不存在
		try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.cj.jdbc.Driver");

		      //STEP 3: Open a connection
		      //System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL+"LUNCH", USER, PASS);

		      //STEP 4: Execute a query
		      //System.out.println("Deleting database...");
		      stmt = conn.createStatement();
		      String sql="SELECT id,name,address,place,rate,cost,typ,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday FROM DATA WHERE name='"+a+"'";
		      ResultSet rs = stmt.executeQuery(sql);
		      while(rs.next()){
		    	  return true;
			  }
		      return false;
		   }catch(SQLException se){
			   se.printStackTrace();
			   return false;
		   }catch(Exception e){	
			   e.printStackTrace();
			   return false;
		   }		
	}
	public static int getid(String name) {//回傳指定店名的id，錯誤則回傳0
		ArrayList<Lunch> a=new ArrayList<Lunch>();
		try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.cj.jdbc.Driver");

		      //STEP 3: Open a connection
		      //System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL+"LUNCH", USER, PASS);

		      //STEP 4: Execute a query
		      //System.out.println("Deleting database...");
		      stmt = conn.createStatement();
		      ResultSet rs = stmt.executeQuery("SELECT * FROM DATA WHERE name='"+name+"'");
		      //STEP 5: Extract data from result set
		      rs.next();
		     	 int id = rs.getInt("id"); //店家評價
		      rs.close();
		      return id;
		   }catch(SQLException se){

		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }
		Lunch[] rt=new Lunch[a.size()];
		for(int i=0;i<rt.length;i++) {
			rt[i]=a.get(i);
		}
		return 0;
	}
	public static Lunch randompick() {//隨機回傳一筆資料
		ArrayList<Lunch> a=new ArrayList<Lunch>();
		try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.cj.jdbc.Driver");

		      //STEP 3: Open a connection
		      //System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL+"LUNCH", USER, PASS);

		      //STEP 4: Execute a query
		      //System.out.println("Deleting database...");
		      stmt = conn.createStatement();
		      ResultSet rs = stmt.executeQuery("SELECT * FROM DATA");
		      //STEP 5: Extract data from result set
		      while(rs.next()){
		         //Retrieve by column name
		         String name = rs.getString("name"); //店家名稱
		     	 String address = rs.getString("address"); //店家地點
		     	 String place = rs.getString("place"); //店家距離
		     	 int rate = rs.getInt("rate"); //店家評價
		         int cost = rs.getInt("cost"); //價格
		         String typ = rs.getString("typ"); //食物種類
		         char[] open = new char[7];
		         open[0]=rs.getString("Monday").charAt(0);
		         open[1]=rs.getString("Tuesday").charAt(0);
		         open[2]=rs.getString("Wednesday").charAt(0);
		         open[3]=rs.getString("Thursday").charAt(0);
		         open[4]=rs.getString("Friday").charAt(0);
		         open[5]=rs.getString("Saturday").charAt(0);
		         open[6]=rs.getString("Sunday").charAt(0);
		         a.add(new Lunch(name,address,place,rate,cost,typ,open));
		      }
		      rs.close();
		   }catch(SQLException se){

		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }
		Random r1 = new Random();
		return a.get(r1.nextInt(a.size()));
	}
	public static Lunch[] operator(String sql) {//用來call DB,回傳lunch陣列的function
		ArrayList<Lunch> a=new ArrayList<Lunch>();
		try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.cj.jdbc.Driver");

		      //STEP 3: Open a connection
		      //System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL+"LUNCH", USER, PASS);

		      //STEP 4: Execute a query
		      //System.out.println("Deleting database...");
		      stmt = conn.createStatement();
		      ResultSet rs = stmt.executeQuery(sql);
		      //STEP 5: Extract data from result set
		      while(rs.next()){
		         //Retrieve by column name
		         String name = rs.getString("name"); //店家名稱
		     	 String address = rs.getString("address"); //店家地點
		     	 String place = rs.getString("place"); //店家距離
		     	 int rate = rs.getInt("rate"); //店家評價
		         int cost = rs.getInt("cost"); //價格
		         String typ = rs.getString("typ"); //食物種類
		         char[] open = new char[7];
		         open[0]=rs.getString("Monday").charAt(0);
		         open[1]=rs.getString("Tuesday").charAt(0);
		         open[2]=rs.getString("Wednesday").charAt(0);
		         open[3]=rs.getString("Thursday").charAt(0);
		         open[4]=rs.getString("Friday").charAt(0);
		         open[5]=rs.getString("Saturday").charAt(0);
		         open[6]=rs.getString("Sunday").charAt(0);
		         a.add(new Lunch(name,address,place,rate,cost,typ,open));
		      }
		      rs.close();
		   }catch(SQLException se){

		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }
		Lunch[] rt=new Lunch[a.size()];
		for(int i=0;i<rt.length;i++) {
			rt[i]=a.get(i);
		}
		return rt;
	}
	public static boolean operating(String sql) {//用來call DB,回傳boolean的function
		try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.cj.jdbc.Driver");

		      //STEP 3: Open a connection
		      //System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL+"LUNCH", USER, PASS);

		      //STEP 4: Execute a query
		      //System.out.println("Deleting database...");
		      stmt = conn.createStatement();
		      stmt.executeUpdate(sql);
		      //System.out.println("Database deleted successfully...");
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
			   return false;
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
			   return false;
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }
		return true;
	}
	public static boolean remove(String name) {//移除指定店名資料，成功回傳true
		if(!exist(name)) {
			return false;
		}
		return operating("DELETE FROM DATA "+"WHERE name = '"+name+"'");
	}
	public static boolean update(int id,Lunch l) {//輸入要改的資料id(用getid(name)先查詢)與修改後的資料
		if(!operating("DELETE FROM DATA "+"WHERE id = "+id)) {
			return false;
		}
		return insert(l);
	}
	public static boolean insert(Lunch L) {//新增一筆資料，若店名重複則回傳false
		if(exist(L.name)) {
			return false;
		}
		return operating("INSERT INTO DATA (name,address,place,rate,cost,"+
		"typ,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday)"+
		L.toString());
	}
	public static Lunch[] search() {//回傳資料庫所有資料，回傳型別為lunch array
		return operator("SELECT * FROM DATA");
	}
	//傳入值為地點(複選)，種類(複選)，評價，價錢，是否要當天有開的，不限制則分別輸入(null,null,0,0,false)，回傳所有可能值
	public static Lunch[] filter(String[] place,String[] type,int rate,int cost,boolean open) {
		boolean a=false;
		String sql="SELECT * FROM DATA WHERE ";
		if(place!=null) {
			sql+="(place='"+place[0]+"'";
			if(place.length>1) {
				for(int i=1;i<place.length;i++) {
					sql+=" OR place ='"+place[i]+"'";
				}
			}
			sql+=")";
			a=true;
		}
		if(type!=null) {
			if(a) {
				sql+=" AND ";
			}
			sql+="(typ='"+type[0]+"'";
			if(type.length>1) {
				for(int i=1;i<type.length;i++) {
					sql+=" OR typ='"+type[i]+"'";
				}
			}
			sql+=")";
			a=true;
		}
		if(rate>0) {
			if(a) {
				sql+=" AND ";
			}
			sql+="rate >= "+rate;
			a=true;
		}
		if(cost>0) {
			if(a) {
				sql+=" AND ";
			}
			sql+="cost <= "+cost;
			a=true;
		}
		Date date = new Date();
		if(open==true) {
			if(a) {
				sql+=" AND ";
			}
			switch(date.getDay()) {
				case 0:
					sql+="Sunday ";
					break;
				case 1:
					sql+="Monday ";
					break;
				case 2:
					sql+="Tuesday ";
					break;
				case 3:
					sql+="Wednesday ";
					break;
				case 4:
					sql+="Thursday ";
					break;
				case 5:
					sql+="Friday ";
					break;
				case 6:
					sql+="Saturday ";
					break;
			}
			sql+="= '1'";
		}
		System.out.println(sql);
		return operator(sql);
	}
	public static Lunch[] search(int id) {//回傳指定id的整筆資料
		return operator("SELECT * FROM DATA WHERE id="+id);
	}
	public static Lunch[] KeywordSearch(String name) {//店名關鍵字搜尋
		return operator("SELECT * FROM DATA WHERE name LIKE '%"+name+"%'");
	}
	public static Lunch[] search(String name) {//回傳指定店名的整筆資料
		return operator("SELECT * FROM DATA WHERE name='"+name+"'");
	}
}