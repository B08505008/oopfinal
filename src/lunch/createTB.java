package lunch;
import java.sql.*;

public class createTB {
	 // JDBC driver name and database URL
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	   static final String DB_URL = "jdbc:mysql://localhost:3306/LUNCH";
	   //  Database credentials
	   static final String USER = "root";
	   static final String PASS = "12345678";
   
   public static void create() {
   Connection conn = null;
   Statement stmt = null;
   try{
      //STEP 2: Register JDBC driver
      Class.forName("com.mysql.jdbc.Driver");

      //STEP 3: Open a connection
      System.out.println("Connecting to a selected database...");
      conn = DriverManager.getConnection(DB_URL, USER, PASS);
      System.out.println("Connected database successfully...");
      
      //STEP 4: Execute a query
      System.out.println("Creating table in given database...");
      stmt = conn.createStatement();
      
      String sql = "CREATE TABLE DATA " +
                   "(id INT NOT NULL AUTO_INCREMENT,"+
                   " name VARCHAR(255), " +
                   " address VARCHAR(255), " + 
                   " place VARCHAR(255), " + 
                   " rate INTEGER, "+
                   " cost INTEGER, "+
                   " typ VARCHAR(255), "+
                   " Monday VARCHAR(1), "+
                   " Tuesday VARCHAR(1), "+
                   " Wednesday VARCHAR(1), "+
                   " Thursday VARCHAR(1), "+
                   " Friday VARCHAR(1), "+
                   " Saturday VARCHAR(1), "+
                   " Sunday VARCHAR(1), "+
                   " PRIMARY KEY (id))"; 

      stmt.executeUpdate(sql);
      System.out.println("Created table in given database...");
   }catch(SQLException se){
      //Handle errors for JDBC
      //se.printStackTrace();
   }catch(Exception e){
      //Handle errors for Class.forName
      //e.printStackTrace();
   }finally{
      //finally block used to close resources
      try{
         if(stmt!=null)
            conn.close();
      }catch(SQLException se){
      }// do nothing
      try{
         if(conn!=null)
            conn.close();
      }catch(SQLException se){
         se.printStackTrace();
      }//end finally try
   }//end try
   System.out.println("Goodbye!");
}//end main
}//end JDBCExample