package lunch;
public class Lunch{
	public String name; //店家名稱
	public String address; //店家地點
	public String place; //店家距離
	public int rate; //店家評價
	public int cost; //價格
    public String typ; //食物種類
    public char[] open; //營業時間，暫時印不出來
    public Lunch(String n,String a,String p,int r,int c,String t,char[] open){//店名,地點,距離,評價,低價,高價,種類,有沒有開*7(有開'1',沒開'0')
    	name=n;
    	address=a;
    	place=p;
    	rate=r;
    	cost=c;
    	typ=t;
    	this.open=open;
    }
    public String output() {
    	return "店名="+name+"，地址="+address+"，地點="+place+"，評價="+rate+"，價格="+cost+"，種類="+typ;
    }
    public String toString() { //給資料庫用的
    	String a="VALUES ('"+name+"','"+address+"','"+place+"',"+rate+","+cost+",'"+typ+"'";
    	for(char o:open) {
    		a+=",'"+o+"'";
    	}
    	a+=")";
    	return a;
    }
    public static void main(String[] args) { 
    	//DB.init();
    	//System.out.println(DB.getid("小惠"));
    	/*char[] b= {'0','1','0','1','0','1','0'};
		Lunch a=new Lunch("小惠","某個地方","長興",8,30,"中式",b);
		System.out.println(DB.insert(a));//把lunch輸入資料庫
		System.out.println(DB.update(a));
		Lunch[] rst=DB.search();//印出所有資料
		for(Lunch c:rst) {
			System.out.println(c.output());
		}
		System.out.println(DB.remove("小惠"));
		rst=DB.search();//印出所有資料
		for(Lunch c:rst) {
			System.out.println(c.output());
		}
    	String[] a={"長興"};
		DB.randompick();//隨機選取一筆資料
    	//DB.init();
    	char[] b= {'0','1','0','1','0','1','0'};
		Lunch a=new Lunch("小惠","某個地方","長興",8,30,"中式",b);
		System.out.println(DB.insert(a));//把lunch輸入資料庫
		System.out.println(DB.update(a));
		Lunch[] rst=DB.search();//印出所有資料
		for(Lunch c:rst) {
			System.out.println(c.output());
		}
		//DB.randompick();//隨機選取一筆資料
		DB.remove("小惠99");
		System.out.println("after remove...");
		Lunch[] rs=DB.search();//印出所有資料
		for(Lunch c:rs) {
			System.out.println(c.output());
		}
		DB.search();
		}
    	DB.randompick();//隨機選取一筆資料
		DB.remove("小惠1");
		System.out.println("after remove...");*/
    	//String[] a={"中式","長興"};
		Lunch[] rs=DB.KeywordSearch("小");//印出所有資料
		for(Lunch c:rs) {
			System.out.println(c.output());
		}
		//DB.search();
    }
    
}
