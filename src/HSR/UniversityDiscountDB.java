package HSR;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

public class UniversityDiscountDB {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost:3306/";
	//  Database credentials
	static final String USER = "root";
	static final String PASS = "12345678";
	static Connection conn = null;
	static Statement stmt = null;
	public static void init() {
		if(createTB.exist("UniversityDiscount")) {
			return;
		}
		createTB.create("CREATE TABLE UniversityDiscount " +
                   "(no VARCHAR(255), " +
                   " discount VARCHAR(255))");
		UniversityDiscount[] a=GetUniversityDiscount.getInfo();
		try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.cj.jdbc.Driver");

		      //STEP 3: Open a connection
		      //System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);

		      //STEP 4: Execute a query
		      //System.out.println("Deleting database...");
		      stmt = conn.createStatement();
		      for(UniversityDiscount k:a) {
		    	  String sql = "INSERT INTO UniversityDiscount "+
		                   "VALUES ('"+k.getTrainNo()+"','"+Arrays.toString(k.getDiscount())+"')";
		    	  stmt.executeUpdate(sql);
		      }
		      //System.out.println("Database deleted successfully...");
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }
	}
	public static String getDiscount(int a,String no) {
		String[] ans=getDiscount(a);
		for(String rt:ans) {
			if(rt.contains(no)) {
				return rt.split(",")[1];
			}
		}
		return null;
	}
	public static float getDiscountValue(int a,String no) {
		String[] ans=getDiscount(a);
		for(String rt:ans) {
			if(rt.contains(no)) {
				return Float.parseFloat(rt.split(",")[1]);
			}
		}
		return 0;
	}
	public static String[] getDiscount(int a) {
		ArrayList<String> result = new ArrayList<String>();
		try{
			//STEP 2: Register JDBC driver
			Class.forName("com.mysql.cj.jdbc.Driver");

			//STEP 3: Open a connection
			//System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);

			//STEP 4: Execute a query
			//System.out.println("Deleting database...");
			stmt = conn.createStatement();
			String sql = "select * from universitydiscount";
		    ResultSet rs = stmt.executeQuery(sql);
		    while(rs.next()) {
		    	String k=rs.getString("no");
		    	k+=","+TransForm.parsedata(rs.getString("discount"))[a];
		    	result.add(k);
		    }
		    String[] rt=new String[result.size()];
		    for(int i=0;i<result.size();i++) {
				rt[i]=result.get(i);
			}
		    return rt;
			//System.out.println("Database deleted successfully...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}
		return null;
	}
	public static void main(String[] args) {
		//init();
		String a=getDiscount(4,"0333");
		System.out.println(Float.parseFloat(a));
	}
}
