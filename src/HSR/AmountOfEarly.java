package HSR;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
public class AmountOfEarly {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost:3306/";
	//  Database credentials
	static final String USER = "root";
	static final String PASS = "12345678";
	static Connection conn = null;
	static Statement stmt = null;
	public static void exist(String day,int date) {
		try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.cj.jdbc.Driver");

		      //STEP 3: Open a connection
		      //System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);

		      //STEP 4: Execute a query
		      //System.out.println("Deleting database...");
		      stmt = conn.createStatement();
	    	  stmt.executeQuery("select * from early"+day);
		      
		      //System.out.println("Database deleted successfully...");
		   }catch(SQLException se){
		      //Handle errors for JDBC
			   init(day,date);
		      //se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }
	}
	public static void init(String day,int date) {//輸入日期以及星期幾(0-6)
		EarlyDiscount[] a=EarlyDiscount.getEarlyDiscountFromJson();
		try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.cj.jdbc.Driver");

		      //STEP 3: Open a connection
		      //System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);

		      //STEP 4: Execute a query
		      //System.out.println("Deleting database...");
		      stmt = conn.createStatement();
		      stmt.executeUpdate("truncate early"+day);
		      for(EarlyDiscount k:a) {
		    	  String sql = "INSERT INTO early"+day+
		                   " VALUES ('"+k.getTrainNo()+"',"+k.getDiscount()[date][0]+
		                   ","+k.getDiscount()[date][1]+","+k.getDiscount()[date][2]+")";
		    	  stmt.executeUpdate(sql);
		      }
		      //System.out.println("Database deleted successfully...");
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }
	}
	public static int[] print(String day,String no) {
		try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.cj.jdbc.Driver");

		      //STEP 3: Open a connection
		      //System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);

		      //STEP 4: Execute a query
		      //System.out.println("Deleting database...");
		      stmt = conn.createStatement();
		      String sql = "select * from early"+day+" where no='"+no+"'";
		      ResultSet rs = stmt.executeQuery(sql);
		      rs.next();
		    	  int[] discount=new int[3]; 
		    	  discount[0] = rs.getInt("nine");
		    	  discount[1] = rs.getInt("eight");
		    	  discount[2] = rs.getInt("sv");
		      return discount;
		      
		      //System.out.println("Database deleted successfully...");
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      return new int[3];
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		      
		   }
		return null;
	}
	public static void cancelticket(Ticket t) {
		int[] discount=print(t.Date,t.trainNo);
		try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.cj.jdbc.Driver");
		      //STEP 3: Open a connection
		      //System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);

		      //STEP 4: Execute a query
		      //System.out.println("Deleting database...");
		      stmt = conn.createStatement();
		      switch(""+t.discount) {
		      	case "0.9":
		    	  discount[0]+=1;
		    	  break;
		      	case "0.8":
			    	  discount[1]+=1;
			    	  break;
			    case "0.65":
			    	discount[2]+=1;
			    	break;  
		      	default:
		      		
		      }
		      String sql = "UPDATE early"+t.Date+" SET nine="+discount[0]+
		    		  " ,eight="+discount[1]+" ,sv="+discount[2]+" WHERE no='"+t.trainNo+"'";
		      stmt.executeUpdate(sql);
		      //System.out.println("Database deleted successfully...");
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		      
		   }
	}
	public static double buyticket(String day,String no) {
		Date date=new Date();
	    SimpleDateFormat ft = new SimpleDateFormat ("yyyyMMdd");
	    if(Math.abs(TransForm.dateSubtraction(day,ft.format(date)))<=5) {
			return 1; 
	    }
	    int[] discount=print(day,no);
		try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.cj.jdbc.Driver");
		      //STEP 3: Open a connection
		      //System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);

		      //STEP 4: Execute a query
		      //System.out.println("Deleting database...");
		      stmt = conn.createStatement();
		      Double rt=1.0;
		      if(discount[2]!=0) {
		    	  rt=0.65;
		    	  discount[2]-=1;
		      } else if(discount[1]!=0) {
		    	  rt=0.8;
		    	  discount[1]-=1;
		      } else {
		    	  rt=0.9;
		    	  discount[0]-=1;
		      }
		      String sql = "UPDATE early"+day+" SET nine="+discount[0]+
		    		  " ,eight="+discount[1]+" ,sv="+discount[2]+" WHERE no='"+no+"'";
		      stmt.executeUpdate(sql);
		      
		      return rt;
		      //System.out.println("Database deleted successfully...");
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		      
		   }
		return 0f;
	}
	public static float getdiscount(String day,String no) {
		if(print(day,no)[2]>0) {
			return 0.65f;
		} else if(print(day,no)[1]>0) {
			return 0.8f;
		}else if(print(day,no)[0]>0) {
			return 0.9f;
		}
		return 1.0f;
	}
	public static void main(String[] args) {
		//init("0621",0);
		for(int i=0;i<20;i++) {
			System.out.println(buyticket("0621","1505"));
		}
	}
}
