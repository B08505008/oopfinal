package HSR;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

public class TrainInfoDB {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost:3306/";
	//  Database credentials
	static final String USER = "root";
	static final String PASS = "12345678";
	static Connection conn = null;
	static Statement stmt = null;
	public static void init() {
		if(createTB.exist("TrainInfo")) {
			return;
		}
		createTB.create("CREATE TABLE TrainInfo"+
				"(no VARCHAR(255), " +
				" direction INTEGER, "+
				" startingStation VARCHAR(255), "+
				" endingStation VARCHAR(255), "+
				" stopStations VARCHAR(255), "+
				" depatureTimes VARCHAR(255),"+ 
				" serviceday VARCHAR(255))");
		TrainInfo[] a=GetTimeTableFromJson.getTimeTable();
		try{
			//STEP 2: Register JDBC driver
			Class.forName("com.mysql.cj.jdbc.Driver");

			//STEP 3: Open a connection
			//System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);

			//STEP 4: Execute a query
			//System.out.println("Deleting database...");
			stmt = conn.createStatement();
			for(TrainInfo k:a) {
				String sql = "INSERT INTO traininfo "+
						"VALUES ('"+k.getTrainNo()+"',"+k.getDirection()+",'"+
						k.getStartingStation()+"','"+k.getEndingStation()+"','"+
						k.getStopStations().toString()+"','"+k.getDepatureTimes().toString()+
						"','"+Arrays.toString(k.getService())+"')";
				stmt.executeUpdate(sql);
			}
			//System.out.println("Database deleted successfully...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}
	}
	public static String[] getTiming(String a) {
		ArrayList<String> result = new ArrayList<String>();
		String[] par=a.split(",");
		int way=0;
		if(par[0].compareTo(par[1])>0) {
			way=1;
		}
		try{
			//STEP 2: Register JDBC driver
			Class.forName("com.mysql.cj.jdbc.Driver");

			//STEP 3: Open a connection
			//System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);

			//STEP 4: Execute a query
			//System.out.println("Deleting database...");
			stmt = conn.createStatement();
			String sql = "select * from traininfo where direction="+way+" ";
		    ResultSet rs = stmt.executeQuery(sql);
		    while(rs.next()) {
		    	String[] station=TransForm.parsedata(rs.getString("stopStations"));
		    	if(!(Arrays.stream(station).anyMatch(par[0]::equals)&Arrays.stream(station).anyMatch(par[1]::equals))) {
		    		continue;
		    	}
		    	String k=rs.getString("no");
		    	Timing[] time=TransForm.parsetime(rs.getString("depatureTimes"));
		    	for(int i=0;i<station.length;i++) {
		    		if(station[i].equals(par[0])) {
		    			k+=","+time[i].toString();
		    		}
		    		if(station[i].equals(par[1])) {
		    			k+=","+time[i].toString();
		    		}
		    	}
		    	result.add(k);
		    }
		    String[] rt=new String[result.size()];
		    for(int i=0;i<result.size();i++) {
				rt[i]=result.get(i);
			}
		    return rt;
			//System.out.println("Database deleted successfully...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}
		return null;
	}
	public static String[] getStation(String no) {
		try{
			//STEP 2: Register JDBC driver
			Class.forName("com.mysql.cj.jdbc.Driver");

			//STEP 3: Open a connection
			//System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);

			//STEP 4: Execute a query
			//System.out.println("Deleting database...");
			stmt = conn.createStatement();
			String sql = "select * from traininfo where no='"+no+"'";
		    ResultSet rs = stmt.executeQuery(sql);
		    rs.next();
		    String[] station=TransForm.parsedata(rs.getString("stopStations"));
		    return station;
			//System.out.println("Database deleted successfully...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}
		return null;
	}
	public static boolean noExist(String a) {

		try{
			//STEP 2: Register JDBC driver
			Class.forName("com.mysql.cj.jdbc.Driver");

			//STEP 3: Open a connection
			//System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);

			//STEP 4: Execute a query
			//System.out.println("Deleting database...");
			stmt = conn.createStatement();
			String sql = "select * from traininfo where no='"+a+"'";
		    ResultSet rs = stmt.executeQuery(sql);
		    return rs.next();
			//System.out.println("Database deleted successfully...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}
		return false;
	}
	public static String getTime(String no,String start,String end) {
		try{
			//STEP 2: Register JDBC driver
			Class.forName("com.mysql.cj.jdbc.Driver");

			//STEP 3: Open a connection
			//System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);

			//STEP 4: Execute a query
			//System.out.println("Deleting database...");
			stmt = conn.createStatement();
			String sql = "select * from traininfo where no='"+no+"' ";
		    ResultSet rs = stmt.executeQuery(sql);
		    rs.next();
		    String ans="";
		    String st=rs.getString("stopStations");
		    String rt=rs.getString("depatureTimes");
		    String[] stt=TransForm.parsedata(st);
		    String[] rtt=TransForm.parsedata(rt);
		    for(int i=0;i<stt.length;i++) {
		    	if(stt[i].equals(start)) {
		    		ans+=rtt[i];
		    	}
		    	if(stt[i].equals(end)) {
		    		ans+=","+rtt[i];
		    		break;
		    	}
		    }
		    return ans;
			//System.out.println("Database deleted successfully...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}
		return null;
	}
	public static void main(String[] args) {
		//init();
		//String a=getTime("0108","1070","1000");
		System.out.println(noExist("0628"));
	}
}
