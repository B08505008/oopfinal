package HSR;

import java.io.*;
import java.util.ArrayList;
import org.json.*;

//Call GetTimeTableFromJson.gettimesTable() to get an array of all TrainInfo 
public class GetTimeTableFromJson {

	public static TrainInfo openGeneralTimeTable(JSONObject thisTrain) {

		String trainNo = thisTrain.getJSONObject("GeneralTrainInfo").getString("TrainNo");
		int direction = thisTrain.getJSONObject("GeneralTrainInfo").getInt("Direction");
		String startingStation = thisTrain.getJSONObject("GeneralTrainInfo").getString("StartingStationID");
		String endingStation = thisTrain.getJSONObject("GeneralTrainInfo").getString("EndingStationID");

		JSONArray stopInfo = thisTrain.getJSONArray("StopTimes");
		ArrayList<String> stopStations = new ArrayList<String>();
		ArrayList<String> depatureTimes = new ArrayList<String>();

		for (int j = 0; j < stopInfo.length(); j++) {
			JSONObject s = stopInfo.getJSONObject(j);
			stopStations.add(s.getInt("StopSequence")-1,s.getString("StationID"));
			depatureTimes.add(s.getInt("StopSequence")-1,s.getString("DepartureTime"));
		}

		int[] service = new int[7];
		service[0] = thisTrain.getJSONObject("ServiceDay").getInt("Monday");
		service[1] = thisTrain.getJSONObject("ServiceDay").getInt("Tuesday");
		service[2] = thisTrain.getJSONObject("ServiceDay").getInt("Wednesday");
		service[3] = thisTrain.getJSONObject("ServiceDay").getInt("Thursday");
		service[4] = thisTrain.getJSONObject("ServiceDay").getInt("Friday");
		service[5] = thisTrain.getJSONObject("ServiceDay").getInt("Saturday");
		service[6] = thisTrain.getJSONObject("ServiceDay").getInt("Sunday");

		return new TrainInfo(trainNo, direction, startingStation, endingStation, stopStations, depatureTimes, service);
	}

	public static TrainInfo[] getTimeTable() {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader("../oopfinal/src/json/timeTable.json"));
			String str = null;
			String data = "";
			while ((str = br.readLine()) != null) {
				data = data + str + "\n";
			}
			br.close();
			JSONArray allData = new JSONArray(data);
			TrainInfo[] timeTable = new TrainInfo[allData.length()];

			for (int i = 0; i < allData.length(); i++) {
				timeTable[i] = openGeneralTimeTable(allData.getJSONObject(i).getJSONObject("GeneralTimetable"));
			}
			return timeTable;

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}


}