package HSR;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class PanelTest {
	private JFrame frame = new JFrame("今天午餐要吃什麼好呢");
	private JPanel panel1 = new JPanel();
	private JPanel panel21 = new JPanel();
	
	private JComboBox jcbx1 = new JComboBox();
	private JComboBox jcbx2 = new JComboBox();
	private JComboBox jcbx3 = new JComboBox();
	
	private JLabel l1 = new JLabel("v.s.");
	private JLabel l2 = new JLabel("身分證字號");
	private JLabel l3 = new JLabel();
	private JLabel l4 = new JLabel();
	private JLabel l5 = new JLabel();
	private JLabel l6 = new JLabel();
	private JLabel l7 = new JLabel();

	private JButton search1 = new JButton("我要找午餐");
	private JButton data1 = new JButton("我要改資料");
	
	private JButton back21 = new JButton("回上頁");
	private JButton shopName21 = new JButton("依店名搜尋");
	private JButton condition21 = new JButton("依條件搜尋");
	
	private JTextField t1 = new JTextField();
	
	private JComponent[] jc1 = {search1,data1 };
	private JComponent[] jc21 = {back21,shopName21,condition21};
	private String userinput;

	private int fill[] = { GridBagConstraints.BOTH, GridBagConstraints.VERTICAL, GridBagConstraints.HORIZONTAL,
			GridBagConstraints.NONE };
	private int anchor[] = { GridBagConstraints.CENTER, GridBagConstraints.EAST, GridBagConstraints.SOUTHEAST,
			GridBagConstraints.SOUTH, GridBagConstraints.SOUTHWEST, GridBagConstraints.WEST,
			GridBagConstraints.NORTHWEST, GridBagConstraints.NORTH, GridBagConstraints.NORTHEAST };
	
	private int layout1[][] = {{ 0, 1, 3, 1, 0, 0, fill[0], anchor[5] },{ 0, 2, 3, 1, 0, 0, fill[0], anchor[5] }};
	private int layout21[][] = {{ 0, 0, 3, 1, 0, 0, fill[0], anchor[5] }, { 0, 1, 3, 1, 0, 0, fill[0], anchor[5] },{ 0, 2, 3, 1, 0, 0, fill[0], anchor[5] }};
	
	
	public void add(JPanel panel,JComponent[] jc , int[][] layout ,int i) {
		GridBagConstraints c1 = new GridBagConstraints();
		c1.gridx = layout[i][0];
		c1.gridy = layout[i][1];
		c1.gridwidth = layout[i][2];
		c1.gridheight = layout[i][3];
		c1.weightx = layout[i][4];
		c1.weighty = layout[i][5];
		c1.fill = layout[i][6];
		c1.anchor = layout[i][7];
		panel.add(jc[i], c1);
	}
	
	public void setPage1() {
		for (int i = 0; i < jc1.length; i++) {
			add(panel1,jc1,layout1,i);
		}
	}
	
	public void removePage1() {
		frame.remove(panel1);
	}
	
	public void setPage21() {
		for (int i = 0; i < jc21.length; i++) {
			add(panel21,jc21,layout21,i);
		}
	}
	
	public void removePage21() {
			frame.remove(panel21);
	}
	
	
	public PanelTest() {
		frame.setSize(600, 160);
		frame.setLayout(new GridBagLayout());
		panel1.setLayout(new GridBagLayout());
		panel21.setLayout(new GridBagLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		search1.addActionListener(new Search1Listener());
		back21.addActionListener(new Back21Listener());
		
		setPage1();
		setPage21();
		frame.add(panel1);

		frame.setVisible(true);
	}

	
	
	class Search1Listener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			frame.add(panel21);
			frame.setVisible(true);
			removePage1();
			frame.setVisible(true);
			
		}
		
	}
	
	class Back21Listener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			frame.add(panel1);
			frame.setVisible(true);
			removePage21();
			frame.setVisible(true);
		}
		
	}
}
