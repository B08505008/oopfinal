package HSR;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.text.*;
public class TransForm {
	public static String[] parsedata(String a) {
		a=a.replace("[", "");
		a=a.replace("]", "");

		String[] c=a.split(", ");
		return c;
	}
	
	
	public static Date StingToDate(String d) {
		SimpleDateFormat ft = new SimpleDateFormat ("yyyyMMdd"); 
		Date t; 
		try { 
			t = ft.parse(d); 
			return t;
		}catch (ParseException e) { 
			System.out.println("Unparseable using " + ft); 
		}
		return null;
	}
	
	//取得星期
	public static int DateToDay(String a) {
		Date t = StingToDate(a);
		if(t == null) {return 0;}
		return (t.getDay()+6)%7;
	}
	
	//日期取差值
	public static int dateSubtraction(String d1, String d2) {
		Date date1 = StingToDate(d1);
		Date date2 = StingToDate(d2);
        long diff = date2.getTime() - date1.getTime();

        TimeUnit time = TimeUnit.DAYS; 
        int diffrence = (int)time.convert(diff, TimeUnit.MILLISECONDS);
        return diffrence;
	}
	
	//日期相加 i:往後推幾日
	public static String dateAdd(String d1, int i) {
		Date original = StingToDate(d1);
		long time = original.getTime();
		time += i*24*60*60*1000L;
		Date result = new Date();
		result.setTime(time);
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");  
		return dateFormat.format(result);
	}
	
	public static Timing[] parsetime(String a) {
		a=a.replace("[", "");
		a=a.replace("]", "");
		ArrayList<Timing> ans=new ArrayList<Timing>();
		String[] c=a.split(", ");
		for(int i=0;i<c.length;i++) {
			ans.add(new Timing(c[i]));
		}
		Timing[] cc=new Timing[ans.size()];
		for(int i=0;i<ans.size();i++) {
			cc[i]=ans.get(i);
		}
		return cc;
	}
	public static String transStationID(String s){//輸入站名ID，回傳站名
		switch (s) {
		case"0990":
			return "南港";
		case"1000":
			return "台北";
		case"1010":
			return "板橋";
		case"1020":
			return "桃園";
		case"1030":
			return "新竹";
		case"1035":
			return "苗栗";
		case"1040":
			return "台中";
		case"1043":
			return "彰化";
		case"1047":
			return "雲林";			
		case"1050":
			return "嘉義";
		case"1060":
			return "台南";
		case"1070":
			return "左營";
		default:
			return null;
		}
	}
	public static void main(String[] args) {
		System.out.println(DateToDay("20210626")); 
	}
	
	
}
