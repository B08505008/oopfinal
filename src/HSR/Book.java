package HSR;

public class Book {
	private String uid;
	private String bookingCode;
	private String id;
	private int year;
	private int month;
	private int day;
	private int hour;
	private int minute;
	private String ticketType;
	private int numberOfTickets;
	private String initial;
	private String arrival;
	


	public Book(String uid,int year,int month, int day, int hour, int minute,
				String initial, String arrival, String ticketType, int numberOfTickets) throws BookException {
		setUid(uid);
		setYear(year);
		setMonth(month);
		setDay(day);
		setHour(hour);
		setMinute(minute);
		setInitial(initial);
		setArrival(arrival);
		setTicketType(ticketType);
		setNumberOfTickets(numberOfTickets);
		
	}
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getBookingCode() {
		return bookingCode;
	}
	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) throws BookException {
		if(1<=month && month<=12) {
			this.month = month;
		}
		else {
			throw new BookException("month");
		}
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getHour() {
		return hour;
	}
	public void setHour(int hour) {
		this.hour = hour;
	}
	public int getMinute() {
		return minute;
	}
	public void setMinute(int minute) {
		this.minute = minute;
	}
	public String getTicketType() {
		return ticketType;
	}
	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}
	public int getNumberOfTickets() {
		return numberOfTickets;
	}
	public void setNumberOfTickets(int numberOfTickets) {
		this.numberOfTickets = numberOfTickets;
	}
	public String getInitial() {
		return initial;
	}
	public void setInitial(String intial) {
		this.initial = intial;
	}
	
	
	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}
	
}
