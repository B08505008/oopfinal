package HSR;

import java.sql.*;

public class createTB {
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/HSR";
	// Database credentials
	static final String USER = "root";
	static final String PASS = "12345678";
	public static void initday(String day) {
		if(exist("ticket"+day)) {
			System.out.println("exist day");
			return;
		}
		create("CREATE TABLE early"+day+
                "(no VARCHAR(255), "+
                " nine INTEGER, "+
                " eight INTEGER, "+
                " sv INTEGER)");
		AmountOfEarly.init(day, TransForm.DateToDay(day));
		create("CREATE TABLE ticket"+day+
                " (no VARCHAR(255), "+
                " start VARCHAR(255), "+
                " end VARCHAR(255), "+
                " date VARCHAR(255), "+
                " starttime VARCHAR(255), "+
                " endtime VARCHAR(255), "+
                " seat VARCHAR(255), "+
                " type INTEGER, "+
                " standard VARCHAR(255), "+
                " discount DOUBLE, "+
                " UID VARCHAR(255), "+
                " code INTEGER, "+
                " price INTEGER, "+
                " paydeadline VARCHAR(255))");
	}
	public static boolean exist(String table) {
		Connection conn = null;
		Statement stmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			// STEP 3: Open a connection
			//System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			//System.out.println("Connected database successfully...");

			// STEP 4: Execute a query
			//System.out.println("Creating table in given database...");
			stmt = conn.createStatement();
			stmt.executeUpdate("CREATE TABLE "+table+
	                   " (car INTEGER, "+
	                   " position INTEGER, "+
	                   " roww INTEGER, "+
	                   " seatcode VARCHAR(255))");
			stmt.executeUpdate("DROP TABLE "+table);
			//System.out.println("Created table in given database...");
		} catch (SQLException se) {
			// Handle errors for JDBC
			return true;
			//se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			//e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					conn.close();
			} catch (SQLException se) {
			} // do nothing
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try
		return false;
	}
	public static void create(String sql) {
		Connection conn = null;
		Statement stmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			// STEP 3: Open a connection
			//System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			//System.out.println("Connected database successfully...");

			// STEP 4: Execute a query
			//System.out.println("Creating table in given database...");
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			//System.out.println("Created table in given database...");
		} catch (SQLException se) {
			// Handle errors for JDBC
			//se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			//e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					conn.close();
			} catch (SQLException se) {
			} // do nothing
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try
		//System.out.println("Goodbye!");
	}// end main
}// end JDBCExample