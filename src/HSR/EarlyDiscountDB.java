package HSR;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
public class EarlyDiscountDB {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost:3306/";
	//  Database credentials
	static final String USER = "root";
	static final String PASS = "12345678";
	static Connection conn = null;
	static Statement stmt = null;
	public static void init() {
		if(createTB.exist("EarlyDiscount")) {
			return;
		}
		createTB.create("CREATE TABLE EarlyDiscount " +
                   "(no VARCHAR(255), " +
                   " Monday VARCHAR(255), "+
                   " Tuesday VARCHAR(255), "+
                   " Wednesday VARCHAR(255), "+
                   " Thursday VARCHAR(255), "+
                   " Friday VARCHAR(255), "+
                   " Saturday VARCHAR(255), "+
                   " Sunday VARCHAR(255))");
		EarlyDiscount[] a=EarlyDiscount.getEarlyDiscountFromJson();
		try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.cj.jdbc.Driver");

		      //STEP 3: Open a connection
		      //System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);

		      //STEP 4: Execute a query
		      //System.out.println("Deleting database...");
		      stmt = conn.createStatement();
		      for(EarlyDiscount k:a) {
		    	  String sql = "INSERT INTO earlydiscount (no,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday)"+
		                   "VALUES ('"+k.getTrainNo()+"'";
		    	  for(int i=0;i<7;i++) {
		    		  sql+=",'"+Arrays.toString(k.getDiscount()[i])+"'";
		    	  }
		    	  sql+=")";
		    	  stmt.executeUpdate(sql);
		      }
		      //System.out.println("Database deleted successfully...");
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }
	}
	public static void getdiscount(String date) {
		ArrayList<EarlyDiscount> sites = new ArrayList<EarlyDiscount>();
		try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.cj.jdbc.Driver");

		      //STEP 3: Open a connection
		      //System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);

		      //STEP 4: Execute a query
		      //System.out.println("Deleting database...");
		      stmt = conn.createStatement();
		      String sql = "SELECT no,"+date+" FROM earlydiscount";
		      ResultSet rs = stmt.executeQuery(sql);
		      //STEP 5: Extract data from result set
		      while(rs.next()){
		         //Retrieve by column name
		         String no = rs.getString("no"); //店家名稱
		     	 String discount = rs.getString(date); //店家地點
		      }
		      rs.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }
	}
	public static void main(String[] args) {
		init();
	}
}
