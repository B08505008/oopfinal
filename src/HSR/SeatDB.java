package HSR;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

public class SeatDB {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost:3306/";
	//  Database credentials
	static final String USER = "root";
	static final String PASS = "12345678";
	static Connection conn = null;
	static Statement stmt = null;
	public static void init() {
		createTB.create("CREATE TABLE Seat " +
                   "(car INTEGER, "+
                   " position INTEGER, "+
                   " roww INTEGER, "+
                   " seatcode VARCHAR(255))");
		Seat[] a=Seat.getSeatFromJson();
		try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.cj.jdbc.Driver");

		      //STEP 3: Open a connection
		      //System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);

		      //STEP 4: Execute a query
		      //System.out.println("Deleting database...");
		      stmt = conn.createStatement();
		      for(Seat k:a) {
		    	  String sql = "INSERT INTO seat "+
		          "VALUES ("+k.getCar()+","+k.getPosition()+","+k.getRow()+",'"+k.getSeatCode()+"')";
		    	  stmt.executeUpdate(sql);
		      }
		      //System.out.println("Database deleted successfully...");
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }
	}
	public static void main(String[] args) {
		init();
	}
}
