package HSR;

public class Ticket {

	@Override
	public String toString() {
		return "Ticket [trainNo=" + trainNo + ", startStation=" + startStation + ", endStation=" + endStation
				+ ", Date=" + Date + ", startTime=" + startTime + ", endTime=" + endTime + ", seat=" + seat
				+ ", standard=" + standard + ", type=" + type + ", price=" + price + ", discount=" + discount + ", uid="
				+ uid + ", payDeadLine=" + payDeadLine + ", code=" + code + "]";
	}

	public String trainNo;
	public String startStation;
	public String endStation;
	public String Date;
	public Timing startTime;
	public Timing endTime;
	public Seat seat;
	public boolean standard;// true for standard, false for business;

	/*public enum typeenum {
		ADULT, CHILD, ELDER, DISABLED, EARLYDISCOUNT, UNIVERSITYDISCOUNT;
	}這個東西先用不到*/

	public int type;
	public int price;
	public double discount; // for early discount and university discount;
	public String uid; // 使用者
	public String payDeadLine;
	public int code; // 定位代碼，由order帶入

	public Ticket(String trainNo, String startStation, String endStation, String date, Timing startTime, Timing endTime,
			Seat seat, int type, boolean standard, double discount, String uid, int code, String payDeadLine) {
		super();
		this.trainNo = trainNo;
		this.startStation = startStation;
		this.endStation = endStation;
		this.Date = date;
		this.startTime = startTime;
		this.endTime = endTime;
		this.seat = seat;
		this.type = type;
		this.standard = standard;
		this.discount = discount;
		this.uid = uid;
		this.code = code;
		this.payDeadLine = payDeadLine;
		calculatePrice();
	}

	public void calculatePrice() {
		this.price = new Price(startStation, endStation, type, standard, discount).getPrice();
	}

	// ticket type:int to string
	public String getTicketType() {
		int d = (int) (discount * 100);
		if (d % 10 == 0) {
			d /= 10;
		}
		
		
		switch (this.type) {
		case 0:
			return "全票";
		case 1:
			return "孩童";
		case 2:
			return "敬老";
		case 3:
			return "愛心";
		case 4:
			return ("全票 (" + d + "折)");
		case 5:
			return (d == 10)?("大學生"):("大學生 (" + d + "折)");
		default:
			return "無效票";
		}
	}

	// ticket type:string to int
	public static int getTypeIndex(String s) {

		switch (s) {
		case "成人":
			return 0;
		case "孩童":
			return 1;
		case "敬老":
			return 2;
		case "愛心":
			return 3;
		case "早鳥優惠":
			return 4;
		case "大專生優惠":
			return 5;
		default:
			return -1;
		}

	}
	
	public String getSeat() {
		return(this.seat.getCar()+"車 "+this.seat.getRow()+this.seat.getSeatCode());
	}

}
