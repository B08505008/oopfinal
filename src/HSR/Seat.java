package HSR;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;

public class Seat {

	private int car;
	private int position; // 0 for window, 1 for middle, 2 for aisle, 3 for error.
	private int row;
	private char seatCode;
	
	
	public Seat(int car, int row, char seatCode) {
		super();
		this.car = car;
		this.row = row;
		this.seatCode = seatCode;
		if(seatCode == 'A'|| seatCode == 'E') {
			position = 0;
		}else if(seatCode == 'B') {
			position = 1;
		}else if(seatCode == 'C'||seatCode == 'D') {
			position = 2;
		}else {
			position = 3;
		}
		
	}
	public Seat(String seat) {
		String[] c=seat.split(",");
		this.car = Integer.parseInt(c[0]);
		this.row = Integer.parseInt(c[1]);
		this.seatCode = c[2].charAt(0);
		if(seatCode == 'A'|| seatCode == 'E') {
			position = 0;
		}else if(seatCode == 'B') {
			position = 1;
		}else if(seatCode == 'C'||seatCode == 'D') {
			position = 2;
		}else {
			position = 3;
		}
	}
	public String toString() {
		return ""+car+","+row+","+seatCode;
	}


	public int getCar() {
		return car;
	}

	public int getPosition() {
		return position;
	}

	public int getRow() {
		return row;
	}

	public char getSeatCode() {
		return seatCode;
	}

	// call this function to generate seat table.
	public static Seat[] getSeatFromJson() {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader("../oopfinal/src/json/seat.json"));
			String str = null;
			String data = "";
			while ((str = br.readLine()) != null) {
				data = data + str + "\n";
			}
			br.close();
			JSONArray allCar = new JSONObject(data).getJSONArray("cars");

			ArrayList<Seat> seatTable = new ArrayList<>();

			for (int i = 0; i < allCar.length(); i++) {
				JSONObject thisCar = allCar.getJSONObject(i);
				int car = thisCar.getInt("car");
				JSONObject allRows = thisCar.getJSONObject("seats");

				Iterator<String> keys = allRows.keys();
				while (keys.hasNext()) {
					String a = keys.next();
					JSONArray thisRow = allRows.getJSONArray(a);
					for (int j = 0; j < thisRow.length(); j++) {
						char code = thisRow.getString(j).charAt(0);
						seatTable.add(new Seat(car, Integer.valueOf(a), code));
					}
				}
			} 
			
			Seat[] result = new Seat[seatTable.size()];
			result = seatTable.toArray(result);  
			return result;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}

	}
	
}
