package HSR;

import java.io.*;
import java.util.ArrayList;
import org.json.*;

//Call GetTimeTableFromJson.gettimeTable() to get an array of all TrainInfos 
public class GetUniversityDiscount {

	public static UniversityDiscount[] getInfo() {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader("../oopfinal/src/json/universityDiscount.json"));
			String str = null;
			String data = "";
			while ((str = br.readLine()) != null) {
				data = data + str + "\n";
			}
			br.close();
			JSONArray allData = new JSONArray(new JSONObject(data).getJSONArray("DiscountTrains"));
			UniversityDiscount[] result = new UniversityDiscount[allData.length()];

			for (int i = 0; i < allData.length(); i++) {
				JSONObject info = allData.getJSONObject(i);
				double discount[] = new double[7];

				discount[0] = info.getJSONObject("ServiceDayDiscount").getDouble("Monday");
				discount[1] = info.getJSONObject("ServiceDayDiscount").getDouble("Tuesday");
				discount[2] = info.getJSONObject("ServiceDayDiscount").getDouble("Wednesday");
				discount[3] = info.getJSONObject("ServiceDayDiscount").getDouble("Thursday");
				discount[4] = info.getJSONObject("ServiceDayDiscount").getDouble("Friday");
				discount[5] = info.getJSONObject("ServiceDayDiscount").getDouble("Saturday");
				discount[6] = info.getJSONObject("ServiceDayDiscount").getDouble("Sunday");

				String trainNo = info.getString("TrainNo");
				result[i] = new UniversityDiscount(trainNo, discount);
			}
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


}