package HSR;

public class TimeTableSearchX extends TimeTableSearch {
	public String startStation;
	public String endStation;
	public String Date;
	public boolean universityDiscountonly;
	public boolean earlyDiscountonly;
	public boolean standard;
	public TimeTableSearchX(String startStation, String endStation, String date, boolean universityDiscountonly,
			boolean earlyDiscountonly,boolean standard) {
		super(startStation,endStation,date,universityDiscountonly,earlyDiscountonly);
		this.standard=standard;
	}
}
