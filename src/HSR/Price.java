package HSR;

public class Price {
	public int startStation;
	public int endStation;
	public int type;
	public boolean standard;
	public double discount;
	
	//北到南依序, {一般座}{商務座}
	int[][] arr1 = { { 40, 70, 200, 330, 480, 750, 870, 970, 1120, 1390, 1530}, //一般 南港出發 arr[0][X]
						 {40, 160, 290, 430, 700, 820, 930, 1080, 1350, 1490}, //arr[1][X]
						     {130, 260, 400, 670, 790, 900, 1050, 1320, 1460}, //arr[2][X]
						          {130, 280, 540, 670, 780,  920, 1190, 1330}, //arr[3][X]
						               {140, 410, 540, 640,  790, 1060, 1200}, //arr[4][X]
						                    {270, 390, 500,  640,  920, 1060}, //arr[5][X]
						                         {130, 230,  380,  650,  790}, //arr[6][X]
						                              {110,  250,  530,  670}, //arr[7][X]
						                                    {150,  420,  560}, //arr[8][X]
						                                          {280,  410}, //arr[9][X]
						                                                {140}}; //arr[10][X]
	int[][] arr2 = {{260, 310, 500, 700, 920, 1330, 1510, 1660, 1880, 2290, 2500}, //商務 南港出發  arr[0][X] 
                        {260, 440, 640, 850, 1250, 1430, 1600, 1820, 2230, 2440}, 
                             {400, 590, 800, 1210, 1390, 1550, 1780, 2180, 2390}, 
                                  {400, 620, 1010, 1210, 1370, 1580, 1990, 2200},
                                       {410,  820, 1010, 1160, 1390, 1790, 2000}, 
                                             {610,  790,  950, 1160, 1580, 1790}, 
                                                   {400,  550,  770, 1180, 1390}, 
                                                         {370,  580, 1000, 1210}, 
                                                               {430,  830, 1040}, 
                                                                     {620,  820}, 
                                                                           {410} }; //arr[21][X]
	
	
	public Price ( String startStation, String endStation, int type, boolean standard , double discount) {
		super();                       //是否需要??
		this.startStation = Integer.parseInt(startStation);  //string轉成int
		this.endStation = Integer.parseInt(endStation);
		this.type = type;
		this.standard = standard;
		this.discount = discount;
	}
	
	
	
	
	public int getPrice() {
		
		int temporary=0;
		if(endStation<startStation) {
			temporary=endStation;
			endStation=startStation;
			startStation=temporary;
		}
		
		//起站代碼換成0~11
		switch(startStation) {
		case 990:
			startStation=0;
			break;
		case 1000:
			startStation=1;
			break;
		case 1010:
			startStation=2;
			break;
		case 1020:
			startStation=3;
			break;
		case 1030:
			startStation=4;
			break;
		case 1035:
			startStation=5;
			break;
		case 1040:
			startStation=6;
			break;
		case 1043:
			startStation=7;
			break;
		case 1047:
			startStation=8;
			break;
		case 1050:
			startStation=9;
			break;
		case 1060:
			startStation=10;
			break;
		case 1070:
			startStation=11;
			break;
		
		}
		
		//終站代碼換成0~11
		switch( endStation ) {
		case 990:
			endStation=0;
			break;
		case 1000:
			endStation=1;
			break;
		case 1010:
			endStation=2;
			break;
		case 1020:
			endStation=3;
			break;
		case 1030:
			endStation=4;
			break;
		case 1035:
			endStation=5;
			break;
		case 1040:
			endStation=6;
			break;
		case 1043:
			endStation=7;
			break;
		case 1047:
			endStation=8;
			break;
		case 1050:
			endStation=9;
			break;
		case 1060:
			endStation=10;
			break;
		case 1070:
			endStation=11;
			break;
		}
		
		if(standard) {//標準艙
			if(type==0 && discount==0) {//防呆
				return arr1[startStation][endStation-startStation-1];
			}else {
				if(type==1||type==2||type==3) { //都五折
					return arr1[startStation][endStation-startStation-1]/2;//查字典的概念
				}else if(type == 4 || type == 5){
					return ((int)(arr1[startStation][endStation-startStation-1]*discount/5.0))*5;
				}else{//原價
					return arr1[startStation][endStation-startStation-1];
				}
			}
			
			
		}
		
		if(!standard) {//商務艙
			if(type==0 && discount==0) {
				return arr2[startStation][endStation-startStation-1];
			}else {
				if(type==1||type==2||type==3) { //都五折
					return arr2[startStation][endStation-startStation-1]/2;//查字典的概念
				}else if(type == 4 || type == 5){  //折數已有
					return -1;//出現bug了 商務艙沒有早鳥票跟大專票
				}else{//原價
					return arr2[startStation][endStation-startStation-1];
				}
			}
			
		}
		return -1;
	}

}
