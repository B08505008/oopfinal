package HSR;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;

import org.json.*;

public class EarlyDiscount {
	@Override
	public String toString() {
		return "EarlyDiscount [trainNo=" + trainNo + ", discount=" + Arrays.toString(discount[0]) + "]";
	}

	// 每一車次一周的優惠資訊打包於一個EarlyDiscount object中
	// call EarlyDiscount.getEarlyDiscountFromJson()獲得所有車次的優惠資訊
	private String trainNo;
	private int discount[][] = new int[7][3]; // from MON to SUN;9折，8折，65折的張數

	public EarlyDiscount(String trainNo, int[][] discount) {
		super();
		this.trainNo = trainNo;
		this.discount = discount;
	}

	
	public String getTrainNo() {
		return trainNo;
	}

	public int[][] getDiscount() {
		return discount;
	}


	public static EarlyDiscount[] getEarlyDiscountFromJson() {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader("../oopfinal/src/json/earlyDiscount.json"));
			String str = null;
			String data = "";
			while ((str = br.readLine()) != null) {
				data = data + str + "\n";
			}
			br.close();
			JSONArray allDiscountData = new JSONObject(data).getJSONArray("DiscountTrains");
			
			EarlyDiscount[] result = new EarlyDiscount[allDiscountData.length()];			
			for(int i =0; i <allDiscountData.length(); i++) {

				
				JSONObject thisTrain = allDiscountData.getJSONObject(i);
				String trainNo = thisTrain.getString("TrainNo");
				JSONObject discountInfo = thisTrain.getJSONObject("ServiceDayDiscount");
				
				int discount[][] = new int [7][3];
				
				discount[0] = solveWeekdayInfo(discountInfo.get("Monday"));
				discount[1] = solveWeekdayInfo(discountInfo.get("Tuesday"));
				discount[2] = solveWeekdayInfo(discountInfo.get("Wednesday"));
				discount[3] = solveWeekdayInfo(discountInfo.get("Thursday"));
				discount[4] = solveWeekdayInfo(discountInfo.get("Friday"));
				discount[5] = solveWeekdayInfo(discountInfo.get("Saturday"));
				discount[6] = solveWeekdayInfo(discountInfo.get("Sunday"));
				
				
				result[i] = new EarlyDiscount(trainNo,discount);
			}
			
			return result;
			} catch (Exception e) {
				e.printStackTrace();
			return null;
		}
	}
	

	


	private static int[] solveWeekdayInfo(Object a) {
		if(a.getClass() == new JSONArray().getClass()) {
			int discount[] = new int[] { 0, 0, 0 };
			for (int i = 0; i < ((JSONArray) a).length(); i++) {
				double rate = ((JSONArray) a).getJSONObject(i).getDouble("discount");
				if (rate == 0.9) {
					discount[0] = ((JSONArray) a).getJSONObject(i).getInt("tickets");
				} else if (rate == 0.8) {
					discount[1] = ((JSONArray) a).getJSONObject(i).getInt("tickets");
				} else if (rate == 0.65) {
					discount[2] = ((JSONArray) a).getJSONObject(i).getInt("tickets");
				}
			}
			return discount; // 回傳陣列為{0.9張數,0.8張數,0.65張數)
		}
		
		return new int[] { 0, 0, 0 };
	}






}
