package HSR;

public class BookException extends Exception {
	
	private String type;
	public BookException(String errType) {
		type = errType;
	}

	public void printErrMsg() {
		System.out.println(type);
	}
	public String getErrMsg() {
		return type;
	}

}
