package HSR;
import java.util.Arrays;

public class UniversityDiscount {
	public String getTrainNo() {
		return trainNo;
	}

	public double[] getDiscount() {
		return discount;
	}

	@Override
	public String toString() {
		return "UniversityDiscount [trainNo=" + trainNo + ", discount=" + Arrays.toString(discount) + "]";
	}

	public UniversityDiscount(String trainNo, double[] discount) {
		super();
		this.trainNo = trainNo;
		this.discount = discount;
	}

	private String trainNo;
	private double[] discount = new double[7];

}
