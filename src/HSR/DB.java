package HSR;

import java.util.*;

import javax.print.attribute.standard.MediaSize.Other;

import java.text.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class DB {
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/";
	// Database credentials
	static final String USER = "root";
	static final String PASS = "12345678";
	static Connection conn = null;
	static Statement stmt = null;
	public static void init() {
		createDB.create();
		EarlyDiscountDB.init();
		SeatDB.init();
		TrainInfoDB.init();
		UniversityDiscountDB.init();
		createTB.create("CREATE TABLE code" + " (id INT NOT NULL AUTO_INCREMENT," + "date varchar(255),"
				+ "datee varchar(255)," + "PRIMARY KEY (id))");
	}
	public static void operate(String sql) {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL + "HSR", USER, PASS);
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			// System.out.println("Database deleted successfully...");
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		}
	}
	public static void cancel(Ticket ticket) {
		if (ticket.type == 4) {
			AmountOfEarly.cancelticket(ticket);
		}
		try{
			TicketDB.cancel(ticket);
		}catch(BookException e) {

		}
	}
	public static int GetCode(String date) {
		int ans = 0;
		Connection conn = null;
		Statement stmt = null;
		try{
			//STEP 2: Register JDBC driver
			Class.forName("com.mysql.cj.jdbc.Driver");

			// STEP 3: Open a connection
			// System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL + "HSR", USER, PASS);
			// STEP 4: Execute a query
			// System.out.println("Deleting database...");
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("select * from code");
			while (rs.next()) {
				ans = rs.getInt("id");
			}
			operate("insert into code (date) VALUES('" + date + "')");
			ans++;
			ans+=1;
			// System.out.println("Database deleted successfully...");
		} catch (SQLException se) {
			// Handle errors for JDBC
			operate("insert into code (date) VALUES('" + date + "')");
			System.out.println(1);
			ans = 1;
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		}
		for (int k = 0; k < 50; k++) {
			ans *= 3;
			ans -= 1;
			ans = ans % 100000000;
		}
		return ans;
	}
	public static int GetCode(String UID,String start,String end,String date,String no) {
		try{
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);
			//STEP 4: Execute a 
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT code FROM ticket"+date+" where UID='"+UID+"'AND start='"+
					start+"'AND end='"+end+"' AND no='"+no+"'");
			if(!rs.next()) {
				return 0;
			}
			return rs.getInt("code");
		}catch(SQLException se){
			se.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}
		return 0;
	}
	public static TrainInfo[] timesearch(TimeTableSearch search) {
		ArrayList<TrainInfo> rt = new ArrayList<TrainInfo>();
		ArrayList<String> st = new ArrayList<String>();
		st.add(search.startStation);
		st.add(search.endStation);
		String[] a = TrainInfoDB.getTiming(search.startStation + "," + search.endStation);
		double[] b = new double[a.length];
		double[] c = new double[a.length];
		for (int i = 0; i < a.length; i++) {
			b[i] = Math.round(AmountOfEarly.getdiscount(search.Date, a[i].split(",")[0]) * 100);
			b[i] /= 100;
		}
		for (int i = 0; i < a.length; i++) {
			c[i] = Math.round(
					UniversityDiscountDB.getDiscountValue(TransForm.DateToDay(search.Date), a[i].split(",")[0]) * 100);
			c[i] /= 100;
		}
		for (int i = 0; i < a.length; i++) {
			ArrayList<String> t = new ArrayList<String>();
			t.add(a[i].split(",")[1]);
			t.add(a[i].split(",")[2]);
			if ((b[i] == 0f) | (c[i] == 0f)) {
				continue;
			}
			if (search.earlyDiscountonly && search.universityDiscountonly) {
				if (((b[i] == 1.0f)) & (c[i] == 1.0f)) {
					continue;
				}
			} else if (search.earlyDiscountonly) {
				if (b[i] == 1.0f) {
					continue;
				}
			} else if (search.universityDiscountonly) {
				if (c[i] == 1.0f) {
					continue;
				}
			}
			rt.add(new TrainInfo(a[i].split(",")[0], 0, search.startStation, search.endStation, st, t, null, c[i],
					b[i]));
		}
		TrainInfo[] ans = new TrainInfo[rt.size()];
		for (int i = 0; i < rt.size(); i++) {
			ans[i] = rt.get(i);
		}
		return ans;
	}
	public static TrainInfo[] TimingSearch(TimeTableSearch search) {
		Date now=new Date();
		SimpleDateFormat ft = new SimpleDateFormat ("yyyyMMdd");
		String nowTime=ft.format(now);
		ArrayList<TrainInfo> rt = new ArrayList<TrainInfo>();
		TrainInfo[] a=timesearch(search);
		for(TrainInfo b:a) {
			if((TransForm.dateSubtraction(search.Date,nowTime)<0)|((new Timing(b.depatureTimes.get(0)).largerthan(new Timing(new Date())))&(new Timing(b.depatureTimes.get(0)).minus(new Timing(new Date()))).largerthan(new Timing("01:00")))) {
				System.out.println(b.trainNo+","+b.depatureTimes.get(0)+","+new Timing(new Date())+","+new Timing(b.depatureTimes.get(0)).minus(new Timing(new Date())));
<<<<<<< HEAD
			}
			if((new Timing(b.depatureTimes.get(0)).largerthan(new Timing(new Date())))&(new Timing(b.depatureTimes.get(0)).minus(new Timing(new Date()))).largerthan(new Timing("01:00"))) {
=======
=======
<<<<<<< HEAD
			if((TransForm.dateSubtraction(search.Date,nowTime)<0)|((new Timing(b.depatureTimes.get(0)).largerthan(new Timing(new Date())))&(new Timing(b.depatureTimes.get(0)).minus(new Timing(new Date()))).largerthan(new Timing("01:00")))) {
				System.out.println(b.trainNo+","+b.depatureTimes.get(0)+","+new Timing(new Date())+","+new Timing(b.depatureTimes.get(0)).minus(new Timing(new Date())));
=======
<<<<<<< HEAD
			if((new Timing(b.depatureTimes.get(0)).larger(new Timing(new Date())))&(new Timing(b.depatureTimes.get(0)).minus(new Timing(new Date()))).larger(new Timing("01:00"))) {
=======
			if((new Timing(b.depatureTimes.get(0)).largerthan(new Timing(new Date())))&(new Timing(b.depatureTimes.get(0)).minus(new Timing(new Date()))).largerthan(new Timing("01:00"))) {
<<<<<<< HEAD
=======
				System.out.println(b.trainNo+","+b.depatureTimes.get(0)+","+new Timing(new Date())+","+new Timing(b.depatureTimes.get(0)).minus(new Timing(new Date())));
>>>>>>> c385f4c14d1e1d09d6a19ededb3166d98bca0066
>>>>>>> db41d89ac7b4cc1b3b076a8b17f3fded0bb2d3f7
>>>>>>> 0137ddbfebaf853ba590dea1c82ed8fc2d28abf5
>>>>>>> 6f625fa842f7417cccae3b876c4dfc527682edb1
>>>>>>> 9fc24e43e3c786a6c0d7f1c1bd286190c9df0deb
				rt.add(b);
			}
		}
		TrainInfo[] rtt=new TrainInfo[rt.size()];
		for(int i=0;i<rt.size();i++) {
			rtt[i]=rt.get(i);
		}
		return rtt;
	}
	public static void storeTicket(Ticket order, String[] time) {
		String sql = "insert into ticket" + order.Date + " values('" + order.trainNo + "','" + order.startStation
				+ "','" + order.endStation + "','" + order.Date + "','" + time[0] + "','" + time[1] + "','"
				+ order.seat.toString() + "'," + order.type + ",'" + order.standard + "'," + order.discount + ",'"
				+ order.uid + "'," + order.code + "," + order.price + ",'" + order.payDeadLine + "') ";
		operate(sql);
	}
	public static void drop() {
		operate("DROP database HSR");
	}
	public static Ticket[] buyticket(Order order) throws BookException{
		Date now=new Date();
		SimpleDateFormat ft = new SimpleDateFormat ("yyyyMMdd,HH:mm");
		String nowTime=ft.format(now);
		if(TransForm.dateSubtraction(nowTime.split(",")[0],order.Date)<0|TransForm.dateSubtraction(nowTime.split(",")[0],order.Date)>28) {
			throw new BookException("日期錯誤");
		}
		try {
			createTB.initday(order.Date);
			ArrayList<Ticket> rt=new ArrayList<Ticket>();
			String[] time=TrainInfoDB.getTime(order.trainNo, order.startStation, order.endStation).split(",");
<<<<<<< HEAD
			if(TransForm.dateSubtraction(nowTime.split(",")[0],order.Date)==0&!new Timing(nowTime.split(",")[1]).minus(new Timing(time[0])).largerthan(new Timing("01:00"))) {
					throw new BookException("時間錯誤");
				}
				String paydl="";
				Date date=new Date();
				ft = new SimpleDateFormat ("yyyyMMdd");
				if(Math.abs(TransForm.dateSubtraction(order.Date,ft.format(date)))==0) {
					paydl=order.Date;
				} else if(Math.abs(TransForm.dateSubtraction(order.Date,ft.format(date)))<=3) {
					paydl=TransForm.dateAdd(order.Date, -1);
				} else {
					paydl=TransForm.dateAdd(ft.format(date), 3);
				}
				if(order.amounts[5]!=0&UniversityDiscountDB.getDiscountValue(TransForm.DateToDay(order.Date),order.trainNo)==0.0) {
					throw new BookException("沒有大學生優惠");
				}
				for(int i=0;i<6;i++) {
					for(int k=0;k<order.amounts[i];k++) {
						Seat a=TicketDB.buy(order);
						double discount=0;
						if(i==0) {
							discount=AmountOfEarly.buyticket(order.Date,order.trainNo);
						} else if(i==5) {
							discount=UniversityDiscountDB.getDiscountValue(TransForm.DateToDay(order.Date),order.trainNo);
						} else {
							discount=0.5;
						}
						System.out.println(2);
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 6f625fa842f7417cccae3b876c4dfc527682edb1
			if(TransForm.dateSubtraction(nowTime.split(",")[0],order.Date)==0) {
				if(!new Timing(nowTime.split(",")[1]).minus(new Timing(time[0])).largerthan(new Timing("01:00"))) {
					throw new BookException("時間錯誤");
				}
<<<<<<< HEAD
=======
=======
			if(!new Timing(nowTime.split(",")[1]).minus(new Timing(time[0])).largerthan(new Timing("01:00"))) {
				throw new BookException("時間錯誤");
>>>>>>> 0137ddbfebaf853ba590dea1c82ed8fc2d28abf5
>>>>>>> 6f625fa842f7417cccae3b876c4dfc527682edb1
			}
			String paydl="";
			Date date=new Date();
			ft = new SimpleDateFormat ("yyyyMMdd");
			if(Math.abs(TransForm.dateSubtraction(order.Date,ft.format(date)))==0) {
				paydl=order.Date;
			} else if(Math.abs(TransForm.dateSubtraction(order.Date,ft.format(date)))<=3) {
				paydl=TransForm.dateAdd(order.Date, -1);
			} else {
				paydl=TransForm.dateAdd(ft.format(date), 3);
			}
			if(order.amounts[5]!=0&UniversityDiscountDB.getDiscountValue(TransForm.DateToDay(order.Date),order.trainNo)==0.0) {
				throw new BookException("沒有大學生優惠");
			}
			for(int i=0;i<6;i++) {
				for(int k=0;k<order.amounts[i];k++) {
					Seat a=TicketDB.buy(order);
					double discount=0;
					if(i==0) {
						discount=AmountOfEarly.buyticket(order.Date,order.trainNo);
					} else if(i==5) {
						discount=UniversityDiscountDB.getDiscountValue(TransForm.DateToDay(order.Date),order.trainNo);
					} else {
						discount=0.5;
					}
					System.out.println(2);
>>>>>>> 9fc24e43e3c786a6c0d7f1c1bd286190c9df0deb

						int type=i;
						if((i==0)&(discount<1)) {
							type=4;
						}
						Ticket b=new Ticket(order.trainNo,order.startStation,order.endStation,order.Date, new Timing(time[0]), new Timing(time[1]),
								a, type, order.standard, discount, order.uid, order.code, paydl);
						rt.add(b);
						storeTicket(b,time);
					}
				}

				Ticket[] rtr=new Ticket[rt.size()];
				for(int i=0;i<rt.size();i++) {
					rtr[i]=rt.get(i);
				}
				return rtr;
			}catch(BookException e) {
				System.out.println(e.getErrMsg());
				throw e;
			}
		}
		public static Ticket[] search(String UID,int code) throws BookException{
			Date now=new Date();
			SimpleDateFormat ft = new SimpleDateFormat ("yyyyMMdd,HH:mm");
			String nowTime=ft.format(now);
			ArrayList<Ticket> rt=new ArrayList<Ticket>();
			try{
				Class.forName("com.mysql.cj.jdbc.Driver");
				conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);
				stmt = conn.createStatement();
				ResultSet rs=stmt.executeQuery("select * from code");
				String date="";
				while(rs.next()) {
					int ans=rs.getInt("id");
					for(int k=0;k<50;k++) {
						ans*=3;
						ans-=1;
						ans=ans%100000000;
					}
					if(ans==code) {
						date=rs.getString("date");
					}
				}
				if(date.equals("")) {
					throw new BookException("訂位代號有誤");
				}
				rs=stmt.executeQuery("select * from ticket"+date+" where UID='"+UID+"' AND code="+code);
				System.out.println(date);
				System.out.println();
				while(rs.next()) {
					if(((!new Timing(rs.getString("start")).largerthan(new Timing(nowTime.split(",")[1])))&TransForm.dateSubtraction(nowTime.split(",")[1], rs.getString("date"))==0)|TransForm.dateSubtraction(nowTime.split(",")[1], rs.getString("date"))<0) {
						throw new BookException("車子已經開走了");
					}
					Ticket b=new Ticket(rs.getString("no"),rs.getString("start"),rs.getString("end"),rs.getString("date"), 
							new Timing(rs.getString("starttime")), new Timing(rs.getString("endtime")),
							new Seat(rs.getString("seat")),rs.getInt("type"), rs.getBoolean("standard"), 
							rs.getDouble("discount"), rs.getString("UID"), rs.getInt("code"), rs.getString("paydeadline"));
					rt.add(b);
				}
				if(rt.size()==0) {
					throw new BookException("訂位代碼或UID有誤");
				}
				Ticket[] result=new Ticket[rt.size()];
				for(int i=0;i<rt.size();i++) {
					result[i]=rt.get(i);
				}
				return result;
			}catch(SQLException se){
				se.printStackTrace();
			}catch(BookException be) {
				throw be;
			}catch(Exception e){
				e.printStackTrace();
			}
			return null;
		}
		public static void main(String[] args) {
			//operate("drop table seat202106281691");
			/*int[] type= {2,0,0,0,0,0,0};
			Order test=new Order("b08505008", "1691", "0990", "1000", "20210720", true,2, type);
			//TimingSearch(new TimeTableSearch("0990", "1000","20210627",false,false));
			try {
				for(int i=0;i<2;i++) {
					Ticket[] a=buyticket(test);
					for(Ticket b:a) {
						System.out.println(b.toString());
					}
				}
			}catch(BookException e) {
				System.out.println(e.getErrMsg());
			}*/
			try {
				DB.search("b08505008",27006619);
				System.out.println(1);
			}catch(BookException be ) {
				System.out.println(be.getErrMsg());
			}
<<<<<<< HEAD
			/*Ticket test=new Ticket("1691","0990","1000","20210628",new Timing("21:10"),new Timing("21:21"),new Seat(1,3,'D'),4,true,0.65,"b08505008",61800194,"20210627");
=======
		}catch(BookException e) {
			System.out.println(e.getErrMsg());
		}
		/*Ticket test=new Ticket("1691","0990","1000","20210628",new Timing("21:10"),new Timing("21:21"),new Seat(1,3,'D'),4,true,0.65,"b08505008",61800194,"20210627");
>>>>>>> 9fc24e43e3c786a6c0d7f1c1bd286190c9df0deb
<<<<<<< HEAD
		Ticket test=new Ticket("1691","0990","1000","20210628",new Timing("21:10"),new Timing("21:21"),new Seat(1,3,'D'),4,true,0.65,"b08505008",61800194,"20210627");
		cancel(test);
		Ticket[] test=search("9999",35033093);
=======
=======
<<<<<<< HEAD
		/*Ticket test=new Ticket("1691","0990","1000","20210628",new Timing("21:10"),new Timing("21:21"),new Seat(1,3,'D'),4,true,0.65,"b08505008",61800194,"20210627");
=======
>>>>>>> 0137ddbfebaf853ba590dea1c82ed8fc2d28abf5
		Ticket test=new Ticket("1691","0990","1000","20210628",new Timing("21:10"),new Timing("21:21"),new Seat(1,3,'D'),4,true,0.65,"b08505008",61800194,"20210627");
		cancel(test);
		Ticket[] test=search("9999",35033093);
<<<<<<< HEAD
=======

=======
>>>>>>> db41d89ac7b4cc1b3b076a8b17f3fded0bb2d3f7
>>>>>>> 0137ddbfebaf853ba590dea1c82ed8fc2d28abf5
>>>>>>> 6f625fa842f7417cccae3b876c4dfc527682edb1
		cancel(test);
		Ticket[] test=search("b08505008",21152226);
		for(Ticket b:test) {
			System.out.println(b.toString());
		}*/

		}

	}