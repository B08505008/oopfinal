package HSR;
import java.sql.*; 
import java.util.ArrayList;
public class TicketDB {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost:3306/";
	//  Database credentials
	static final String USER = "root";
	static final String PASS = "12345678";
	static Connection conn = null;
	static Statement stmt = null;
	public static void init(String date,String no) {
		if(createTB.exist("seat" + date + no)) {
			return;
		}
		ArrayList<Seat> st=new ArrayList<Seat>();
		createTB.create("CREATE TABLE seat" + date + no +
				"(car INTEGER, "+
				" position INTEGER, "+
				" roww INTEGER, "+
				" seatcode VARCHAR(255))");
		String[] list=TrainInfoDB.getStation(no);
		int size=0;
		for(String sql:list) {
			DB.operate("alter table seat" + date + no +" ADD s"+sql+" varchar(1)");
			size++;
		}
		try{
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);
			stmt = conn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			ResultSet rs = stmt.executeQuery("select * from seat");
			while(rs.next()) {
				Seat a=new Seat(rs.getInt("car"),rs.getInt("roww"),rs.getString("seatcode").charAt(0));
				st.add(a);
			}
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}
		String ql="";
		boolean al=false;
		for(String sql:list) {
			if(al) {
				ql+=",";
			} else {
				al=true;
			}
			ql+="s"+sql+"='N'";
		}
		DB.operate("INSERT INTO seat" + date + no +" (car,position,roww,seatcode) select car,position,roww,seatcode from seat");
		String op="Update seat" + date + no + " set " + ql;
		DB.operate(op);
	}
	public static Seat buy(Order order) throws BookException {
		init(order.Date,order.trainNo);
		String[] sql=getSQL(order).split("-");
		try{
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);
			stmt = conn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			ResultSet rs = stmt.executeQuery(sql[0]);
			if(!rs.next()) {
				System.out.println("error");
				throw new BookException("餘票不足");
			}
			System.out.println("AC");
			Seat a=new Seat(rs.getInt("car"),rs.getInt("roww"),rs.getString("seatcode").charAt(0));
			stmt.executeUpdate(getUPSQL(order,rs.getInt("car"),rs.getInt("roww"),rs.getString("seatcode"),sql));
			return a;
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(BookException e) {
			throw e;
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}	
		finally{

			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}
		return null;
	}
	public static String getUPSQL(Order order,int car,int row,String seat,String[] station) {
		boolean first=true;
		String a="update seat"+order.Date+order.trainNo+" SET ";
		for(int i=1;i<station.length;i++) {
			if(first) {
				first=false;
			} else {
				a+=" , ";
			}
			a+=station[i]+" ='Y' ";
		}
		a+="where car="+car+" AND roww="+row+" AND seatcode='"+seat+"'";
		return a;
	}
	public static String getSQL(Order order) {
		int start=0;
		int end=0;
		boolean up=false;
		try{
			//STEP 2: Register JDBC driver
			Class.forName("com.mysql.cj.jdbc.Driver");

			//STEP 3: Open a connection
			//System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);
			//STEP 4: Execute a query
			//System.out.println("Deleting database...");
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM seat"+order.Date+order.trainNo);
			ResultSetMetaData rsmd = rs.getMetaData();
			for(int i=0;i<rsmd.getColumnCount();i++) {
				if(rsmd.getColumnName(i+1).equals("s"+order.startStation)) {
					start=i;
				}
				if(rsmd.getColumnName(i+1).equals("s"+order.endStation)) {
					end=i;
				}
			}
			String seatlist="";
			String sql="select * from seat"+order.Date+order.trainNo+" where ";
			for(int i=start+1;i<end+2;i++) {
				if(!up) {
					up=true;
				} else {
					sql+=" AND ";
				}
				sql+=rsmd.getColumnName(i)+"='N'";
				seatlist+="-"+rsmd.getColumnName(i);
			}
			if(!order.standard) {
				sql+=" AND car=6";
			}
			if(order.seatPreference!=3) {
				sql+=" AND position = "+order.seatPreference;
			}
			return sql+seatlist;
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}
		return null;
	}
	public static String getSQL(Ticket order) throws BookException{
		int start=0;
		int end=0;
		boolean up=false;
		try{
			//STEP 2: Register JDBC driver
			Class.forName("com.mysql.cj.jdbc.Driver");

			//STEP 3: Open a connection
			//System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);
			//STEP 4: Execute a query
			//System.out.println("Deleting database...");
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM seat"+order.Date+order.trainNo);
			ResultSetMetaData rsmd = rs.getMetaData();
			for(int i=0;i<rsmd.getColumnCount();i++) {
				if(rsmd.getColumnName(i+1).equals("s"+order.startStation)) {
					start=i;
				}
				if(rsmd.getColumnName(i+1).equals("s"+order.endStation)) {
					end=i;
				}
			}
			String seatlist="";
			String sql="select * from seat"+order.Date+order.trainNo+" where ";
			for(int i=start+1;i<end+2;i++) {
				if(!up) {
					up=true;
				} else {
					sql+=" AND ";
				}
				sql+=rsmd.getColumnName(i)+"='N'";
				seatlist+="-"+rsmd.getColumnName(i);
			}
			if(!up) {
				throw new BookException("請確認車次及起訖站是否正確");
			}
			return sql+seatlist;
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(BookException e){
			//Handle errors for Class.forName
			throw e;
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}
		return null;
	}
	public static String getUPSQL(Ticket order) {
		try{
			String[] station=getSQL(order).split("-");

			boolean first=true;
			String a="update seat"+order.Date+order.trainNo+" SET ";
			for(int i=1;i<station.length;i++) {
				if(first) {
					first=false;
				} else {
					a+=" , ";
				}
				a+=station[i]+" ='N' ";
			}
			a+="where car="+order.seat.getCar()+" AND roww="+order.seat.getRow()+" AND seatcode='"+order.seat.getSeatCode()+"'";
			return a;}catch(Exception e) {
			}
		return null;
	}
	public static void cancel(Ticket order) throws BookException {
		init(order.Date,order.trainNo);
		try{
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL+"HSR", USER, PASS);
			stmt = conn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			stmt.executeUpdate("DELETE FROM ticket" + order.Date +" where seat='"+order.seat.toString()+"' AND no='"+order.trainNo+"'");
			stmt.executeUpdate(getUPSQL(order));
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}
	}
	public static void main(String[] args) {
		Order test=new Order("b08505008", "0108", "1040", "1000", "20210627", true,
				2, null);
	}
}