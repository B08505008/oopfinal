package HSR;

import java.util.ArrayList;
import java.util.Arrays;

public class TrainInfo {

	public String getTrainNo() {
		return trainNo;
	}

	public int getDirection() {
		return direction;
	}

	public String getStartingStation() {
		return startingStation;
	}

	public String getEndingStation() {
		return endingStation;
	}

	public ArrayList<String> getStopStations() {
		return stopStations;
	}

	public ArrayList<String> getDepatureTimes() {
		return depatureTimes;
	}

	public int[] getService() {
		return service;
	}
	
	//不特定日期版本
	public TrainInfo(String trainNo, int direction, String startingStation, String endingStation,
			ArrayList<String> stopStations, ArrayList<String> depatureTimes, int[] service) {
		super();
		this.trainNo = trainNo;
		this.direction = direction;
		this.startingStation = startingStation;
		this.endingStation = endingStation;
		this.stopStations = stopStations;
		this.depatureTimes = depatureTimes;
		this.service = service;
	}
	
	
	//特定日期版本,加入兩種優惠
	public TrainInfo(String trainNo, int direction, String startingStation, String endingStation,
			ArrayList<String> stopStations, ArrayList<String> depatureTimes, int[] service, double universityDiscount, double earlyDiscount) {
		super();
		this.trainNo = trainNo;
		this.direction = direction;
		this.startingStation = startingStation;
		this.endingStation = endingStation;
		this.stopStations = stopStations;
		this.depatureTimes = depatureTimes;
		this.service = service;
		this.universityDiscount = universityDiscount;
		this.earlyDiscount = earlyDiscount;
		
	}

	@Override
	public String toString() {
		return "TrainInfo [trainNo=" + trainNo + ", direction=" + direction + ", startingStation=" + startingStation
				+ ", endingStation=" + endingStation + ", stopStations=" + stopStations + ", depatureTimes="
				+ depatureTimes + ", service=" + Arrays.toString(service) + ", universityDiscount="
				+ universityDiscount + ", earlyDiscount=" + earlyDiscount+ "]";
	}

	public String trainNo;
	public int direction; // 0:southbound 1:northbound
	public String startingStation; // StationID
	public String endingStation; // StationID

	// stopStations&depatureTimes use the stopping sequence as their index
	public ArrayList<String> stopStations = new ArrayList<String>();
	public ArrayList<String> depatureTimes = new ArrayList<String>();

	// From "Monday" to "Sunday"!!!!!!!
	public int[] service = new int[7];
	
	
	//這兩個資料只在查詢特定日期之班次時有意義
	public double universityDiscount;
	public double earlyDiscount;
	
	
	public double getUniversityDiscount() {
		return universityDiscount;
	}

	public double getEarlyDiscount() {
		return earlyDiscount;
	}

}
