package HSR;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class AWTdemo {
    public static void main(String[] args) {
        Frame frame = new Frame("AWTDemo");
        frame.addWindowListener(new AdapterDemo());
        frame.setLayout(new FlowLayout());
         
        Button button = new Button("AWT1");
        Checkbox checkbox = new Checkbox("AWT2");
        Choice choice = new Choice();
        choice.add("AWT3");
        Label label = new Label("AWT4");
        TextArea textarea = new TextArea("AWT6");
        TextField textfield = new TextField("AWT7");
         
        frame.add(button);
        frame.add(checkbox);
        frame.add(choice);
        frame.add(label);
        frame.add(textarea);
        frame.add(textfield);
         
        frame.pack();
        frame.setVisible(true);
        for(int i=0;i<2100000000;i++) {
        	
        }
        AWTdemo2.windowCg();
    }
}
class AdapterDemo extends WindowAdapter {
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }
}
class AWTdemo2 {
    public static void windowCg() {
    	Frame frame = new Frame("AWTDemo");
        frame.setLayout(new FlowLayout());
        frame.addWindowListener(new AdapterDemo());

        Button button = new Button("AWT1");
        Checkbox checkbox = new Checkbox("AWT2");
        Choice choice = new Choice();
        choice.add("AWT3");
        Label label = new Label("AWT4");

        frame.add(button);
        frame.add(checkbox);
        frame.add(choice);
        frame.add(label);
        
        frame.pack();
        frame.setVisible(true);
    }
}