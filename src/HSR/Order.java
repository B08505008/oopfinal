package HSR;

public class Order {

	public Order(String uid, String trainNo, String startStation, String endStation, String date, boolean standard,
			int seatPreference, int[] amounts) {
		super();
		this.uid = uid; //uid 新增
		this.trainNo = trainNo;
		this.startStation = startStation;
		this.endStation = endStation;
		this.Date = date;
		this.standard = standard;
		this.seatPreference = seatPreference;
		this.amounts = amounts; //array長度=6 index4務必保持0 
		this.code = this.codeGenerator(); 
	}
	
	public String uid;
	public String trainNo; 
	public String startStation;
	public String endStation;
	public String Date;
	public boolean standard =true ; //true for standard false for business
	/*public enum tickettype{
		ADULT,CHILD,ELDER,DISABLED,EARLYDISCOUNT,UNIVERSITYDISCOUNT;
	}這個東西先用不到*/
	public int seatPreference = 3; // 0 for window, 2 for aisle, 3 for none. correspond to Seat.java.	
	public int[] amounts; //index對照請參tickettypetransform 內容代表該index之訂票數量
	public int code; //定位代碼

	
/*	當字典用
 * public type ticketTypetransform(int i) {
		switch(i) {
		case'0':
			return type.ADULT+EARLYDISCOUNT;
		case'1':
			return type.CHILD;
		case'2':
			return type.ELDER;
		case'3':
			return type.DISABLED;
		case'4':
			return type.EARLYDISCOUNT;
			在Order這裡千萬不要用4!!!!!!
			但在Ticket還是要用!!!!!
		case'5':
			return type.UNIVERSITYDISCOUNT;
		default:
			return type.ADULT;
		}	
	}*/
	
	public void setCode(int i) {
		this.code = i;
	}//用在改票&來回票
	
	public int codeGenerator() {
		return DB.GetCode(this.Date);
	}//產生唯一之訂位代碼
}
