package HSR;

public class TimeTableSearch {

	public String startStation;
	public String endStation;
	public String Date;
	public boolean universityDiscountonly;
	public boolean earlyDiscountonly;
	
	public TimeTableSearch(String startStation, String endStation, String date, boolean universityDiscountonly,
			boolean earlyDiscountonly) {
		this.startStation = startStation;
		this.endStation = endStation;
		Date = date;
		this.universityDiscountonly = universityDiscountonly;
		this.earlyDiscountonly = earlyDiscountonly;
	}

}
